//
//  SettingsViewController.swift
//  crosspost_swift
//
//  Created by vamika on 16/09/20.
//  Copyright © 2020 vamika. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController,PurchaseHandlerDelegate, PurchaseViewControllerDelegate {
    
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var userNameLbl: UIButton!
    @IBOutlet weak var managaSubscription: UILabel!
    @IBOutlet weak var shareApp: UILabel!
    @IBOutlet weak var followUs: UILabel!
    @IBOutlet weak var settingLbl: UILabel!
    @IBOutlet weak var onSubsCription: UILabel!
    @IBOutlet weak var howToRepost: UILabel!
    var purchaseViewController = PurchaseViewController()
    var purchaseSuccessViewController = PurchaseSuccessViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Global.addShadowAtBottom(view: topView)
        self.navigationController?.navigationBar.isHidden = true
        if UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE) as? String == nil {
            self.userNameLbl.setTitle("Add Workspace", for: .normal)
        }else{
            self.userNameLbl.setTitle(UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE) as? String, for: .normal)
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.onSubsCription.text = String(format: "Active Subscription: %@", Global.getActiveSubscriptionName())
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.onSubsCription.textColor = UIColor(named: "APP_BLUE_COLOR")
        self.managaSubscription.textColor = UIColor(named: "APP_BLUE_COLOR")
        self.howToRepost.textColor = UIColor(named: "APP_BLUE_COLOR")
        self.followUs.textColor = UIColor(named: "APP_BLUE_COLOR")
        self.shareApp.textColor = UIColor(named: "APP_BLUE_COLOR")
        self.settingLbl.textColor = UIColor(named: "APP_BLUE_COLOR")
    }
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func oCLickUserNameButton(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: ViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                NotificationCenter.default.post(name: NSNotification.Name("Add_Worspace"), object: [:], userInfo: nil)
                break
            }
        }
    }
    
    @IBAction func onSubscriptionClick(_ sender: Any) {
        initialisePurchaseViewController()
    }
    
    @IBAction func manageSubscriptionClick(_ sender: Any) {
        if UIApplication.shared.canOpenURL(NSURL(string: MANAGE_SUBSCRIPTION_URL)! as URL){
            UIApplication.shared.open(NSURL(string: MANAGE_SUBSCRIPTION_URL)! as URL, options: [:], completionHandler: nil)
        }
        
    }
    @IBAction func howToRepostClick(_ sender: Any) {
    }
    
    @IBAction func onClickShareApp(_ sender: Any) {
        let shareAll = [SHARE_APP_TEXT ,FOLLOW_US_WITH_WEB_URL as Any] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func followUsOnIG(_ sender: Any) {
        let instagramURL = NSURL(string: FOLLOW_US_WITH_APP_URL)! as URL
        if UIApplication.shared.canOpenURL(instagramURL) {
            UIApplication.shared.open(instagramURL, options: [:], completionHandler: nil)
        }else{
            let instagramURL = NSURL(string: FOLLOW_US_WITH_WEB_URL)! as URL
            if UIApplication.shared.canOpenURL(instagramURL) {
                UIApplication.shared.open(instagramURL, options: [:], completionHandler: nil)
            }
        }
    }
    func initialisePurchaseViewController(){
        
        self.view.endEditing(true)
        if (purchaseViewController.isKind(of: PurchaseViewController.self)){
            purchaseViewController.view.removeFromSuperview()
            purchaseViewController = PurchaseViewController()
            
        }
        purchaseViewController = PurchaseViewController.init(nibName: "PurchaseViewController", bundle: nil)
        purchaseViewController.delegate = self
        purchaseViewController.view.frame = UIScreen.main.bounds
        let currentWindow = UIApplication.shared.keyWindow
        purchaseViewController.crossBtn.addTarget(self, action: #selector(onNoThanksBtn(_:)), for: .touchUpInside)
        currentWindow?.addSubview(purchaseViewController.view)
        
        purchaseViewController.view.alpha = 0.0;
        UIView.animate(withDuration: 0.35, animations: {
            self.purchaseViewController.view.alpha = 1.0
        }) { (finished) in
            self.purchaseViewController.view.alpha = 1.0
        }
    }
    
    func initialisePurchaseSuccessViewController(){
        if (purchaseSuccessViewController.isKind(of: PurchaseSuccessViewController.self)){
            purchaseSuccessViewController.view.removeFromSuperview()
            purchaseSuccessViewController = PurchaseSuccessViewController()
        }
        purchaseSuccessViewController = PurchaseSuccessViewController.init(nibName: "PurchaseSuccessViewController", bundle: nil)
        purchaseSuccessViewController.view.frame = UIScreen.main.bounds
        let currentWindow = UIApplication.shared.keyWindow
        purchaseSuccessViewController.letsGoBtn!.addTarget(self, action: #selector(onLetsGoBtn(_:)), for: .touchUpInside)
        currentWindow?.addSubview(purchaseSuccessViewController.view)
        
        purchaseSuccessViewController.view.alpha = 0.0;
        UIView.animate(withDuration: 0.35, animations: {
            self.purchaseSuccessViewController.view.alpha = 1.0
        }) { (finished) in
            self.purchaseSuccessViewController.view.alpha = 1.0
        }
    }
    @objc func onLetsGoBtn(_ sender: Any) {
        if purchaseSuccessViewController.view != nil {
            UIView.animate(withDuration: 0.35, animations: { [self] in
                purchaseSuccessViewController.view.alpha = 0.0
            }) { [self] finished in
                purchaseSuccessViewController.view.alpha = 0.0
                purchaseSuccessViewController.view.removeFromSuperview()
                purchaseSuccessViewController = PurchaseSuccessViewController()
            }
        }
    }
    @objc func onNoThanksBtn(_ sender: Any?) {
        if purchaseViewController.view != nil {
            UIView.animate(withDuration: 0.35, animations: { [self] in
                purchaseViewController.view.alpha = 0.0
            }) { [self] finished in
                purchaseViewController.view.alpha = 0.0
                purchaseViewController.view.removeFromSuperview()
                purchaseViewController = PurchaseViewController()
//                UserDefaults.standard.set(1, forKey: APP_SESSION_COUNT)
//                UserDefaults.standard.synchronize()
            }
        }
    }
    func buy(_ product: SKProduct?) {
        PurchaseHandler.sharedInstance().delegate = self
        PurchaseHandler.sharedInstance()?.buyProduct(product?.productIdentifier)
        setUserInterationDisabled()
    }

    func restoreTransaction() {
        PurchaseHandler.sharedInstance().delegate = self
        PurchaseHandler.sharedInstance().restorePurchases()
        setUserInterationDisabled()
    }

    func setUserInterationDisabled() {
        purchaseViewController.activityIndicator.isHidden = false
        purchaseViewController.activityIndicator.startAnimating()
        purchaseViewController.startFreeWeekBtn.isUserInteractionEnabled = false
        purchaseViewController.restoreBtn.isUserInteractionEnabled = false
        purchaseViewController.crossBtn.isUserInteractionEnabled = false
    }
   
    func transactionFailed() {
        purchaseViewController.activityIndicator.isHidden = true
        purchaseViewController.activityIndicator.stopAnimating()
        purchaseViewController.startFreeWeekBtn.isUserInteractionEnabled = true
        purchaseViewController.restoreBtn.isUserInteractionEnabled = true
        purchaseViewController.crossBtn.isUserInteractionEnabled = true
        Global.removeWaitingView()
    }

    func transactionSuccessful(_ transaction: SKPaymentTransaction?) {
        purchaseViewController.activityIndicator.isHidden = true
        purchaseViewController.activityIndicator.stopAnimating()
        purchaseViewController.startFreeWeekBtn.isUserInteractionEnabled = true
        purchaseViewController.restoreBtn.isUserInteractionEnabled = true
        purchaseViewController.crossBtn.isUserInteractionEnabled = true
        onNoThanksBtn(nil)
        initialisePurchaseSuccessViewController()
    }
    func transactionRestored() {

        Global.removeWaitingView()

        if Global.isPaidUser() {

            let controller = UIAlertController(
                title: "Success",
                message: "Restored Successfully!!",
                preferredStyle: .alert)

            controller.addAction(UIAlertAction(title: "OK", style: .default, handler: { [self] action in
                if purchaseViewController.view != nil {
                    purchaseViewController.activityIndicator.isHidden = true
                    purchaseViewController.activityIndicator.stopAnimating()
                    purchaseViewController.startFreeWeekBtn.isUserInteractionEnabled = true
                    purchaseViewController.restoreBtn.isUserInteractionEnabled = true
                    purchaseViewController.crossBtn.isUserInteractionEnabled = true
                    onNoThanksBtn(nil)
                    initialisePurchaseSuccessViewController()
                }
            }))

            UIApplication.shared.keyWindow?.rootViewController?.present(controller, animated: true)
        } else {
            let controller = UIAlertController(
                title: "Woops!!",
                message: "No Active subscription found.",
                preferredStyle: .alert)

            controller.addAction(UIAlertAction(title: "OK", style: .default, handler: { [self] action in
                if  purchaseViewController.view != nil {
                    purchaseViewController.activityIndicator.isHidden = true
                    purchaseViewController.activityIndicator.stopAnimating()
                    purchaseViewController.startFreeWeekBtn.isUserInteractionEnabled = true
                    purchaseViewController.restoreBtn.isUserInteractionEnabled = true
                    purchaseViewController.crossBtn.isUserInteractionEnabled = true
                }
            }))

            UIApplication.shared.keyWindow?.rootViewController?.present(controller, animated: true)
        }
    }
}
