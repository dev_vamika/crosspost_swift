//
//  WorkspaceViewCell.swift
//  crosspost_swift
//
//  Created by vamika on 01/10/20.
//  Copyright © 2020 vamika. All rights reserved.
//

import UIKit

class WorkspaceViewCell: UITableViewCell {
    @IBOutlet weak var workspaceName: UILabel!
    @IBOutlet weak var nameInitialLabel: UILabel!
    @IBOutlet weak var postCount: UILabel!
//    @IBOutlet weak var workSpaceTF: UITextField!
    @IBOutlet weak var deletBtn: UIButton!
    @IBOutlet weak var nameInitialLabelHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setDefaultImage(name: String, bgColor : UIColor, textColor: UIColor) {
        nameInitialLabel.text = String(name[name.startIndex]).uppercased()
        nameInitialLabel.backgroundColor = bgColor
        nameInitialLabel.textColor = textColor
    }

    
}
