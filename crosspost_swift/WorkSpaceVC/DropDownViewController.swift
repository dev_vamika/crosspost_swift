//
//  DropDownViewController.swift
//  crosspost_swift
//
//  Created by vamika on 25/09/20.
//  Copyright © 2020 vamika. All rights reserved.
//

import UIKit
protocol DropDownViewControllerDelegate: class {
    func onAddNewWorspace()
    func changeWorkspace()
}
class DropDownViewController: UIViewController, UITextFieldDelegate {
    weak var delegate: DropDownViewControllerDelegate?
//    @IBOutlet weak var tfBackView: UIView!
    @IBOutlet weak var backViewHeight: NSLayoutConstraint!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sampleLbl: UILabel!
    @IBOutlet weak var deleteMsgLbl: UILabel!
    @IBOutlet weak var workSpaceLbl: UILabel!
    @IBOutlet weak var workSpaceTF: UITextField!
//    @IBOutlet weak var tfTop: NSLayoutConstraint!
    @IBOutlet weak var loader: UIActivityIndicatorView!
//    @IBOutlet weak var addWorkspaceBtn: UIButton!
    @IBOutlet weak var alertBackView: UIView!
    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var closeBtn: UIButton!
    var data = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.alertBackView.isHidden = true
        self.loader.isHidden = true
//        self.tfBackView.isHidden = true
        self.backView.isHidden = false
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.workSpaceTF.delegate = self
    
        let db = DBManager()
        data = NSMutableArray()
        data = db.getDataForField(WORSPACE_TABLE, where: nil)
        if data.count == 0 {
            self.workSpaceTF.becomeFirstResponder()
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        Global.makeCornerRadius(view: yesBtn, radius: 20)
        Global.makeCornerRadius(view: addBtn, radius: 5.0)
        Global.addBorderWithhRadius(view: cancelBtn, radius: 20, borderWidth: 0.15, borderColor: sampleLbl.textColor!)
        //Global.addBorderWithhRadius(view: workSpaceTF, radius: 0.0, borderWidth: 1.0, borderColor: UIColor(red: 0.454, green: 0.626, blue: 0.748, alpha: 1))
        Global.setLeftPaddingPoints(field: workSpaceTF)
        Global.addShadowToView(view: backView)
        Global.addShadowToView(view: alertBackView)
        self.backView.roundedToptCornersOfButton()
        self.alertBackView.roundedToptCornersOfButton()
        self.backViewHeight.constant = CGFloat(45*(data.count)) + 150
        if self.backViewHeight.constant > self.view.frame.height*0.75 {
            self.backViewHeight.constant = self.view.frame.height*0.75
        }
        self.alertBackView.backgroundColor = UIColor(named: "CELL_BG_COLOR")
        self.backView.backgroundColor = UIColor(named: "CELL_BG_COLOR")
        
        
        let color = UIColor(named: "APP_BLUE_COLOR")
        self.closeBtn.setTitleColor(color, for: .normal)
        self.cancelBtn.setTitleColor(color, for: .normal)
        self.yesBtn.setTitleColor(UIColor(named: "HASH_BG_COLOR"), for: .normal)
        self.yesBtn.backgroundColor = UIColor(named: "HASH_BG_COLOR")
        self.yesBtn.setTitleColor(color, for: .normal)
        self.addBtn.setTitleColor(color, for: .normal)
        self.workSpaceTF.textColor = color
        self.workSpaceLbl.textColor = color
        self.sampleLbl.textColor = color
        self.deleteMsgLbl.textColor = color
        workSpaceTF.attributedPlaceholder = NSAttributedString(string: "@addWorspace",attributes: [NSAttributedString.Key.foregroundColor: color!])
    }
    override func viewDidDisappear(_ animated: Bool) {
//        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        backView.animShow()
        setUpObserver()
    }
    
    private func setUpObserver() {
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)

    }
//    @objc fileprivate func keyboardWillShow(notification:NSNotification) {
//        if let keyboardRectValue = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
//            let keyboardHeight = keyboardRectValue.height
////            self.tfBackView.isHidden = false
//            self.backView.isHidden = true
//            if isiPhone11{
//                self.tfTop.constant = ((self.view.frame.height - keyboardHeight) - 65 - self.view.safeAreaInsets.bottom)
//            }else{
//                self.tfTop.constant = ((self.view.frame.height - keyboardHeight) - 70 - self.view.safeAreaInsets.bottom)
//            }
//        }
//    }
//    @objc func keyboardWillHide(notification:NSNotification) {
//        self.tfTop.constant = 0.0
//        }
    
    @IBAction func deleteWorkSpace(_ sender: Any) {
        let index = self.yesBtn.tag
        let db = DBManager()
        let dic = data[index] as? NSDictionary
        if dic?.value(forKey: "name") as? String != nil {
            let postData = db.getDataForField(DATA_TABLE, where: String(format: "%@= '%@'", WORK_SPACE_NAME, dic?.value(forKey: "name") as! String))
            if postData?.count != 0 {
                for _ in 0...postData!.count-1{
                    db.deleteRow(DATA_TABLE, where: String(format: "%@= '%@' AND %@= '%@'", WORK_SPACE_NAME, dic?.value(forKey: "name") as! String, "created_time", dic?.value(forKey: "created_time") as! String))
                    
                }
                db.deleteRow(WORSPACE_TABLE, where: String(format: "%@= '%@' AND %@= '%@'", "name", dic?.value(forKey: "name") as! String, "created_time", dic?.value(forKey: "created_time") as! String))
                self.reloadData()
                self.alertBackView.isHidden = true
                self.backView.animShow()
            }else{
                db.deleteRow(WORSPACE_TABLE, where: String(format: "%@= '%@' AND %@= '%@'", "name", dic?.value(forKey: "name") as! String, "created_time", dic?.value(forKey: "created_time") as! String))
                self.reloadData()
                self.alertBackView.isHidden = true
                self.backView.animShow()
            }
        }
        
    }
    @IBAction func onCancel(_ sender: Any) {
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveLinear],
                       animations: {
//                        self.alertBackView.center.y += self.backView.bounds.height
//                        self.alertBackView.layoutIfNeeded()
                        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)

        },  completion: {(_ completed: Bool) -> Void in
            self.alertBackView.isHidden = true
            self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
            self.backView.animShow()
        })
    }
    
    func animHide(){
        
        
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveLinear],
                       animations: {
                        
                        self.backView.center.y += self.backView.bounds.height
                        self.backView.layoutIfNeeded()
                        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)

        },  completion: {(_ completed: Bool) -> Void in
            self.workSpaceTF.resignFirstResponder()
//            self.tfBackView.isHidden = true
            self.backView.isHidden = true
            self.dismiss(animated: false, completion: nil)
        })
    }
    
    @IBAction func onClickAddWorspace(_ sender: Any) {
                if workSpaceTF.text != "" {
                    self.loader.isHidden = false
                    self.loader.startAnimating()
                    let timestamp = NSDate()
                    let db = DBManager()
                    var dictionaryToSave: [AnyHashable : Any] = [:]
                    dictionaryToSave["name"] = workSpaceTF.text?.dropFirst()
                    dictionaryToSave["created_time"] = String(format: "%@", timestamp)
                    db.insertValues(WORSPACE_TABLE, values: dictionaryToSave)
                    let workSpaceData = db.getDataForField(WORSPACE_TABLE, where: nil)
                    let dic = workSpaceData![workSpaceData!.count - 1] as? NSDictionary
                    if UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE) == nil{
                        let postData = db.getDataForField(DATA_TABLE, where: nil)
                        if postData?.count != 0 {
                            for i in 0...postData!.count-1{
                                let postDictCopy = postData![i] as! NSMutableDictionary
                                postDictCopy[WORK_SPACE_NAME] = self.workSpaceTF.text
                                postDictCopy[WORK_SPACE_NAME_ID] = dic!["id"]
                                db.insertValues(DATA_TABLE, values: postDictCopy as? [AnyHashable : Any])
                            }
                            UserDefaults.standard.set(self.workSpaceTF.text?.replacingOccurrences(of: "@", with: ""), forKey: CURRENT_WORK_SPACE)
                            UserDefaults.standard.set(dic!["id"], forKey: CURRENT_WORK_SPACE_ID)
                            let blankData = db.getDataForField(DATA_TABLE, where: String(format:"%@ = '%@'",WORK_SPACE_NAME_ID,""))
                            if blankData?.count != 0 {
                                for _ in 0...postData!.count-1{
                                    db.deleteRow(DATA_TABLE, where: String(format:"%@ = '%@'",WORK_SPACE_NAME_ID,""))
                                }
        
                            }
                            self.dismiss(animated: false) {
                                self.loader.stopAnimating()
                                self.loader.isHidden = true
                                self.delegate?.changeWorkspace()
                            }
        
                        }else{
                            UserDefaults.standard.set(self.workSpaceTF.text?.replacingOccurrences(of: "@", with: ""), forKey: CURRENT_WORK_SPACE)
                            UserDefaults.standard.set(dic!["id"], forKey: CURRENT_WORK_SPACE_ID)
                            self.dismiss(animated: false) {
                                self.loader.stopAnimating()
                                self.loader.isHidden = true
                                self.delegate?.changeWorkspace()
                            }
                        }
                    }else{
                        if data.count == 0{
                        UserDefaults.standard.set(self.workSpaceTF.text?.replacingOccurrences(of: "@", with: ""), forKey: CURRENT_WORK_SPACE)
                        UserDefaults.standard.set(dic!["id"], forKey: CURRENT_WORK_SPACE_ID)
                        self.dismiss(animated: false) {
                            self.loader.stopAnimating()
                            self.loader.isHidden = true
                            self.delegate?.changeWorkspace()
                        }
                        }else{
                            workSpaceTF.text  = ""
                            self.loader.stopAnimating()
                            self.loader.isHidden = true
                            self.workSpaceTF.resignFirstResponder()
                            self.reloadData()
                        }
                    }
                }else{
                    workSpaceTF.becomeFirstResponder()
                }
        
        
        
//         workSpaceTF.resignFirstResponder()
//        if workSpaceTF.text != "" {
//            self.loader.isHidden = false
//            self.loader.startAnimating()
//            let db = DBManager()
//            let timestamp = NSDate()
//            var dictionaryToSave: [AnyHashable : Any] = [:]
//            dictionaryToSave["name"] = workSpaceTF.text
//            dictionaryToSave["created_time"] = String(format: "%@", timestamp)
//            db.insertValues(WORSPACE_TABLE, values: dictionaryToSave)
//
//            if UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE) == nil{
//                let postData = db.getDataForField(DATA_TABLE, where: nil)
//                if postData?.count != 0 {
//                    for i in 0...postData!.count-1{
//                        let postDictCopy = postData![i] as! NSMutableDictionary
//                        postDictCopy[WORK_SPACE_NAME] = self.workSpaceTF.text
//                        db.insertValues(DATA_TABLE, values: postDictCopy as? [AnyHashable : Any])
//                    }
//                        self.loader.stopAnimating()
//                        self.loader.isHidden = true
//                    self.reloadData()
//                }else{
//                        self.loader.stopAnimating()
//                        self.loader.isHidden = true
//                    self.reloadData()
//                }
//            }else{
//                    self.loader.stopAnimating()
//                    self.loader.isHidden = true
//                    self.reloadData()
//            }
//        }
//
    }
    func reloadData(){
        let db = DBManager()
        data = NSMutableArray()
        data = db.getDataForField(WORSPACE_TABLE, where: nil)
        self.backViewHeight.constant = CGFloat(45*(data.count)) + 150
        if self.backViewHeight.constant > self.view.frame.height*0.75 {
            self.backViewHeight.constant = self.view.frame.height*0.75
        }
        self.tableView.reloadData()
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
//        if workSpaceTF.canBecomeFocused {
//            Global.addBorderWithhRadius(view: workSpaceTF, radius: 0.0, borderWidth: 1.0, borderColor: UIColor(red: 0.072, green: 0.301, blue: 0.464, alpha: 1))
//        }else{
//            Global.addBorderWithhRadius(view: workSpaceTF, radius: 0.0, borderWidth: 1.0, borderColor: UIColor(red: 0.454, green: 0.626, blue: 0.748, alpha: 1))
//
//        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.text!.count <= 21 {
            if !textField.text!.trimmingCharacters(in: .whitespaces).isEmpty{
                textField.text = textField.text?.trimmingCharacters(in: .whitespaces)
            }
            if textField.text != ""{
                let fistIndex = textField.text?.first
                if fistIndex != "@" {
                    textField.text = String(format: "@%@", textField.text!)
                }
                
            }else{
                if textField.text == "@"{
                    textField.text = ""
                }
            }
        }else{
            textField.text = String(textField.text!.prefix(21))
        }
        
        return true
    }
    
}
extension UIView{
    func animShow() {
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.center.y -= self.bounds.height
                        self.layoutIfNeeded()
        }, completion: nil)
        self.isHidden = false
    }
    
}
extension DropDownViewController:UITableViewDelegate, UITableViewDataSource, WorkSpaceViewControllerDelegate {
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? WorkspaceViewCell
        
        cell?.workspaceName.font = sampleLbl.font
        cell?.workspaceName.textAlignment = .left
       
        
        let dic = data[indexPath.row] as? NSDictionary
        if dic?.value(forKey: "name") as? String != nil {
            cell?.workspaceName.isHidden = false
            cell?.nameInitialLabelHeight.constant = 40.0
            let db = DBManager()
            if UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE) != nil{
                if dic?.value(forKey: "id") as! String ==  (UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE_ID) as? String)! {
                    cell?.workspaceName.textColor = UIColor(named: "APP_BLUE_COLOR")
                    cell?.postCount?.textColor = UIColor(named: "APP_BLUE_COLOR")
                    cell?.deletBtn.isHidden = true
                    let array = db.getDataForField(DATA_TABLE, where: String(format: "%@= '%@'",WORK_SPACE_NAME_ID,(dic?.value(forKey: "id") as? String)!))
                    cell?.postCount.text = array!.count > 1 ? String(format: "(%d posts)", (array?.count)!) : String(format: "(%d post)", (array?.count)!)
                    cell?.setDefaultImage(name: (dic?.value(forKey: "name") as? String)!, bgColor: UIColor(red: 0.072, green: 0.301, blue: 0.464, alpha: 1), textColor: .white)
                }else{
                    cell?.deletBtn.isHidden = false
                    cell?.workspaceName?.textColor = UIColor(red: 0.454, green: 0.626, blue: 0.748, alpha: 1)
                    cell?.postCount?.textColor = UIColor(red: 0.454, green: 0.626, blue: 0.748, alpha: 1)
                    let array = db.getDataForField(DATA_TABLE, where: String(format: "%@= '%@'",WORK_SPACE_NAME_ID,(dic?.value(forKey: "id") as? String)!))
                    cell?.postCount.text = array!.count > 1 ? String(format: "(%d posts)", (array?.count)!) : String(format: "(%d post)", (array?.count)!)
                    cell?.setDefaultImage(name: (dic?.value(forKey: "name") as? String)!, bgColor: UIColor(red: 0.454, green: 0.626, blue: 0.748, alpha: 1), textColor: .white)
                }
            }
            cell?.workspaceName?.text = String(format: "@%@", dic?.value(forKey: "name") as! String)
        }else{
            cell?.nameInitialLabelHeight.constant = 0.0
            cell?.deletBtn.isHidden = true
            cell?.workspaceName.isHidden = true
        }
        cell?.deletBtn.addTarget(self, action: #selector(showAlert(_:)), for: .touchUpInside)
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dic = data[indexPath.row] as? NSDictionary
        if dic?.value(forKey: "name") as? String != nil {
            UserDefaults.standard.set(dic?.value(forKey: "name") as? String, forKey: CURRENT_WORK_SPACE)
            UserDefaults.standard.set(dic?.value(forKey: "id"), forKey: CURRENT_WORK_SPACE_ID)
            self.delegate?.changeWorkspace()
            self.dismiss(animated: false){}
        }else{
            self.workSpaceTF.becomeFirstResponder()
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       if segue.identifier == "addNewW"{
            if let nextViewController = segue.destination as? WorkSpaceViewController {
                nextViewController.delegate = self
            }
        }
    }
    @objc func showAlert(_ sender: Any) {
        
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveLinear],
                       animations: {
                        self.backView.center.y += self.backView.bounds.height
                        self.backView.layoutIfNeeded()
                        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)

        },  completion: {(_ completed: Bool) -> Void in
            self.backView.isHidden = true
            if let cell = (sender as AnyObject).superview?.superview as? WorkspaceViewCell,
                   let indexPath = self.tableView.indexPath(for: cell) {
                self.yesBtn.tag = indexPath.row
                self.openAlerView()
            
            }
        })
    }
    func openAlerView(){
        UIView.animate(withDuration: 0.2, delay: 0, options: [.transitionFlipFromBottom],
                       animations: {
//                        self.alertBackView.center.y -= 150
//                        self.alertBackView.layoutIfNeeded()
        }, completion: nil)
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        self.alertBackView.isHidden = false
    }
    func newWorkSpaceAddedSuccessfully() {
        let db = DBManager()
        data = NSMutableArray()
        data = db.getDataForField(WORSPACE_TABLE, where: nil)
        self.backViewHeight.constant = CGFloat(45*(data.count)) + 100
        if self.backViewHeight.constant > self.view.frame.height*0.75 {
            self.backViewHeight.constant = self.view.frame.height*0.75
        }
        self.tableView.reloadData()
    }
    func changeWorkspace() {

    }
    @IBAction func onAddWorkSpace(_ sender: Any) {
//        self.dissmissView()
//        addWorkspaceBtn.isUserInteractionEnabled = false
        data.insert("Add Workspace", at: 0)
       // let numberOfCell = data.count
        let indexPath = IndexPath(row: 0, section: 0)
            tableView.beginUpdates()
        tableView.insertRows(at: [indexPath], with: .automatic)
        tableView.endUpdates()
            view.endEditing(true)
        self.backViewHeight.constant = CGFloat(45*(data.count)) + 100
        if self.backViewHeight.constant > self.view.frame.height*0.75 {
            self.backViewHeight.constant = self.view.frame.height*0.75
        }
        self.tableView.reloadData()
        self.tableView(self.tableView, didSelectRowAt: indexPath)
    }
    @IBAction func closeView(_ sender: Any) {
        
        self.animHide()
    }
    
    func dissmissView(){
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveLinear],
                       animations: {
                        self.backView.center.y += self.backView.bounds.height
                        self.backView.layoutIfNeeded()
                        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)

        },  completion: {(_ completed: Bool) -> Void in
            self.backView.isHidden = true
            self.dismiss(animated: false) {
                NotificationCenter.default.post(name: NSNotification.Name("Add_New_Worspace"), object: [:], userInfo: nil)
            }
        })
    }
    
    
}
extension UIView{
    func roundedToptCornersOfButton(){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
            byRoundingCorners: [.topLeft , .topRight],
            cornerRadii: CGSize(width: 8, height: 8))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
}

