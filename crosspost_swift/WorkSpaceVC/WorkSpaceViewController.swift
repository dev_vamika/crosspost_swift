//
//  WorkSpaceViewController.swift
//  crosspost_swift
//
//  Created by vamika on 24/09/20.
//  Copyright © 2020 vamika. All rights reserved.
//

import UIKit
protocol WorkSpaceViewControllerDelegate: class {
    func changeWorkspace()
    func newWorkSpaceAddedSuccessfully()
}
class WorkSpaceViewController: UIViewController {
    weak var delegate: WorkSpaceViewControllerDelegate?
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var backView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpObserver()
        Global.addShadowToView(view: backView)
        backView.roundedCornersToView()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Global.zoonInAnimationOnView(view: backView)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.backView.backgroundColor = UIColor(named: "CELL_BG_COLOR")
        self.saveButton.backgroundColor = UIColor(named: "APP_BLUE_COLOR")
        self.saveButton.setTitleColor(UIColor(named: "HASH_BG_COLOR"), for: .normal)
    }
    private func setUpObserver() {


    }
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.dissmissView()
    }
    func dissmissView(){
            UIView.animate(withDuration: 0.3) {
                self.backView.transform = CGAffineTransform(scaleX: 0.1,y: 0.1);
            } completion: { (finished) in
                self.backView.transform = CGAffineTransform(scaleX: 0.0,y: 0.0);
                self.dismiss(animated: false, completion: nil)
            }
    }
    @IBAction func onSave(_ sender: Any) {
        dissmissView()
       
    }
}
extension UIView{
    func roundedCornersToView(){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.topLeft , .topRight, .bottomRight, .bottomLeft],
            cornerRadii: CGSize(width: 8, height: 8))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
}
