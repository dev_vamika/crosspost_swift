//
//  ZoomPreviewViewController.swift
//  crosspost_swift
//
//  Created by vamika on 24/09/20.
//  Copyright © 2020 vamika. All rights reserved.
//

import UIKit

class ZoomPreviewViewController: UIViewController{
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var logoLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    var mediaArr = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "ZoomCollectionViewCell", bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: "ZoomCollectionViewCell")
        
//        let nib2 = UINib(nibName: "PostRepostCollectionCell", bundle: nil)
//        self.collectionView.register(nib2, forCellWithReuseIdentifier: "PostRepostCollectionCell")
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.backgroundColor = UIColor(named: "APP_BG_COLOR")
        self.backBtn.setTitleColor(UIColor(named: "APP_BLUE_COLOR"), for: .normal)
        self.logoLbl.textColor = UIColor(named: "APP_BLUE_COLOR")
//        scrollView.frame = CGRect(x: 0, y: self.topView.frame.height, width: self.view.frame.size.width, height: self.view.frame.size.height - self.topView.frame.height)
//        imageView.frame = CGRect(x: 10, y: self.scrollView.frame.origin.y, width: self.scrollView.frame.size.width-20, height: self.scrollView.frame.size.height)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        pageController.numberOfPages = mediaArr.count
        if mediaArr.count < 2 {
            pageController.isHidden = true
        }
    }
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}
extension ZoomPreviewViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    // MARK: CollectiontionDelegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mediaArr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let dic = mediaArr[indexPath.row] as! NSDictionary
        if dic[IS_VIDEO] as! String == YES_STRING{
        let titleCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        let cell = PostRepostViewController(nibName: "PostRepostViewController", bundle: nil)
        cell.view.frame = CGRect(x: 0, y: 0, width: self.collectionView.bounds.width, height: self.collectionView.bounds.height)
        cell.index = indexPath
        cell.mediaDict = mediaArr[indexPath.row] as! NSDictionary;
        cell.setValues()
            cell.zoomIcon.isHidden = true
        addChild(cell)
        titleCell.addSubview(cell.view)
        return titleCell
        }
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "ZoomCollectionViewCell", for: indexPath) as! ZoomCollectionViewCell
        cell.mediaDict = mediaArr[indexPath.row] as! NSDictionary;
        cell.setImageOnMediaImageView()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cellSize = CGSize(width:self.collectionView.bounds.width , height: self.collectionView.bounds.height)
        return cellSize
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        let sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return sectionInset
    }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath){
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if pageController?.currentPage != Int(scrollView.contentOffset.x) / Int(scrollView.frame.width){
            let dic = mediaArr[pageController!.currentPage] as! NSDictionary
            if dic[IS_VIDEO] as! String == YES_STRING{
            PostRepostViewController.init().removewObserver()
            self.collectionView.reloadItems(at: [IndexPath.init(row: pageController!.currentPage, section: 0)])
            }
        }
        pageController?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        pageController?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
}
