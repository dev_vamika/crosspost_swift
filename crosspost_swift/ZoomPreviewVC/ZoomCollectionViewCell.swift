//
//  ZoomCollectionViewCell.swift
//  crosspost_swift
//
//  Created by vamika on 21/10/20.
//  Copyright © 2020 vamika. All rights reserved.
//

import UIKit

class ZoomCollectionViewCell: UICollectionViewCell,UIScrollViewDelegate {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imagePreview: UIImageView!
    var mediaDict = NSDictionary()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.scrollView.minimumZoomScale = 1.0
                self.scrollView.maximumZoomScale = 4.0
                self.scrollView.zoomScale = 1.0
                self.scrollView.delegate = self
        // Initialization code
    }
    func setImageOnMediaImageView(){
        self.imagePreview.image = self.mediaDict[DISPLAY_PRIVIEW] as? UIImage
    }
        func viewForZooming(in scrollView: UIScrollView) -> UIView? {
            return self.imagePreview
        }
}
