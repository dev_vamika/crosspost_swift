//
//  IntroViewController.m
//  Cross Post
//
//  Created by Mayank Barnwal on 29/04/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "IntroViewController.h"
#import "WOPageControl.h"

@interface IntroViewController ()
@property (nonatomic, strong) WOPageControl *pageControl;
@end

@implementation IntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self makeCornerRadius:self.plusLabel Corner:3.0];
    
    self.pageControl = [[WOPageControl alloc] initWithFrame:CGRectMake(0, 0,self.customPageView.frame.size.width , self.customPageView.frame.size.height)];
    [self.customPageView addSubview:self.pageControl];
    
    self.pageControl.cornerRadius = 5;
    self.pageControl.dotHeight = 10;
    self.pageControl.dotSpace = 10;
    self.pageControl.currentDotWidth = 10;
    self.pageControl.otherDotWidth = 10;
    self.pageControl.otherDotColor = [UIColor colorWithRed:66/255.0 green:150/255.0 blue:180/255.0 alpha:1.0];
    self.pageControl.currentDotColor = [UIColor whiteColor];
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages = 4;
    
    [self.containerCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
}

-(void) makeCornerRadius:(UIView*) view Corner:(float) radius{
    view.layer.cornerRadius = radius;
    view.layer.masksToBounds = true;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)theSectionIndex {
    self.pageControl.numberOfPages = 4;
    return 4;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *titleCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSArray *arr = [titleCell subviews];
    
    for (UIView *v in arr) {
        [v removeFromSuperview];
    }
    
    NSString *string = @"";
    
    switch (indexPath.row) {
        case 0:
            string = @"Repost more than 60 posts\nin a month";
            break;
        case 1:
            string = @"Remove water mark for\ncredits";
            break;
        case 2:
            string = @"No ads no interuption";
            break;
        case 3:
            string = @"Unlimited history about your\nreposted content";
            break;
            
        default:
            break;
    }
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, titleCell.frame.size.height-58, titleCell.frame.size.width, 58)];
    
    label.font = [UIFont fontWithName:@"Karla-Bold" size:20.0];
    
    label.numberOfLines = 0;
    
    label.textAlignment = NSTextAlignmentCenter;
    
    label.text = string;
    
    label.textColor = [UIColor whiteColor];
    
    [titleCell addSubview:label];
    
    return titleCell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return self.containerCollectionView.bounds.size;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = self.containerCollectionView.frame.size.width;
    
    float currentPage = self.containerCollectionView.contentOffset.x / pageWidth;
    
    if (0.0f != fmodf(currentPage, 1.0f)){
        self.pageControl.currentPage = currentPage + 1;
    }
    else{
        self.pageControl.currentPage = currentPage;
    }
}



@end
