//
//  IntroViewController.h
//  Cross Post
//
//  Created by Mayank Barnwal on 29/04/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface IntroViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *plusLabel;
@property (weak, nonatomic) IBOutlet UIView *customPageView;
@property (weak, nonatomic) IBOutlet UICollectionView *containerCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *crosspostLabel;
@property (weak, nonatomic) IBOutlet UIButton *noThanksBtn;
@property (weak, nonatomic) IBOutlet UIButton *try7DaysFreeBtn;

@end
