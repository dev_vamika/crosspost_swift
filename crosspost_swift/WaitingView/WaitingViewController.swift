//
//  WaitingViewController.swift
//  crosspost_swift
//
//  Created by vamika on 15/09/20.
//  Copyright © 2020 vamika. All rights reserved.
//

import UIKit

class WaitingViewController: UIViewController {
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var backView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Global.makeCornerRadius(view: backView, radius: 10.0)
        // Do any additional setup after loading the view.
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        self.messageLabel.textColor = Global.getTintColor()
        self.loader.color = Global.getTintColor()
    }


}
