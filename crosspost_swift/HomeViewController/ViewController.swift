//
//  ViewController.swift
//  crosspost_swift
//
//  Created by vamika on 05/08/20.
//  Copyright © 2020 vamika. All rights reserved.
//

import UIKit
import SideMenu

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, PurchaseHandlerDelegate{
    var cardHeight:CGFloat  {
        return self.view.frame.height * 0.6
    }
    var cardHandleAreaHeight:CGFloat = 100
    
    enum CardState {
        case expanded
        case collpased
    }

    var cardVisible = false
    var nextState:CardState {
        return cardVisible ? .collpased : .expanded
    }
    
    
    // Animation
    var animations:[UIViewPropertyAnimator] = []
    var currentAnimationProgress:CGFloat = 0
    var animationProgressWhenIntrupped:CGFloat = 0
    
    
    // visual effects
    
    var visualEffectView: UIVisualEffectView!
    
    
//    @IBOutlet weak var gifView: UIImageView!
    @IBOutlet var clipBoardHeight: NSLayoutConstraint!
    @IBOutlet var saveBoardHeight: NSLayoutConstraint!
    
    @IBOutlet weak var settingButton: UIButton!
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var showMeButton: UIButton!
    @IBOutlet weak var howitworksLabel: UILabel!
    @IBOutlet weak var htwDescriptionLabel: UILabel!
    @IBOutlet weak var screenLogoLabel: UILabel!
    @IBOutlet weak var downImage: UIImageView!
    
    
    @IBOutlet var clipTitleHeight: NSLayoutConstraint!
    @IBOutlet weak var clipboardTitleLabel: UILabel!
    @IBOutlet weak var savedTitleLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var remainingCount: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var searchTable: UITableView!
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var hashButton: UIButton!
    @IBOutlet weak var userTagButton: UIButton!
    @IBOutlet weak var goProBtn: UIButton!
    @IBOutlet weak var gridButton: UIButton!
    @IBOutlet weak var listButton: UIButton!
    @IBOutlet weak var userNameLbl: UIButton!
    @IBOutlet weak var seeAllClipboardsButton: UIButton!
    @IBOutlet weak var seeAllSavedPostButton: UIButton!
    @IBOutlet weak var hwoWorkBackView: UIView!
    @IBOutlet weak var clipboardCollection: UICollectionView!
    @IBOutlet weak var savedCollection: UICollectionView!
    
    @IBOutlet weak var guidePopUpViewHeight: NSLayoutConstraint!
    @IBOutlet weak var goToInstagramButton: UIButton!
    @IBOutlet weak var noMediaView: UIView!
//    @IBOutlet weak var warningBackView: UIView!
    @IBOutlet weak var searchTypeImage: UIImageView!
    @IBOutlet weak var searchBtnView: UIView!
    @IBOutlet weak var searchBarBackView: UIView!
    @IBOutlet weak var backBtnWidth: NSLayoutConstraint!
    @IBOutlet weak var backBtn: UIButton!
    var savedDataArray = NSMutableArray()
    var postedDataArray = NSMutableArray()
    var cellsDictionary = NSMutableDictionary()

    var usernamePostsArray = NSMutableArray()
    var instagramUserDic = NSMutableDictionary()
    var hashtagPostsArray = NSMutableArray()
    var searchedArray = NSMutableArray()
    
    @IBOutlet weak var cancelButton: UIButton!
    var introViewController = IntroViewController()
//    var introViewController = TutorialViewController()
    var purchaseViewController = PurchaseViewController()
    @IBOutlet weak var searchButton: UIButton!
    
//    var dropDownViewController:DropDownViewController!
    var purchaseSuccessViewController = PurchaseSuccessViewController()
    var dropDownTableView = DropDownViewController()
    var tutorialView = TutViewController()
    var db = DBManager()
    var isDataLoading = false
    var haveNextPage = false
    var searchString = ""
    var instagramId = ""
    var urlToBeUsed = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.bool(forKey: SHOW_GUIDE_POUP){
            self.guidePopUpViewHeight.constant = 0
        }
        UserDefaults.standard.set(false, forKey: "isFromSerach")
        self.backBtnWidth.constant = 0.0
//        goProBtn.isHidden = true
//        remainingCount.isHidden = true
        searchTable.isHidden = true
        searchBtnView.isHidden = true
        searchButton.isHidden = true
        cancelButton.isHidden = true
     //   warningBackView.isHidden = true
        searchBar.delegate = self
        UIView.appearance().isExclusiveTouch = true
        let gridViewNib = UINib(nibName: "GridViewCell", bundle: nil)
        let listViewNib = UINib(nibName: "ListViewCell", bundle: nil)
        clipboardCollection.register(gridViewNib, forCellWithReuseIdentifier: "GridViewCell")
        clipboardCollection.register(listViewNib, forCellWithReuseIdentifier: "ListViewCell")
        savedCollection.register(gridViewNib, forCellWithReuseIdentifier: "GridViewCell")
        savedCollection.register(listViewNib, forCellWithReuseIdentifier: "ListViewCell")
        _ = UINib(nibName: "PostRepostCollectionCell", bundle: nil)
       
        clipboardCollection.delegate = self
        clipboardCollection.dataSource = self
        savedCollection.delegate = self
        savedCollection.dataSource = self
        
        self.hashButton.roundedLeftCornersOfButton()
        self.userTagButton.roundedRightCornersOfButton()
        let yourAttributes: [NSAttributedString.Key: Any] = [
            .font: remainingCount.font,
            .foregroundColor: remainingCount.textColor ?? UIColor.gray,
              .underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: "Go Pro",
                                                             attributes: yourAttributes)
        goProBtn.setAttributedTitle(attributeString, for: .normal)
        onClickUserTag(userTagButton)
        self.perform(#selector(showIntroScreen), with: nil, afterDelay: 0.3)
//        self.deselectAllTut()
//        self.tutOne.isHidden = false
//        self.instructionLbl.isHidden = false
//        pageController.currentPage = 0
//        self.tutOne.isHidden = false
//        self.instructionLbl.isHidden = false
//        self.tutOne.setBackgroundImage(UIImage(named: "go_to_ig_icon.png"), for: .normal)
//        self.instructionLbl.text = "Press on\n Instagram button\n to open Instagram App"
//        let point1 = CGPoint(x: self.instructionLbl.frame.origin.x + 10, y: self.instructionLbl.frame.origin.y + self.instructionLbl.frame.height);
//        let point2 = CGPoint(x: 50, y: (self.instructionLbl.frame.origin.y + self.tutOne.frame.origin.y)/2);
//        let point3 = CGPoint(x: self.tutOne.frame.origin.x - 10, y: self.tutOne.frame.origin.y + 20);
//        self.createOpenArrowPath(from: point1, to: point3, conftrolPoint: point2)
        reloadDataFromDB()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        self.deselectAllTut()
        gridButton.isHidden = false
        Global.makeCornerRadius(view: searchButton, radius: 5.0)
        Global.addShadowAtBottom(view: topView)
        Global.makeCornerRadius(view: searchBarBackView, radius: 5.0)
        Global.makeCornerRadius(view: hwoWorkBackView, radius: 5.0)
        let color = UIColor(named: "APP_BLUE_COLOR")
        self.userNameLbl.setTitleColor(color, for: .normal)
        self.seeAllClipboardsButton.setTitleColor(color, for: .normal)
        self.seeAllSavedPostButton.setTitleColor(color, for: .normal)
        self.showMeButton.setTitleColor(color, for: .normal)
        self.dismissButton.setTitleColor(color, for: .normal)
        self.howitworksLabel.textColor = color
        self.htwDescriptionLabel.textColor = color
        self.clipboardTitleLabel.textColor = color
        self.savedTitleLabel.textColor = color
        self.screenLogoLabel.textColor = color
        self.searchBar.textColor = color
        self.searchBarBackView.backgroundColor = UIColor(named: "CELL_BG_COLOR")
        self.searchBtnView.backgroundColor = UIColor(named: "HASH_BG_COLOR")
        self.view.backgroundColor = UIColor(named: "APP_BG_COLOR")
        self.searchTable.backgroundColor = UIColor(named: "APP_BG_COLOR")
        self.scrollView.backgroundColor = UIColor(named: "APP_BG_COLOR")
        self.settingButton.setImage(UIImage(named: "setting"), for: .normal)
        self.downImage.image = UIImage(named: "down")
        self.backBtn.setImage(UIImage(named: "back"), for: .normal)
//        Global.addShadowToView(view: noMediaSubBackView)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.isUserInteractionEnabled = true
        clipboardCollection.isUserInteractionEnabled = true;
        savedCollection.isUserInteractionEnabled = true;
        self.navigationController?.navigationBar.isHidden = true
        NotificationCenter.default.addObserver(self,selector: #selector(reloadData(notification:)),name: NSNotification.Name("ReloadData"),object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(onClickWorSpaceButton(notification:)),name: NSNotification.Name("Add_Worspace"),object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(onAddingNewkWorSpace(notification:)),name: NSNotification.Name("Add_New_Worspace"),object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(appEnterInForeground),name: UIApplication.willEnterForegroundNotification,object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(refreshView),name: NSNotification.Name("refreshView"),object: nil)
        
        searchButton.isHidden = true
        cancelButton.isHidden = true
//        gifView.loadGif(name: "cross_post")
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if Global.isPaidUser() {
            self.remainingCount.isHidden = true
            self.goProBtn.isHidden = true
            return
        }
        updateSearchHistory()
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("ReloadData"), object: nil)
//        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        
    }
    func loadTutView(){
        tutorialView = TutViewController()
        tutorialView = TutViewController.init(nibName: "TutViewController", bundle: nil)
        tutorialView.view.frame = noMediaView.bounds
        tutorialView.closeBtn.addTarget(self, action: #selector(onCloseBtn(_:)), for: .touchUpInside)
        addChild(tutorialView)
        self.noMediaView.addSubview(tutorialView.view)
    }
    @objc func onCloseBtn(_ sender: Any?) {
        
        if tutorialView.view != nil {
            if !self.noMediaView.isHidden{
                self.noMediaView.isHidden = true
                self.onCloseBtn(UIButton())
                self.backBtnWidth.constant = 0.0
                self.backBtn.isUserInteractionEnabled = false
                if tutorialView.pageController.currentPage == 3{
                    UserDefaults.standard.set(true, forKey: SHOW_GUIDE_POUP)
                    self.guidePopUpViewHeight.constant = 0
                }
            }
            UIView.animate(withDuration: 0.35, animations: { [self] in
                tutorialView.view.alpha = 0.0
            }) { [self] finished in
                tutorialView.view.alpha = 0.0
                tutorialView.view.removeFromSuperview()
                tutorialView = TutViewController()
            }
        }
    }
    @objc func refreshView() {
        self.reloadDataFromDB()
    }
    @objc func showIntroScreen() {
        let appSesseionCount = UserDefaults.standard.integer(forKey: APP_SESSION_COUNT)

        UserDefaults.standard.set(appSesseionCount + 1, forKey: APP_SESSION_COUNT)
        UserDefaults.standard.synchronize()
   
        if !(Global.isPaidUser()) {
            if appSesseionCount == 14 {
                UserDefaults.standard.set(1, forKey: APP_SESSION_COUNT)
                UserDefaults.standard.synchronize()
                print("\ninitialiseIntroView initialiseIntroView initialiseIntroView initialiseIntroView initialiseIntroView initialiseIntroView initialiseIntroView\n")
                print(appSesseionCount)
                initialiseIntroView()
            }
        }
        if (appSesseionCount == 10) && (appSesseionCount != 0) {
            print("\nopenRateUsPopUp openRateUsPopUp openRateUsPopUp openRateUsPopUp openRateUsPopUp openRateUsPopUp openRateUsPopUp\n")
            print(appSesseionCount)
            Global.openRateUsPopUp()
        }
    }
   @objc func appEnterInForeground() {

        updateSearchHistory()

        showIntroScreen()
    }

    func updateSearchHistory() {
        let searchDate  = UserDefaults.standard.value(forKey: SEARCH_DATE) as? NSDate
        if searchDate != nil {
            let date = NSDate()
            let secondsBetween = date.timeIntervalSince(searchDate! as Date)
            print("\nsecondsBetween secondsBetween secondsBetween secondsBetween secondsBetween secondsBetween secondsBetween\n")
            print(secondsBetween)
            if secondsBetween > 3600 {
                UserDefaults.standard.setValue([AnyHashable](), forKey: SEARCHED_STRINGS)

                UserDefaults.standard.synchronize()
            }
        }
       
        if UserDefaults.standard.value(forKey: SEARCHED_STRINGS) != nil {
            let arr = UserDefaults.standard.value(forKey: SEARCHED_STRINGS) as? [AnyHashable]
            self.searchedArray = NSMutableArray(array: arr!)
        }
        setRemainingCountLabel()
    }
    @IBAction func goToInstagram(_ sender: Any) {
        if UIApplication.shared.canOpenURL(URL(string: "instagram://")!) {
            UIApplication.shared.open(URL(string: "instagram://")!, options: [:], completionHandler: nil)
        }else if UIApplication.shared.canOpenURL(URL(string: "https://instagram.com")!) {
            UIApplication.shared.open(URL(string: "https://instagram.com")!, options: [:], completionHandler: nil)
        }
    }
    @IBAction func cancelButton(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "isFromSerach")
        self.gridButton.isHidden = false
        searchBar.text = ""
        searchTable.isHidden = true
        searchButton.isHidden = true
        cancelButton.isHidden = true
        searchBar.resignFirstResponder()
        searchTypeImage.image = UIImage.init(named: "seach_icon.png.png")
    }
    @IBAction func goProClick(_ sender: Any) {
        self.initialisePurchaseViewController()
    }
    @IBAction func onSearch(_ sender: Any) {
        self.onSearchBtn()
    }
    @IBAction func oCLickUserNameButton(_ sender: Any) {
        self.performSegue(withIdentifier: "workspace", sender: nil)
    }
    @objc func onClickWorSpaceButton(notification: Notification) {
        self.performSegue(withIdentifier: "workspace", sender: nil)
    }
    @objc func onAddingNewkWorSpace(notification: Notification) {
        self.performSegue(withIdentifier: "addWorkspace", sender: nil)
    }
    
    func reloadDataFromDB() {
        postedDataArray.removeAllObjects()
        savedDataArray.removeAllObjects()
        if UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE) as? String == nil {
            self.userNameLbl.setTitle("Add Workspace", for: .normal)//
        }else{
            self.userNameLbl.setTitle(String(format: "@%@",UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE) as! CVarArg), for: .normal)
        }
        if self.userNameLbl.titleLabel?.text != "Add Workspace"  {
            let workspace_id = UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE_ID) as? String
            postedDataArray = db.getDataForField(DATA_TABLE, where: String(format: "%@= '%@' AND %@= '%@'",IS_POSTED,YES_STRING,WORK_SPACE_NAME_ID,workspace_id!))
            savedDataArray = db.getDataForField(DATA_TABLE, where: String(format: "%@= '%@'  AND %@= '%@'",IS_POSTED,NO_STRING,WORK_SPACE_NAME_ID,workspace_id!))
        }else{
            postedDataArray = db.getDataForField(DATA_TABLE, where: String(format: "%@= '%@'",IS_POSTED,YES_STRING))
            savedDataArray = db.getDataForField(DATA_TABLE, where: String(format: "%@= '%@'",IS_POSTED,NO_STRING))
        }
        if postedDataArray.count > 1  {
            let mutableArray = postedDataArray.reverseObjectEnumerator().allObjects
            postedDataArray = NSMutableArray(array: mutableArray)
        }
        if savedDataArray.count > 1  {
            let mutableArray = savedDataArray.reverseObjectEnumerator().allObjects
            savedDataArray = NSMutableArray(array: mutableArray)
        }
        let appSesseionCount = UserDefaults.standard.integer(forKey: "APP_SESSION_COUNT")
        if appSesseionCount != 0{
            self.noMediaView.isHidden = true
        }else{
            UserDefaults.standard.set(1, forKey: "APP_SESSION_COUNT")
            self.noMediaView.isHidden = false
            self.loadTutView()
        }
        
        DispatchQueue.main.async(execute: {
            if UserDefaults.standard.value(forKey: HOME_SCREEN_VIEW_TYPE) != nil {
                if UserDefaults.standard.value(forKey: HOME_SCREEN_VIEW_TYPE) as? String == "grid" {
                    self.refreshGrid()
                }else{
                    self.refreshList()
                }
            }else{
                self.refreshList()
            }
        })
        if !self.searchTable.isHidden && !UserDefaults.standard.bool(forKey: "isFromSerach"){
            self.searchTable.isHidden = true
        }else if !self.searchTable.isHidden{
            self.searchBtnView.isHidden = false
            self.searchButton.isHidden  = false
            self.cancelButton.isHidden  = false
        }
        
    }
    
    
    @IBAction func onBack(_ sender: Any) {
       
    }
    
    @IBAction func onClickSettings(_ sender: Any) {
        //self.view.isUserInteractionEnabled = false
        let menu = storyboard!.instantiateViewController(withIdentifier: "SideMenuNavigationController") as! SideMenuNavigationController
        present(menu, animated: true, completion: nil)
//        let storyboard = UIStoryboard (name: "Main", bundle: nil)
//        let resultVC = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
//        self.navigationController?.pushViewController(resultVC, animated: true)
    }
    @objc func reloadData(notification: Notification) {
        reloadDataFromDB()
        let postDict = notification.object as? NSDictionary
            if postDict != nil {
                Global.removeWaitingView()
                performSegue(withIdentifier: "openPreview", sender: postDict)
            } else {
                Global.removeWaitingView()
            }
        }
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.identifier == "openPreview" {
              
                if let nextViewController = segue.destination as? RepostViewController {
                    nextViewController.postDictionary = sender as! NSDictionary
                }
            }
            else if segue.identifier == "addWorkspace"{
                if let nextViewController = segue.destination as? WorkSpaceViewController {
                    nextViewController.delegate = self
                }
            }
            else if segue.identifier == "workspace"{
                if let nextViewController = segue.destination as? DropDownViewController {
                    nextViewController.delegate = self
                }
            }
            else if segue.identifier == "seeAll"{
                if let nextViewController = segue.destination as? SeeAllPostsVC {
                    nextViewController.type = sender as? String
                }
            }
            
        }
}
extension ViewController: WorkSpaceViewControllerDelegate, DropDownViewControllerDelegate{
    func changeWorkspace() {
        reloadDataFromDB()
    }
    func onAddNewWorspace() {
        self.performSegue(withIdentifier: "addWorkspace", sender: nil)
    }
    func newWorkSpaceAddedSuccessfully() {
        reloadDataFromDB()
    }
}
extension ViewController{
    // MARK: CollectiontionDelegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == clipboardCollection {
            if self.savedDataArray.count == 0 {
                return self.gridButton.isSelected ? 3 : 1
            }
            return self.savedDataArray.count > 10 ? 10 : self.savedDataArray.count
        }
        else if collectionView == savedCollection {
            if self.postedDataArray.count == 0 {
                return self.gridButton.isSelected ? 3 : 1
            }
            return self.postedDataArray.count > 10 ? 10 : self.postedDataArray.count
        }
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == clipboardCollection {
            if self.gridButton.isSelected{
                let cell = self.clipboardCollection.dequeueReusableCell(withReuseIdentifier: "GridViewCell", for: indexPath) as! GridViewCell
                if self.savedDataArray.count == 0 {
                    cell.noMedia.isHidden = false
                }else{
                    cell.noMedia.isHidden = true
                    cell.postDict = self.savedDataArray[indexPath.row] as! NSDictionary
                    cell.setImageOnMediaImageView()
                }
                return cell
            }else{
                
                let cell = self.clipboardCollection.dequeueReusableCell(withReuseIdentifier: "ListViewCell", for: indexPath) as! ListViewCell
                if self.savedDataArray.count != 0 {
                    cell.noMedia.isHidden = true
                cell.postDict = self.savedDataArray[indexPath.row] as! NSDictionary
                cell.setImageOnMediaImageView()
                }else{
                    cell.noMedia.isHidden = false
                }
                return cell
            }
        }
        else{
            if self.gridButton.isSelected{
                let cell = self.savedCollection.dequeueReusableCell(withReuseIdentifier: "GridViewCell", for: indexPath) as! GridViewCell
                if self.postedDataArray.count == 0 {
                    cell.noMedia.isHidden = false
                }else{
                    cell.noMedia.isHidden = true
                    cell.postDict = self.postedDataArray[indexPath.row] as! NSDictionary
                    cell.setImageOnMediaImageView()
                }
                return cell
            }else{
                
                let cell = self.savedCollection.dequeueReusableCell(withReuseIdentifier: "ListViewCell", for: indexPath) as! ListViewCell
                if self.postedDataArray.count != 0 {
                    cell.noMedia.isHidden = true
                cell.postDict = self.postedDataArray[indexPath.row] as! NSDictionary
                cell.setImageOnMediaImageView()
                }else{
                    cell.noMedia.isHidden = false
                }
                return cell
            }
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.isUserInteractionEnabled = false;
        if collectionView == clipboardCollection {
            if self.savedDataArray.count == 0 {
                collectionView.isUserInteractionEnabled = true;
                self.goToInstagram(self.goToInstagramButton as Any)
            }else{
            performSegue(withIdentifier: "openPreview", sender: self.savedDataArray[indexPath.row])
            }
        }else if collectionView == savedCollection{
            if self.postedDataArray.count == 0 {
                collectionView.isUserInteractionEnabled = true;
                self.performSegue(withIdentifier: "addWorkspace", sender: nil)
            }else{
            performSegue(withIdentifier: "openPreview", sender: self.postedDataArray[indexPath.row])
            }
        }else{
            collectionView.isUserInteractionEnabled = true;
        }
         
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
         if self.gridButton.isSelected{
            if collectionView == clipboardCollection{
                let cellSize = CGSize(width:self.clipboardCollection.frame.width/2.5 , height: clipBoardHeight.constant)
                return cellSize
            }else{
                let cellSize = CGSize(width:(self.savedCollection.frame.width/2.5) , height: saveBoardHeight.constant)
            return cellSize
            }
         }
        else if self.listButton.isSelected{
        let cellSize = CGSize(width:self.clipboardCollection.bounds.width , height: 130)
        return cellSize
        }
        return CGSize(width: 0, height: 0)
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        if collectionView == tutCollectionView{
//         if (indexPath.row == 4 ) {
//            
//         }
//        }
    }
    func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        if self.listButton.isSelected {
            let dbObject = DBManager()
            if collectionView == clipboardCollection {
                if self.savedDataArray.count != 0 {
                let postDict = self.savedDataArray[indexPath.row] as! NSDictionary
                dbObject.deleteRow(DATA_TABLE, where: "\(LINK_ID) = '\((postDict[LINK_ID])!)' AND \(WORK_SPACE_NAME_ID) = '\((postDict[WORK_SPACE_NAME_ID])!)'")
                }
            }else if collectionView == savedCollection{
                if self.postedDataArray.count != 0 {
                let postDict = self.postedDataArray[indexPath.row] as! NSDictionary
                dbObject.deleteRow(DATA_TABLE, where: "\(LINK_ID) = '\((postDict[LINK_ID])!)' AND \(WORK_SPACE_NAME_ID) = '\((postDict[WORK_SPACE_NAME_ID])!)'")
                }
            }
            reloadDataFromDB()
        }else{
            if action == #selector(UIResponderStandardEditActions.delete(_:)) {
                let dbObject = DBManager()
                if collectionView == clipboardCollection {
                    if self.savedDataArray.count != 0 {
                        let postDict = self.savedDataArray[indexPath.row] as! NSDictionary
                        dbObject.deleteRow(DATA_TABLE, where: "\(LINK_ID) = '\((postDict[LINK_ID])!)' AND \(WORK_SPACE_NAME_ID) = '\((postDict[WORK_SPACE_NAME_ID])!)'")
                        self.savedDataArray.removeObject(at: indexPath.row)
                    }
                }else if collectionView == savedCollection{
                    if self.postedDataArray.count != 0 {
                        let postDict = self.postedDataArray[indexPath.row] as! NSDictionary
                        dbObject.deleteRow(DATA_TABLE, where: "\(LINK_ID) = '\((postDict[LINK_ID])!)' AND \(WORK_SPACE_NAME_ID) = '\((postDict[WORK_SPACE_NAME_ID])!)'")
                        self.postedDataArray.removeObject(at: indexPath.row)
                    }
                }
                reloadPreviewType()
                return
            }
            self.collectionView(collectionView, performAction: action, forItemAt: indexPath, withSender: sender)
        }
//        groceryList.remove(at: indexPath.row)
//        collectionView.deleteItems(at: [indexPath])
      }
    func reloadPreviewType(){
        DispatchQueue.main.async(execute: {
            if UserDefaults.standard.value(forKey: HOME_SCREEN_VIEW_TYPE) != nil {
                if UserDefaults.standard.value(forKey: HOME_SCREEN_VIEW_TYPE) as? String == "grid" {
                    self.onClickGrid(self.gridButton)
                }else{
                    self.onClickList(self.listButton)
                }
            }else{
                self.onClickList(self.listButton)
            }
        })
    }
    func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        if self.gridButton.isSelected {
            if action == #selector(UIResponderStandardEditActions.delete(_:)) {
                return true
            }
        }
            return super.canPerformAction(action, withSender: sender)
        
        }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    }
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
    }

}
extension ViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.userTagButton.isSelected {
           return self.usernamePostsArray.count
        }else{
           return self.hashtagPostsArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: ListTableViewCell!
        if let cellView = searchTable.dequeueReusableCell(withIdentifier: "ListTableViewCell") as? ListTableViewCell {
           cell = cellView
        }else{
            searchTable.register(UINib(nibName: "ListTableViewCell", bundle: nil), forCellReuseIdentifier: "ListTableViewCell")
            cell = searchTable.dequeueReusableCell(withIdentifier: "ListTableViewCell") as? ListTableViewCell
        }
        var dic = NSDictionary()
        if self.userTagButton.isSelected {
            dic = self.usernamePostsArray[indexPath.row] as! NSDictionary
            cell.postDict = dic
            cell.setValuesinCell(type: "user")
        }else{
            dic = self.hashtagPostsArray[indexPath.row] as! NSDictionary
            cell.postDict = dic
            cell.setValuesinCell(type: "hash")
        }
        
        return cell
}
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UserDefaults.standard.set(true, forKey: "isFromSerach")
        if self.userTagButton.isSelected{
            let postDict = usernamePostsArray[indexPath.row] as! NSDictionary
            let stringToCopy = String(format:"https://www.instagram.com/p/%@",postDict[LINK_ID] as! CVarArg)
            let app = SceneDelegate()
            app.fetchData(urlString: stringToCopy)
            Global.addWaitingView(waitingViewLabel: "Getting data...")
        }
        else{
            let postDict = hashtagPostsArray[indexPath.row] as! NSDictionary
            let stringToCopy = String(format:"https://www.instagram.com/p/%@",postDict[LINK_ID] as! CVarArg)
            let app = SceneDelegate()
            app.fetchData(urlString: stringToCopy)
            Global.addWaitingView(waitingViewLabel: "Getting data...")
        }
    }
    
}
//HeaderTableViewCellDelegate
extension ViewController{
    
    @IBAction func onSeeAllClipboard(_ sender: UIButton) {
        self.view.isUserInteractionEnabled = false
        performSegue(withIdentifier: "seeAll", sender: "clipboard")
    }
    @IBAction func onSeeAllSaved(_ sender: UIButton) {
        self.view.isUserInteractionEnabled = false
        performSegue(withIdentifier: "seeAll", sender: "saved")
    }
    func refreshGrid(){
        self.savedCollection.isScrollEnabled = true
        self.clipboardCollection.isScrollEnabled = true
        if self.gridButton.isSelected {
           
            clipBoardHeight.constant = 0.01
            saveBoardHeight.constant = 0.01
            if isiPad{
                clipBoardHeight.constant = self.view.frame.height*0.4
                saveBoardHeight.constant = self.view.frame.height*0.4
            }
            else if isiPhone6 || isiPhone6Plus {
                clipBoardHeight.constant = self.view.frame.height*0.35
                saveBoardHeight.constant = self.view.frame.height*0.35
            }else{
                clipBoardHeight.constant = self.view.frame.height*0.29
                saveBoardHeight.constant = self.view.frame.height*0.29
            }
            
            clipboardCollection.reloadData()
            savedCollection.reloadData()
            return
        }
        UserDefaults.standard.setValue("grid", forKey: HOME_SCREEN_VIEW_TYPE)
        self.gridButton.isSelected = true
        self.listButton.isSelected = false
        if let flowLayout = clipboardCollection.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
        }
        if let flowLayout = savedCollection.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
        }
        clipBoardHeight.constant = 0.01
        saveBoardHeight.constant = 0.01
        if isiPad{
            clipBoardHeight.constant = self.view.frame.height*0.4
            saveBoardHeight.constant = self.view.frame.height*0.4
        }
        else if isiPhone6 || isiPhone6Plus {
            clipBoardHeight.constant = self.view.frame.height*0.35
            saveBoardHeight.constant = self.view.frame.height*0.35
        }
        else{
            clipBoardHeight.constant = self.view.frame.height*0.29
            saveBoardHeight.constant = self.view.frame.height*0.29
        }
       
        clipboardCollection.reloadData()
        savedCollection.reloadData()
    }
    func refreshList(){
        self.savedCollection.isScrollEnabled = false
        self.clipboardCollection.isScrollEnabled = false
        if self.listButton.isSelected {
            clipBoardHeight.constant = 0.01
            saveBoardHeight.constant = 0.01
            
            if savedDataArray.count > 10 {
                clipBoardHeight.constant = (CGFloat(self.savedDataArray.count <= 1 ? 130 :  10*130)) + 5
            }else{
                clipBoardHeight.constant = (CGFloat(self.savedDataArray.count <= 1 ? 130 :  self.savedDataArray.count*130)) + 5
            }
            if postedDataArray.count > 10 {
                saveBoardHeight.constant = (CGFloat(self.postedDataArray.count <= 1 ? 130 :  10*130)) + 5
            }else{
                saveBoardHeight.constant = (CGFloat(self.postedDataArray.count <= 1 ? 130 :  self.postedDataArray.count*130)) + 5
            }
            clipboardCollection.reloadData()
            savedCollection.reloadData()
            return
        }
        UserDefaults.standard.setValue("list", forKey: HOME_SCREEN_VIEW_TYPE)
        self.listButton.isSelected = true
        self.gridButton.isSelected = false
        if let flowLayout = clipboardCollection.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .vertical
        }
        if let flowLayout = savedCollection.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .vertical
        }
        clipBoardHeight.constant = 0.01
        saveBoardHeight.constant = 0.01
        if savedDataArray.count > 10 {
            clipBoardHeight.constant = (CGFloat(self.savedDataArray.count <= 1 ? 130 :  10*130)) + 5
        }else{
            clipBoardHeight.constant = (CGFloat(self.savedDataArray.count <= 1 ? 130 :  self.savedDataArray.count*130)) + 5
        }
        if postedDataArray.count > 10 {
            saveBoardHeight.constant = (CGFloat(self.postedDataArray.count <= 1 ? 130 :  10*130)) + 5
        }else{
            saveBoardHeight.constant = (CGFloat(self.postedDataArray.count <= 1 ? 130 :  self.postedDataArray.count*130)) + 5
        }
//        clipBoardHeight.constant = (CGFloat(self.savedDataArray.count <= 1 ? 130 :  self.savedDataArray.count*130)) + 5
//        saveBoardHeight.constant = (CGFloat(self.postedDataArray.count <= 1 ? 130 :  self.postedDataArray.count*130)) + 5
        clipboardCollection.reloadData()
        savedCollection.reloadData()
    }
    @IBAction func onClickGrid(_ sender: UIButton) {
        UserDefaults.standard.set(false, forKey: "isFromSerach")
        self.searchTable.isHidden = true
        refreshGrid()
      
    }
    @IBAction func onClickList(_ sender: UIButton) {
        UserDefaults.standard.set(false, forKey: "isFromSerach")
        self.searchTable.isHidden = true
        refreshList()
    }
    
    @IBAction func onClickShow(_ sender: UIButton) {
        self.noMediaView.isHidden = false
        self.loadTutView()
        self.goToInstagramButton.isHidden = false
        self.backBtnWidth.constant = 21.0
        self.backBtn.isUserInteractionEnabled = true
        UserDefaults.standard.set(true, forKey: SHOW_GUIDE_POUP)
        self.guidePopUpViewHeight.constant = 0
        
    }
    @IBAction func onClickDismiss(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: SHOW_GUIDE_POUP)
        self.guidePopUpViewHeight.constant = 0
        
    }
    @IBAction func onClickHash(_ sender: UIButton) {
        self.deSelectAllSearchOption()
        self.usernamePostsArray = NSMutableArray()
        self.searchTable.reloadData()
        self.searchTable.isHidden = true
//        self.remainingCount.isHidden = true
//        self.goProBtn.isHidden = true
        self.searchTypeImage.image = UIImage.init(named: "seach_icon.png")
        self.hashButton.isSelected = true
        self.hashButton.backgroundColor = UIColor(red: 209/255.0, green: 223/255.0, blue: 234/255.0, alpha: 1.0)
        if let hashtagSearched = UserDefaults.standard.value(forKey: HASHTAG_STRING){
        self.searchBar.text = String(format: "%@", Global.getValidString(string: hashtagSearched as! String))
        }else{
            self.searchBar.text = ""
        }
//        if searchBar.text == "" {
//            self.searchTypeImage.image = UIImage.init(named: "seach_icon.png")
//        }else{
//            self.searchTypeImage.image = UIImage.init(named: "#_icon.png")
//        }
    }
    @IBAction func onClickUserTag(_ sender: UIButton) {
        self.deSelectAllSearchOption()
        self.hashtagPostsArray = NSMutableArray()
        self.searchTable.reloadData()
        self.searchTable.isHidden = true
//        self.remainingCount.isHidden = true
//        self.goProBtn.isHidden = true
        self.userTagButton.isSelected = true
        self.searchTypeImage.image = UIImage.init(named: "seach_icon.png")
        self.userTagButton.backgroundColor = UIColor(red: 209/255.0, green: 223/255.0, blue: 234/255.0, alpha: 1.0)
        if let usernameSearched = UserDefaults.standard.value(forKey: USERNAME_STRING){
        self.searchBar.text = String(format: "%@", Global.getValidString(string: usernameSearched as! String))
        }else{
           self.searchBar.text = ""
        }
        
            
        
       self.userTagButton.setTitleColor(self.clipboardTitleLabel.textColor, for: .normal)
    }
    func deSelectAllSearchOption(){
        self.userTagButton.isSelected = false
        self.hashButton.isSelected = false
        self.userTagButton.backgroundColor =  UIColor(named: "HASH_BG_COLOR")
        self.hashButton.backgroundColor = UIColor(named: "HASH_BG_COLOR")
    }

}

extension ViewController{
    func onSearchBtn(){
        if self.userTagButton.isSelected{
            instagramId = ""
            usernamePostsArray = NSMutableArray()
            self.getDataForUserName(username: (self.searchBar.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!)
        }
        else{
            hashtagPostsArray = NSMutableArray()
            self.getDataForHashTag(hashtagString: (self.searchBar.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!)
        }
        self.view.endEditing(true)
    }
    func getDataForHashTag(hashtagString : String) {
        
        
        var hashtagString = hashtagString.replacingOccurrences(of: "#", with: "")
        hashtagString = hashtagString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        UserDefaults.standard.setValue(hashtagString, forKey: HASHTAG_STRING)
        if hashtagString == ""{
            return;
        }
        
        let q = DispatchQueue.global(qos: .default)
        q.async(execute: { [self] in
            
            print("Loading data.... ")
            
            if self.hashtagPostsArray.count == 0 {
                   
                
                UserDefaults.standard.synchronize()
                
                DispatchQueue.main.async(execute: {
                    Global.addWaitingView(waitingViewLabel: "Please wait...")
                })
            }
            if self.getUserLatestImagesWithUserName(userName: hashtagString) != nil{
                self.hashtagPostsArray.addObjects(from: self.getUserLatestImagesWithUserName(userName: hashtagString) as! [Any])
            }
            if !Global.isPaidUser() {
                
                if self.searchedArray.count == 0 {
                    let firstSearchedDate = NSDate()
                    UserDefaults.standard.setValue(firstSearchedDate, forKey: SEARCH_DATE)
                }
                
                if self.searchedArray.count == 0 {
                    self.searchedArray = NSMutableArray()
                }
                
                if (!self.searchedArray.contains(hashtagString) && self.searchedArray.count == 5) && self.hashtagPostsArray.count != 0{
                    DispatchQueue.main.async(execute: { [self] in
                        self.initialisePurchaseViewController()
                    })
                    return
                }
                
                if !self.searchedArray.contains(hashtagString) && self.hashtagPostsArray.count != 0{
                    self.searchedArray.add(hashtagString)
                    UserDefaults.standard.setValue(self.searchedArray, forKey: SEARCHED_STRINGS)
                }
                
                self.setRemainingCountLabel()
            }
            DispatchQueue.main.async(execute: { [self] in
                Global.removeWaitingView()
                if self.hashtagPostsArray.count != 0{
                    
                    self.scrollView.setContentOffset(.zero, animated: true)
                self.searchTable.isHidden = false
                self.searchTable.reloadData()
                }else{
                    self.searchTable.isHidden = true
                    self.searchBtnView.isHidden = true
                }
               
                
                self.isDataLoading = false
            })
        })
    }
    func getDataForUserName(username: String){
        if self.searchedArray.count == 5 && ( !(Global.isPaidUser()) && !(self.searchedArray.contains(username))){
            DispatchQueue.main.async(execute: {
                self.initialisePurchaseViewController()
                self.usernamePostsArray = NSMutableArray()
                self.searchTable.reloadData()
                self.searchTable.isHidden = true
            })
            return;
        }
        
        let username = username.replacingOccurrences(of: "@", with: "")
        UserDefaults.standard.set(username, forKey: USERNAME_STRING)
        if username == ""{
            return;
        }
        let q = DispatchQueue.global(qos: .default)
        q.async(execute: { [self] in
            print("Loading data.... ")
            
            if self.instagramId == ""{
                
                
                
                UserDefaults.standard.synchronize()
                DispatchQueue.main.async(execute: {
                    Global.addWaitingView(waitingViewLabel: "Please wait...")
                })
                
                self.getUserDetails(userName: username)
            }
            
            if self.urlToBeUsed == ""{
//                self.urlToBeUsed = self.getAppropriateUrl:self->instagramId andEndCur:self->endCurser
            }
            self.usernamePostsArray.addObjects(from: self.getUserLatestImagesWithInstagramId(instagramId: instagramId) as! [Any])
            
            if !(Global.isPaidUser()){
                
                if(self.searchedArray.count == 0){
                    let firstSearchedDate = NSDate()
                    UserDefaults.standard.set(firstSearchedDate, forKey: SEARCH_DATE)
                }
                
                if self.searchedArray.count == 0{
                self.searchedArray = NSMutableArray()
                }
                if (!(self.searchedArray.contains(username)) && self.searchedArray.count == 5) && self.usernamePostsArray.count != 0{
                    DispatchQueue.main.async(execute: {
                        self.initialisePurchaseViewController()
                        
                    })
                    return;
                }
                
                if !(self.searchedArray.contains(username)) && self.usernamePostsArray.count != 0{
                    self.searchedArray.add(username)
                    UserDefaults.standard.set(self.searchedArray, forKey: SEARCHED_STRINGS)
                }
                
                self.setRemainingCountLabel()
            }
            DispatchQueue.main.async(execute: {
                Global.removeWaitingView()
                if self.usernamePostsArray.count != 0{
                    
                    self.scrollView.setContentOffset(.zero, animated: true)
                self.searchTable.isHidden = false
                self.searchTable.reloadData()
                }else{
                    self.searchTable.isHidden = true
                    self.searchBtnView.isHidden = true
                }
                self.isDataLoading = false;
                
            })
        })
    }
    func setRemainingCountLabel() {
        if Global.isPaidUser() {
            self.remainingCount.isHidden = true
            self.goProBtn.isHidden = true
            return
        }
        DispatchQueue.main.async(execute: { [self] in
            var btnString = ""
            if 5 - self.searchedArray.count == 0 || 5 - self.searchedArray.count == 1 {
                btnString = "\(5 - Int(self.searchedArray.count)) Search left in this hour"
            } else {
                btnString = "\(5 - Int(self.searchedArray.count)) Searches left in this hour"
            }
            self.remainingCount.text = btnString
        })
    }
    func getUserLatestImagesWithUserName(userName : String) -> NSMutableArray?{
        
        let url = NSURL(string: String(format: GET_USER_MEDIA_URL, userName))
        if url == nil {
            return NSMutableArray()
        }
        let array = NSMutableArray()
        let data = NSData(contentsOf: url! as URL)
        if data == nil {
            haveNextPage = false;
            return nil;
        }
        else{
            
            var jsonObject: [AnyHashable : Any]? = nil
            do {
                jsonObject = try JSONSerialization.jsonObject(with: data! as Data, options: .allowFragments) as? [AnyHashable : Any]
                let graphQldict = jsonObject!["graphql"]
                if graphQldict != nil {
                    let hashtag = (graphQldict as AnyObject).value(forKey:"hashtag") as? [AnyHashable : Any]
                    if hashtag != nil {
                        if let dict = hashtag!["edge_hashtag_to_top_posts"] as? NSDictionary{
                            if let edges = dict["edges"]{
                                var postsArray = edges as! NSArray
                                if postsArray.count == 0 {
                                    postsArray = NSMutableArray()
                                }else{
                                    let object = edges
                                    postsArray = NSMutableArray(array: postsArray.addingObjects(from: object as! [Any]))
                                    
                                }
                                for dict in postsArray {
                                    guard let dict = dict as? [AnyHashable : Any] else {
                                        continue
                                    }
                                    
                                    var dics: [AnyHashable : Any] = [:]
                                    let node = dict["node"] as! [AnyHashable : Any]
                                    let edge_media_to_caption = node["edge_media_to_caption"] as! NSDictionary
                                    if edge_media_to_caption.count != 0 {
                                        let  captionArr = edge_media_to_caption["edges"] as? NSArray
                                        if captionArr?.count != 0 {
                                            let txt = ((captionArr?.value(forKey: "node") as AnyObject).value(forKey: "text") as! NSArray)[0] as! String
                                            if txt == "" {
                                                dics[CAPTION] = ""
                                            }
                                            else
                                            {
                                                dics[CAPTION] = "\(txt)"
                                            }
                                        }else {
                                            dics[CAPTION] = ""
                                        }
                                    }
                                    dics[IS_CAROUSEL] = node["edge_sidecar_to_children"] != nil ? YES_STRING : NO_STRING
                                    dics[IS_VIDEO] = (node["is_video"] as? NSNumber)!.boolValue ? YES_STRING : NO_STRING
                                    dics[DISPLAY_URL] = node["display_url"]
                                    dics[LINK_ID] = node["shortcode"]
                                    dics[PROFILE_URL] = instagramUserDic["profile_pic_url"]
                                    dics[USER_NAME] = instagramUserDic["user_name"]
                                    dics[LINK] = "https://www.instagram.com/p/\((dics[LINK_ID])!)"
                                    dics[FULL_NAME] = instagramUserDic["full_name"]
                                    dics[IS_POSTED] = NO_STRING
                                    dics[SORT_CODE_DICT] = dict["node"]
                                    array[array.count] = dics;
                                }
                            }
                        }
                    }
                }
            } catch {}
            



        }
        return array;
    }
    func initialisePurchaseViewController(){
        
        self.view.endEditing(true)
        if (purchaseViewController.isKind(of: PurchaseViewController.self)){
            purchaseViewController.view.removeFromSuperview()
            purchaseViewController = PurchaseViewController()
            
        }
        purchaseViewController = PurchaseViewController.init(nibName: "PurchaseViewController", bundle: nil)
        purchaseViewController.delegate = self
        purchaseViewController.view.frame = UIScreen.main.bounds
        let currentWindow = UIApplication.shared.keyWindow
        purchaseViewController.crossBtn.addTarget(self, action: #selector(onNoThanksBtn(_:)), for: .touchUpInside)
        currentWindow?.addSubview(purchaseViewController.view)
        
        purchaseViewController.view.alpha = 0.0;
        UIView.animate(withDuration: 0.35, animations: {
            self.purchaseViewController.view.alpha = 1.0
        }) { (finished) in
            self.purchaseViewController.view.alpha = 1.0
        }
    }
   
    @objc func onTry7DaysFreeBtn(_ sender: Any?) {
        noMediaView.isHidden = true
        if introViewController != nil {
            UIView.animate(withDuration: 0.35, animations: { [self] in
                introViewController.view.alpha = 0.0
            }) { [self] finished in
                introViewController.view.alpha = 0.0
                introViewController.view.removeFromSuperview()
                introViewController = IntroViewController()
                noMediaView.isHidden = false
                self.goToInstagramButton.isHidden = false
                
            }
        }
        initialisePurchaseViewController()
    }
  
    func initialiseIntroView(){
        if(introViewController.view != nil){
            introViewController.view.removeFromSuperview()
            introViewController = IntroViewController()
        }
        introViewController = IntroViewController.init(nibName: "IntroViewController", bundle: nil)
        introViewController.view.frame = UIScreen.main.bounds
        let currentWindow = UIApplication.shared.keyWindow
        introViewController.noThanksBtn.addTarget(self, action: #selector(onNoThanksBtn(_:)), for: .touchUpInside)
        introViewController.try7DaysFreeBtn.addTarget(self, action: #selector(onTry7DaysFreeBtn(_:)), for: .touchUpInside)
        currentWindow?.addSubview(introViewController.view)
        introViewController.view.alpha = 0.0;
        UIView.animate(withDuration: 0.35, animations: {
            self.introViewController.view.alpha = 1.0
        }) { (finished) in
            self.introViewController.view.alpha = 1.0
        }
    }

    func initialisePurchaseSuccessViewController(){
        if (purchaseSuccessViewController.isKind(of: PurchaseSuccessViewController.self)){
            purchaseSuccessViewController.view.removeFromSuperview()
            purchaseSuccessViewController = PurchaseSuccessViewController()
        }
        purchaseSuccessViewController = PurchaseSuccessViewController.init(nibName: "PurchaseSuccessViewController", bundle: nil)
        purchaseSuccessViewController.view.frame = UIScreen.main.bounds
        let currentWindow = UIApplication.shared.keyWindow
        purchaseSuccessViewController.letsGoBtn!.addTarget(self, action: #selector(onLetsGoBtn(_:)), for: .touchUpInside)
        currentWindow?.addSubview(purchaseSuccessViewController.view)
        
        purchaseSuccessViewController.view.alpha = 0.0;
        UIView.animate(withDuration: 0.35, animations: {
            self.purchaseSuccessViewController.view.alpha = 1.0
        }) { (finished) in
            self.purchaseSuccessViewController.view.alpha = 1.0
        }
    }
    @objc func onLetsGoBtn(_ sender: Any) {
        if purchaseSuccessViewController.view != nil {
            UIView.animate(withDuration: 0.35, animations: { [self] in
                purchaseSuccessViewController.view.alpha = 0.0
            }) { [self] finished in
                purchaseSuccessViewController.view.alpha = 0.0
                purchaseSuccessViewController.view.removeFromSuperview()
                purchaseSuccessViewController = PurchaseSuccessViewController()
            }
        }
    }
    func getUserDetails(userName: String){
        
        let userName = userName.replacingOccurrences(of: " ", with: "")
        
        let arrToReturn = NSMutableArray()
        
        instagramUserDic = NSMutableDictionary()
        let data = NSData(contentsOf: NSURL(string: String(format: "https://www.instagram.com/%@/", userName))! as URL)
        if data != nil{
            let aString = NSString(data: data! as Data, encoding: String.Encoding.utf8.rawValue)
            let startRangeVideo = aString!.range(of: "window._sharedData = ")
            if startRangeVideo.length == 0{
                DispatchQueue.main.async(execute: {
                    let privateMediaAlert = UIAlertController(title: "eep!!", message: "This username does not exist.", preferredStyle: .alert)
                    privateMediaAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { action in
                        self.usernamePostsArray = NSMutableArray()
                        self.searchTable.reloadData()
                    }))
                    UIApplication.shared.keyWindow?.rootViewController?.present(privateMediaAlert, animated: true)
                    return
                })
            }
            let searchRange = NSMakeRange(startRangeVideo.location + startRangeVideo.length, aString!.length - (startRangeVideo.location + startRangeVideo.length))
            let strVideo = (aString?.substring(with: searchRange))! as NSString
            let endRange1 = strVideo.range(of: ";</script>")
            let searchRange1 = NSMakeRange(0, endRange1.location)
            let jsonStr = strVideo.substring(with: searchRange1)
            if jsonStr.isEmpty {
                DispatchQueue.main.async(execute: {
                    let privateMediaAlert = UIAlertController(title: "eep!!", message: "This username does not exist.", preferredStyle: .alert)
                    privateMediaAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { action in
                        self.usernamePostsArray = NSMutableArray()
                        self.searchTable.reloadData()
                    }))
                    UIApplication.shared.keyWindow?.rootViewController?.present(privateMediaAlert, animated: true)
                    return
                })
            }
            let dataToDecode = jsonStr.data(using: String.Encoding.utf8)
            if dataToDecode == nil  {
                DispatchQueue.main.async(execute: {
                    let privateMediaAlert = UIAlertController(title: "eep!!", message: "This username does not exist.", preferredStyle: .alert)
                    privateMediaAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { action in
                        self.usernamePostsArray = NSMutableArray()
                        self.searchTable.reloadData()
                    }))
                    UIApplication.shared.keyWindow?.rootViewController?.present(privateMediaAlert, animated: true)
                    return
                })
            }
            var jsonObject: [AnyHashable : Any]? = nil
            do {
                jsonObject = try JSONSerialization.jsonObject(with: dataToDecode! as Data, options: []) as? [AnyHashable : Any]
                //                let bool = ((jsonObject?["entry_data"] as! AnyObject)["ProfilePage"][0]["graphql"]["user"]["is_private"] as? NSNumber)?.boolValue
                let entry_data = jsonObject?["entry_data"]  as? [AnyHashable : Any]
               
                if entry_data != nil {
                    let ProfilePage = entry_data!["ProfilePage"]  as? [AnyHashable]
                    let value = ProfilePage![0] as? [AnyHashable : Any]
                    if value != nil {
                        if let graphql = value!["graphql"] as? NSDictionary{
                            if let user = graphql["user"] as? NSDictionary{
                                let is_private = user["is_private"] as! Int
                                if is_private == 0 {
                                    let edge_owner_to_timeline_media = user["edge_owner_to_timeline_media"] as? [AnyHashable : Any]
                                    let postsArray = edge_owner_to_timeline_media!["edges"] as! [AnyHashable]
                                    instagramUserDic["rhx_gis"] = jsonObject?["rhx_gis"]
                                    instagramUserDic["user_name"] = user["username"]
                                    instagramUserDic["profile_pic_url"] = user["profile_pic_url"]
                                    instagramUserDic["full_name"] = user["full_name"]
                                    instagramId = user["id"] as! String
                                    
                                    for dict in postsArray {
                                        if dict != nil {
                                            var dics: [AnyHashable : Any] = [:]
                                            
                                            if let jsonNode = (dict as! [AnyHashable : Any])["node"]{
                                                let  node = jsonNode as! [AnyHashable : Any]
                                            let edge_media_to_caption = node["edge_media_to_caption"] as! NSDictionary
                                            if edge_media_to_caption.count != 0 {
                                                let  captionArr = edge_media_to_caption["edges"] as? NSArray
                                                if captionArr?.count != 0 {
                                                    let txt = ((captionArr?.value(forKey: "node") as AnyObject).value(forKey: "text") as! NSArray)[0] as! String
                                                    if txt == "" {
                                                        dics[CAPTION] = ""
                                                    }
                                                    else
                                                    {
                                                        dics[CAPTION] = "\(txt)"
                                                    }
                                                }else {
                                                    dics[CAPTION] = ""
                                                }
                                            }
                                            dics[IS_CAROUSEL] = node["edge_sidecar_to_children"] != nil ? YES_STRING : NO_STRING
                                            dics[IS_VIDEO] = (node["is_video"] as? NSNumber)!.boolValue ? YES_STRING : NO_STRING
                                            dics[DISPLAY_URL] = node["display_url"]
                                            dics[LINK_ID] = node["shortcode"]
                                            dics[PROFILE_URL] = instagramUserDic["profile_pic_url"]
                                            dics[USER_NAME] = instagramUserDic["user_name"]
                                            dics[LINK] = "https://www.instagram.com/p/\((dics[LINK_ID])!)"
                                            dics[FULL_NAME] = instagramUserDic["full_name"]
                                            dics[IS_POSTED] = NO_STRING
                                            dics[SORT_CODE_DICT] = jsonObject?["node"]
                                            arrToReturn[arrToReturn.count] = dics
                                            }
                                        }
                                        if arrToReturn.count != 0 {
                                            
                                            Global.cookietoUse(userDict: instagramUserDic)
                                        }
                                    }
                                }else{
                                    DispatchQueue.main.async(execute: {
                                        let privateMediaAlert = UIAlertController(title: "eep!!", message: "The account you are trying to search is a private account.", preferredStyle: .alert)
                                        privateMediaAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { action in
                                            self.usernamePostsArray = NSMutableArray()
                                            self.searchTable.reloadData()
                                        }))
                                        UIApplication.shared.keyWindow?.rootViewController?.present(privateMediaAlert, animated: true)
                                        return
                                    })
                                    
                                }
                            }
                        }
                    }
                }
            } catch {}
        }else{
            DispatchQueue.main.async(execute: {
                let privateMediaAlert = UIAlertController(title: "eep!!", message: "This username does not exist.", preferredStyle: .alert)
                privateMediaAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { action in
                    self.usernamePostsArray = NSMutableArray()
                    self.searchTable.reloadData()
                }))
                UIApplication.shared.keyWindow?.rootViewController?.present(privateMediaAlert, animated: true)
                return
            })
        }
        
        usernamePostsArray.addObjects(from: arrToReturn as! [Any])
    }

    func getAppropriateUrl(instagramId: String)-> String?{
        if cookiesArray.count == 0 {
            Global.cookietoUse(userDict: instagramUserDic)
        }

        if instagramId == "" {
            return ""
        }

        let variables = String(format: VARIABLES, instagramId, "1")

        let urlString1 = "\((GET_USER_MEDIA_URL_1)!)\((queryId)!)\(variables)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)

        let url1 = URL(string: urlString1 ?? "")

        var request: NSMutableURLRequest? = nil
        if let url1 = url1 {
            request = NSMutableURLRequest(url: url1)
        }

        request?.cachePolicy = .useProtocolCachePolicy
        request?.allHTTPHeaderFields = HTTPCookie.requestHeaderFields(with: cookiesArray as! [HTTPCookie])
        request?.setValue("1", forHTTPHeaderField: "Dnt")
        request?.setValue("gzip, deflate, br", forHTTPHeaderField: "Accept-Encoding")
        request?.setValue("en-US,en;q=0.9,hi;q=0.8", forHTTPHeaderField: "Accept-Language")
        request?.setValue("*/*", forHTTPHeaderField: "Accept")
        request?.setValue("https://www.instagram.com/\((instagramUserDic["user_name"])!)", forHTTPHeaderField: "Referer")
        request?.setValue("www.instagram.com", forHTTPHeaderField: "Authority")
        request?.setValue("XMLHttpRequest", forHTTPHeaderField: "X-Requested-With")
        request?.setValue(Global.getGis(userDic: instagramUserDic as! [AnyHashable : Any], variables: variables, userAgent: request?.value(forHTTPHeaderField: "User-Agent")), forHTTPHeaderField: "X-Instagram-Gis")

        request?.httpMethod = "GET"

        var response: URLResponse? = nil
        var data1: Data? = nil
        do {
            if let request = request {
                data1 = try NSURLConnection.sendSynchronousRequest(
                    request as URLRequest,
                    returning: &response)
            }
        } catch {
        }
        if data1 != nil {
       
            var jsonObject: [AnyHashable : Any]? = nil
        do {
            jsonObject = try JSONSerialization.jsonObject(with: data1!, options: .allowFragments) as? [AnyHashable : Any]
        } catch {
        }

        if jsonObject?["status"] as! String == "ok" {
            return "\((GET_USER_MEDIA_URL_1)!)\((queryId)!)"
        } else {

            let variables = String(format: VARIABLES, instagramId, "1")

            let urlString2 = "\((GET_USER_MEDIA_URL_2)!)\((queryHash)!)\(variables)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)

            let url2 = URL(string: urlString2 ?? "")

            var request: NSMutableURLRequest? = nil
            if let url2 = url2 {
                request = NSMutableURLRequest(url: url2)
            }

            request?.cachePolicy = .useProtocolCachePolicy
            request?.allHTTPHeaderFields = HTTPCookie.requestHeaderFields(with: cookiesArray as! [HTTPCookie])
            request?.setValue("1", forHTTPHeaderField: "Dnt")
            request?.setValue("gzip, deflate, br", forHTTPHeaderField: "Accept-Encoding")
            request?.setValue("en-US,en;q=0.9,hi;q=0.8", forHTTPHeaderField: "Accept-Language")
            request?.setValue("*/*", forHTTPHeaderField: "Accept")
            request?.setValue("https://www.instagram.com/\((instagramUserDic["user_name"])!)", forHTTPHeaderField: "Referer")
            request?.setValue("www.instagram.com", forHTTPHeaderField: "Authority")
            request?.setValue("XMLHttpRequest", forHTTPHeaderField: "X-Requested-With")
            request?.setValue(Global.getGis(userDic: instagramUserDic as! [AnyHashable : Any], variables: variables, userAgent: request?.value(forHTTPHeaderField: "User-Agent")), forHTTPHeaderField: "X-Instagram-Gis")

            request?.httpMethod = "GET"

            var response: URLResponse? = nil
            var _: Error? = nil
            var data2: Data? = nil
            do {
                if let request = request {
                    data2 = try NSURLConnection.sendSynchronousRequest(
                        request as URLRequest,
                        returning: &response)
                }
            } catch {
            }

            if data2 != nil {
                return "\((GET_USER_MEDIA_URL_2)! )\((queryHash)!)"
                }
                else{
                   return ""
                }
        }
    }else{
        let variables = String(format: VARIABLES, instagramId, "1")

        let urlString2 = "\((GET_USER_MEDIA_URL_2)!)\((queryHash)!)\(variables)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)

        let url2 = URL(string: urlString2 ?? "")

        var request: NSMutableURLRequest? = nil
        if let url2 = url2 {
            request = NSMutableURLRequest(url: url2)
        }
        request?.cachePolicy = .useProtocolCachePolicy
        request?.allHTTPHeaderFields = HTTPCookie.requestHeaderFields(with: cookiesArray as! [HTTPCookie])
        request?.setValue("1", forHTTPHeaderField: "Dnt")
        request?.setValue("gzip, deflate, br", forHTTPHeaderField: "Accept-Encoding")
        request?.setValue("en-US,en;q=0.9,hi;q=0.8", forHTTPHeaderField: "Accept-Language")
        request?.setValue("*/*", forHTTPHeaderField: "Accept")
        request?.setValue("https://www.instagram.com/\((instagramUserDic["user_name"])!)", forHTTPHeaderField: "Referer")
        request?.setValue("www.instagram.com", forHTTPHeaderField: "Authority")
        request?.setValue("XMLHttpRequest", forHTTPHeaderField: "X-Requested-With")
        request?.setValue(Global.getGis(userDic: instagramUserDic as! [AnyHashable : Any], variables: variables, userAgent: request?.value(forHTTPHeaderField: "User-Agent")), forHTTPHeaderField: "X-Instagram-Gis")

        request?.httpMethod = "GET"

        var response: URLResponse? = nil
        var error: Error? = nil
        var data2: Data? = nil
        do {
            if let request = request {
                data2 = try NSURLConnection.sendSynchronousRequest(
                    request as URLRequest,
                    returning: &response)
            }
        } catch {
        }

        if data2 != nil {
            return "\((GET_USER_MEDIA_URL_2)!)\((queryHash)!)"
        } else {
            return ""
        }

        }
        
    }

    func getUserLatestImagesWithInstagramId(instagramId: String) -> NSMutableArray?{
        var array = NSMutableArray()
        if instagramId == "" {
            return array
        }
        if urlToBeUsed == "" {
            return array
        }
        let variables = String(format: VARIABLES, instagramId, GET_DATA_COUNT, instagramUserDic["endCursor"] as! CVarArg)

        let urlString = "\(urlToBeUsed)\(variables)"

        let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed) ?? "")

        var request: NSMutableURLRequest? = nil
        if let url = url {
            request = NSMutableURLRequest(url: url)
        }
        request?.cachePolicy = .useProtocolCachePolicy
        request?.allHTTPHeaderFields = HTTPCookie.requestHeaderFields(with: cookiesArray as! [HTTPCookie])
        request?.setValue("1", forHTTPHeaderField: "Dnt")
        request?.setValue("gzip, deflate, br", forHTTPHeaderField: "Accept-Encoding")
        request?.setValue("en-US,en;q=0.9,hi;q=0.8", forHTTPHeaderField: "Accept-Language")
        request?.setValue("*/*", forHTTPHeaderField: "Accept")
        request?.setValue("https://www.instagram.com/\(instagramUserDic["user_name"])/", forHTTPHeaderField: "Referer")
        request?.setValue("www.instagram.com", forHTTPHeaderField: "Authority")
        request?.setValue("XMLHttpRequest", forHTTPHeaderField: "X-Requested-With")
        request?.setValue(Global.getGis(userDic: instagramUserDic as! [AnyHashable : Any], variables: variables, userAgent: request?.value(forHTTPHeaderField: "User-Agent")), forHTTPHeaderField: "X-Instagram-Gis")

        request?.httpMethod = "GET"

        request?.timeoutInterval = 30

        var response: URLResponse? = nil
        var error: Error? = nil
        var data: Data? = nil
        do {
            if let request = request {
                data = try NSURLConnection.sendSynchronousRequest(
                    request as URLRequest,
                    returning: &response)
            }
        } catch {
        }
        print("Headers = \(request!.allHTTPHeaderFields ?? [:])")

        if let data = data {

            var jsonObject: [AnyHashable : Any]? = nil
            do {
                jsonObject = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [AnyHashable : Any]
            } catch {
            }

            if jsonObject == nil {
                haveNextPage = false
                return array
            }
            let user = (jsonObject!["data"] as AnyObject)["user"] as? [AnyHashable : Any]
            if user == nil {
                haveNextPage = false
                return array
            }

            let postsArray = (user!["edge_owner_to_timeline_media"] as AnyObject)["edges"] as! [AnyHashable : Any]
            for dict in postsArray {
                if dict != nil {
                    var dics: [AnyHashable : Any] = [:]
                    
                    let node = jsonObject?["node"] as! [AnyHashable : Any]
                    let edge_media_to_caption = node["edge_media_to_caption"] as! NSDictionary
                    if edge_media_to_caption.count != 0 {
                        let  captionArr = edge_media_to_caption["edges"] as? NSArray
                        if captionArr?.count != 0 {
                            let txt = ((captionArr?.value(forKey: "node") as AnyObject).value(forKey: "text") as! NSArray)[0] as! String
                            if txt == "" {
                                dics[CAPTION] = ""
                            }
                            else
                            {
                                dics[CAPTION] = "\(txt)"
                            }
                        }else {
                            dics[CAPTION] = ""
                        }
                    }
                    dics[IS_CAROUSEL] = node["edge_sidecar_to_children"] != nil ? YES_STRING : NO_STRING
                    dics[IS_VIDEO] = (node["is_video"] as? NSNumber)!.boolValue ? YES_STRING : NO_STRING
                    dics[DISPLAY_URL] = node["display_url"]
                    dics[LINK_ID] = node["shortcode"]
                    dics[PROFILE_URL] = instagramUserDic["profile_pic_url"]
                    dics[USER_NAME] = instagramUserDic["user_name"]
                    dics[LINK] = "https://www.instagram.com/p/\((dics[LINK_ID])!)"
                    dics[FULL_NAME] = instagramUserDic["full_name"]
                    dics[IS_POSTED] = NO_STRING
                    dics[SORT_CODE_DICT] = jsonObject?["node"]
                    array[array.count] = dics
                }
            }
        }
        return array;
    }

}
extension ViewController: PurchaseViewControllerDelegate, UITextFieldDelegate{
    func buy(_ product: SKProduct?) {
        PurchaseHandler.sharedInstance().delegate = self
        PurchaseHandler.sharedInstance()?.buyProduct(product?.productIdentifier)
        setUserInterationDisabled()
    }

    func restoreTransaction() {
        PurchaseHandler.sharedInstance().delegate = self
        PurchaseHandler.sharedInstance().restorePurchases()
        setUserInterationDisabled()
    }

    func setUserInterationDisabled() {
        purchaseViewController.activityIndicator.isHidden = false
        purchaseViewController.activityIndicator.startAnimating()
        purchaseViewController.startFreeWeekBtn.isUserInteractionEnabled = false
        purchaseViewController.restoreBtn.isUserInteractionEnabled = false
        purchaseViewController.crossBtn.isUserInteractionEnabled = false
    }
   
    func transactionFailed() {
        purchaseViewController.activityIndicator.isHidden = true
        purchaseViewController.activityIndicator.stopAnimating()
        purchaseViewController.startFreeWeekBtn.isUserInteractionEnabled = true
        purchaseViewController.restoreBtn.isUserInteractionEnabled = true
        purchaseViewController.crossBtn.isUserInteractionEnabled = true
        Global.removeWaitingView()
    }

    func transactionSuccessful(_ transaction: SKPaymentTransaction?) {
        purchaseViewController.activityIndicator.isHidden = true
        purchaseViewController.activityIndicator.stopAnimating()
        purchaseViewController.startFreeWeekBtn.isUserInteractionEnabled = true
        purchaseViewController.restoreBtn.isUserInteractionEnabled = true
        purchaseViewController.crossBtn.isUserInteractionEnabled = true
        onNoThanksBtn(nil)
        initialisePurchaseSuccessViewController()
    }
    @objc func onNoThanksBtn(_ sender: Any?) {
        Global.removeWaitingView()
        if introViewController.view != nil {
            UIView.animate(withDuration: 0.35, animations: { [self] in
                introViewController.view.alpha = 0.0
            }) { [self] finished in
                introViewController.view.alpha = 0.0
                introViewController.view.removeFromSuperview()
                introViewController = IntroViewController()
//                UserDefaults.standard.set(1, forKey: APP_SESSION_COUNT)
//                UserDefaults.standard.synchronize()
            }
        }

        if purchaseViewController.view != nil {
            UIView.animate(withDuration: 0.35, animations: { [self] in
                purchaseViewController.view.alpha = 0.0
            }) { [self] finished in
                purchaseViewController.view.alpha = 0.0
                purchaseViewController.view.removeFromSuperview()
                purchaseViewController = PurchaseViewController()
//                UserDefaults.standard.set(1, forKey: APP_SESSION_COUNT)
//                UserDefaults.standard.synchronize()
            }
        }
    }
    func transactionRestored() {

        Global.removeWaitingView()

        if Global.isPaidUser() {

            let controller = UIAlertController(
                title: "Success",
                message: "Restored Successfully!!",
                preferredStyle: .alert)

            controller.addAction(UIAlertAction(title: "OK", style: .default, handler: { [self] action in
                if purchaseViewController.view != nil {
                    purchaseViewController.activityIndicator.isHidden = true
                    purchaseViewController.activityIndicator.stopAnimating()
                    purchaseViewController.startFreeWeekBtn.isUserInteractionEnabled = true
                    purchaseViewController.restoreBtn.isUserInteractionEnabled = true
                    purchaseViewController.crossBtn.isUserInteractionEnabled = true
                    onNoThanksBtn(nil)
                    initialisePurchaseSuccessViewController()
                }
            }))

            UIApplication.shared.keyWindow?.rootViewController?.present(controller, animated: true)
        } else {
            let controller = UIAlertController(
                title: "Woops!!",
                message: "No Active subscription found.",
                preferredStyle: .alert)

            controller.addAction(UIAlertAction(title: "OK", style: .default, handler: { [self] action in
                if  purchaseViewController.view != nil {
                    purchaseViewController.activityIndicator.isHidden = true
                    purchaseViewController.activityIndicator.stopAnimating()
                    purchaseViewController.startFreeWeekBtn.isUserInteractionEnabled = true
                    purchaseViewController.restoreBtn.isUserInteractionEnabled = true
                    purchaseViewController.crossBtn.isUserInteractionEnabled = true
                }
            }))

            UIApplication.shared.keyWindow?.rootViewController?.present(controller, animated: true)
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
            setRemainingCountLabel()
            self.searchBtnView.isHidden = false
            self.searchButton.isHidden  = false
            self.cancelButton.isHidden  = false
        
        if self.userTagButton.isSelected{
            if searchBar.text != ""{
                searchTypeImage.image = UIImage.init(named: "@_icon.png")
            }else{
                searchTypeImage.image = UIImage.init(named: "seach_icon.png.png")
            }
        }
        else{
            if searchBar.text != "" {
                searchTypeImage.image = UIImage.init(named: "#_icon.png")
            }else{
                searchTypeImage.image = UIImage.init(named: "seach_icon.png.png")
            }
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
       
            self.searchBtnView.isHidden = true
            self.searchButton.isHidden  = true
            self.cancelButton.isHidden  = true
       
        if self.userTagButton.isSelected{
            if searchBar.text == "@"{
                searchBar.text = ""
                searchTypeImage.image = UIImage.init(named: "seach_icon.png.png")
            }
        }
        else{
            if searchBar.text == "#" {
                searchBar.text = ""
                searchTypeImage.image = UIImage.init(named: "seach_icon.png.png")
            }
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == " "{
            
            return false
        }
     
        if self.userTagButton.isSelected{
            if searchBar.text != ""{
                searchTypeImage.image = UIImage.init(named: "@_icon.png")
            }else{
                searchTypeImage.image = UIImage.init(named: "seach_icon.png.png")
            }
        }
        else{
            if searchBar.text != "" {
                searchTypeImage.image = UIImage.init(named: "#_icon.png")
            }else{
                searchTypeImage.image = UIImage.init(named: "seach_icon.png.png")
            }
        }
        
        if string == "\n"{
            self.onSearchBtn()
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if self.userTagButton.isSelected{
            instagramId = ""
            usernamePostsArray = NSMutableArray()
            self.getDataForUserName(username: (searchBar.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!)
        }
        else{
            
            hashtagPostsArray = NSMutableArray()
            self.getDataForHashTag(hashtagString: (searchBar.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!)
        }
        self.view.endEditing(true)
        textField.resignFirstResponder()
        return true
    }
}

extension UIButton{
    func roundedRightCornersOfButton(){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
            byRoundingCorners: [.bottomRight , .topRight],
            cornerRadii: CGSize(width: 8, height: 8))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
    
    func roundedLeftCornersOfButton(){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
            byRoundingCorners: [.topLeft , .bottomLeft],
            cornerRadii: CGSize(width: 8, height: 8))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
}
extension UIBezierPath {

static func arrow(from start: CGPoint, to end: CGPoint, tailWidth: CGFloat, headWidth: CGFloat, headLength: CGFloat) -> UIBezierPath {
    let length = hypot(end.x - start.x, end.y - start.y)
    let tailLength = length - headLength

    func p(_ x: CGFloat, _ y: CGFloat) -> CGPoint { return CGPoint(x: x, y: y) }
    let points: [CGPoint] = [
        p(0, tailWidth / 2),
        p(tailLength, tailWidth / 2),
        p(tailLength, headWidth / 2),
        p(length, 0),
        p(tailLength, -headWidth / 2),
        p(tailLength, -tailWidth / 2),
        p(0, -tailWidth / 2)
    ]

    let cosine = (end.x - start.x) / length
    let sine = (end.y - start.y) / length
    let transform = CGAffineTransform(a: cosine, b: sine, c: -sine, d: cosine, tx: start.x, ty: start.y)

    let path = CGMutablePath()
    path.addLines(between: points, transform: transform)
//    path.addArc(tangent1End: start, tangent2End: end, radius: 30)

    path.closeSubpath()

    return self.init(cgPath: path)
}}
