//
//  SeeAllPostsVC.swift
//  crosspost_swift
//
//  Created by vamika on 14/09/20.
//  Copyright © 2020 vamika. All rights reserved.
//

import UIKit
import SideMenu
class SeeAllPostsVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate {

    @IBOutlet weak var tableView: UITableView!
     @IBOutlet weak var topView: UIView!
    @IBOutlet weak var userNameLbl: UIButton!
    @IBOutlet weak var downImage: UIImageView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var logoLbl: UILabel!
    @IBOutlet weak var settingButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var typeLbl: UILabel!
    var type : String!
    var dataArray = NSMutableArray()
    var filteredDataArray = NSMutableArray()
    var isSearchEnable = false
    override func viewDidLoad() {
        super.viewDidLoad()
        Global.addShadowAtBottom(view: topView)
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.isUserInteractionEnabled = true
        if type == "clipboard" {
            typeLbl.text = "Clipboard"
        }else{
            typeLbl.text = "Saved"
        }
        reloadData()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let color = UIColor(named: "APP_BLUE_COLOR")
        self.userNameLbl.setTitleColor(color, for: .normal)
        self.logoLbl.textColor = color
        self.typeLbl.textColor = color
        self.settingButton.setImage(UIImage(named: "setting"), for: .normal)
        self.downImage.image = UIImage(named: "down")
        self.backBtn.setTitleColor(UIColor(named: "APP_BLUE_COLOR"), for: .normal)
        self.view.backgroundColor = UIColor(named: "APP_BG_COLOR")
    }
    func reloadData(){
        dataArray = NSMutableArray()
        let db = DBManager()
        if UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE) != nil{
            self.userNameLbl.setTitle(String(format: "@%@", UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE)  as! CVarArg) , for: .normal)
            //let name = UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE) as? String
            let workspace_id = UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE_ID) as? String
            if type == "saved" {
                dataArray = db.getDataForField(DATA_TABLE, where: String(format: "%@= '%@'  AND %@= '%@'",IS_POSTED,YES_STRING,WORK_SPACE_NAME_ID,workspace_id!))
            }else{
                dataArray = db.getDataForField(DATA_TABLE, where: String(format: "%@= '%@'  AND %@= '%@'",IS_POSTED,NO_STRING,WORK_SPACE_NAME_ID,workspace_id!))
            }
        }else{
            self.userNameLbl.setTitle("Add Workspace", for: .normal)
            if type == "saved" {
                dataArray = db.getDataForField(DATA_TABLE, where: String(format: "%@= '%@'",IS_POSTED,YES_STRING))
            }else{
                dataArray = db.getDataForField(DATA_TABLE, where: String(format: "%@= '%@'",IS_POSTED,NO_STRING))
            }
        }
        if dataArray.count > 1  {
            let mutableArray = dataArray.reverseObjectEnumerator().allObjects
            dataArray = NSMutableArray(array: mutableArray)
        }
        tableView.reloadData()
    }
    @IBAction func oCLickUserNameButton(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: ViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                NotificationCenter.default.post(name: NSNotification.Name("Add_Worspace"), object: [:], userInfo: nil)
                break
            }
        }
    }
    @IBAction func onClickSettings(_ sender: Any) {
        let menu = storyboard!.instantiateViewController(withIdentifier: "SideMenuNavigationController") as! SideMenuNavigationController
        present(menu, animated: true, completion: nil)
//        self.view.isUserInteractionEnabled = false
//        let storyboard = UIStoryboard (name: "Main", bundle: nil)
//        let resultVC = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
//        self.navigationController?.pushViewController(resultVC, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchEnable {
            return self.filteredDataArray.count
        }
       return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: ListTableViewCell!
        if let cellView = self.tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell") as? ListTableViewCell {
           cell = cellView
        }else{
            self.tableView.register(UINib(nibName: "ListTableViewCell", bundle: nil), forCellReuseIdentifier: "ListTableViewCell")
            cell = self.tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell") as? ListTableViewCell
        }
        if isSearchEnable {
            cell.postDict = self.filteredDataArray[indexPath.row] as! NSDictionary
            cell.setValuesinCell(type: "user")
        }else{
            cell.postDict = self.dataArray[indexPath.row] as! NSDictionary
            cell.setValuesinCell(type: "user")
        }
        Global.addShadowAtBottom(view: cell.backView)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isSearchEnable {
            performSegue(withIdentifier: "shareView", sender: self.filteredDataArray[indexPath.row])
        }else{
        performSegue(withIdentifier: "shareView", sender: self.dataArray[indexPath.row])
        }
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            let dbObject = DBManager()
            if self.isSearchEnable {
                let postDict = self.filteredDataArray[indexPath.row] as! NSDictionary
                dbObject.deleteRow(DATA_TABLE, where: "\(LINK_ID) = '\((postDict[LINK_ID])!)' AND \(WORK_SPACE_NAME_ID) = '\((postDict[WORK_SPACE_NAME_ID])!)'")
            }else{
                let postDict = self.dataArray[indexPath.row] as! NSDictionary
                dbObject.deleteRow(DATA_TABLE, where: "\(LINK_ID) = '\((postDict[LINK_ID])!)' AND \(WORK_SPACE_NAME_ID) = '\((postDict[WORK_SPACE_NAME_ID])!)'")
            }
            self.reloadData()
            NotificationCenter.default.post(name: NSNotification.Name("refreshView"), object: [:], userInfo: nil)
            success(true)
        })
        deleteAction.image = UIImage(named: "trash_icon.png")
        deleteAction.backgroundColor =  UIColor(named: "DELETE_BG_COLOR")
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "shareView" {
            if let nextViewController = segue.destination as? RepostViewController {
                nextViewController.postDictionary = sender as! NSDictionary
            }
        }
    }
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func goToInstagram(_ sender: Any) {
        if UIApplication.shared.canOpenURL(URL(string: "instagram://")!) {
            UIApplication.shared.open(URL(string: "instagram://")!, options: [:], completionHandler: nil)
        }else if UIApplication.shared.canOpenURL(URL(string: "https://instagram.com")!) {
            UIApplication.shared.open(URL(string: "https://instagram.com")!, options: [:], completionHandler: nil)
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            
            if let searchText = searchBar.text {
                if searchBar.text == "" {
                    isSearchEnable = false
                    searchBar.showsCancelButton = false
                    searchBar.text = ""
                    searchBar.resignFirstResponder()
                    tableView.reloadData()
                }else{
                    isSearchEnable = true
                    filteredDataArray = NSMutableArray()
                    let resultPredicate = NSPredicate(format: "username contains[c] %@", searchText)
                    let arr = dataArray.copy() as! NSArray
                    filteredDataArray = NSMutableArray(array: arr.filtered(using: resultPredicate) as NSArray)
                    tableView.reloadData()
                }
            }
        }
        
        func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
            self.searchBar.showsCancelButton = true
        }
        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
                isSearchEnable = false
                searchBar.showsCancelButton = false
                searchBar.text = ""
                searchBar.resignFirstResponder()
                tableView.reloadData()
        }
}
