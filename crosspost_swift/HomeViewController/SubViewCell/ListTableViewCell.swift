//
//  ListTableViewCell.swift
//  crosspost_swift
//
//  Created by vamika on 21/09/20.
//  Copyright © 2020 vamika. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {
    @IBOutlet weak var imageContainer: UIImageView!
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var videoImg: UIImageView!
    @IBOutlet weak var carousalImg: UIImageView!
    @IBOutlet weak var backView: UIView!
    var postDict = NSDictionary()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.carousalImg.isHidden = true
        self.videoImg.isHidden = true
        Global.makeCornerRadius(view: backView, radius: 3.0)
        self.contentView.backgroundColor = UIColor(named: "APP_BG_COLOR")
        self.backView.backgroundColor = UIColor(named: "CELL_BG_COLOR")
        self.caption.textColor = UIColor(named: "APP_BLUE_COLOR")
        self.userName.textColor = UIColor(named: "APP_BLUE_COLOR")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setValuesinCell(type: String!) {
        if type == "user"{
        self.userName.text = String(format: "@%@",postDict[USER_NAME] as! CVarArg)
        }else{
            self.userName.text = ""
        }
        self.caption.text = postDict[CAPTION] as? String
        if self.postDict[IS_CAROUSEL] as! String == YES_STRING{
            self.carousalImg.isHidden = false
        }else{
            self.carousalImg.isHidden = true
        }
        if self.postDict[IS_VIDEO] as! String == YES_STRING{
            self.videoImg.isHidden = false
        }
        else{
            self.videoImg.isHidden = true
        }
//       setImageForUserView()
       setImageOnMediaImageView(type: type)

    }
    func setImageForUserView() {
    let image = UIImage(contentsOfFile: "\((Global.userImageLocation())!)/\((self.postDict[USER_NAME])!).png")
    if image != nil {

//            image = Global.getMidiumSizeImage(image, width: 300)

        imageContainer.image = image

        imageContainer.alpha = 0.0

        UIView.animate(withDuration: 0.35, animations: {
            self.imageContainer.alpha = 1.0
        }) { finished in
            self.imageContainer.alpha = 1.0
        }

//            shimmerPostImageView.hidden = true
        return
    }

    imageContainer.yy_setImage(
        with: URL(string: postDict[DISPLAY_URL] as? String ?? ""),
        placeholder: nil,
        options: .setImageWithFadeAnimation,
        progress: { receivedSize, expectedSize in

        },
        transform: { image, url in
            return image
        }) { image, url, from, stage, error in
            if stage == .finished {
                let str = "\((Global.userImageLocation())!)/\((self.postDict[USER_NAME])!).png"
                NSData(data: (image?.pngData())!).write(toFile: str, atomically: true)

//                self.shimmerPostImageView.hidden = true
        }
    }
    }
    func setImageOnMediaImageView(type: String!) {
        if type == "user" {
//            let db = DBManager()
//            let mediaArray =  db.getDataForField(MEDIA_TABLE, where: String(format: "%@='%@'", LINK_ID,self.postDict[LINK_ID] as! String))
            var media_id = ""
//            if mediaArray?.count != 0 {
//                let dic = mediaArray?.object(at: 0) as! NSDictionary
//                media_id = dic[MEDIA_ID] as! String
//            }else{
                media_id = self.postDict[LINK_ID] as! String
//            }
           
            
            let image = UIImage(contentsOfFile: "\((Global.fullImageLocation())!)/\(media_id).png")
            if image != nil {

    //            image = Global.getMidiumSizeImage(image, width: 300)

                imageContainer.image = image

                imageContainer.alpha = 0.0

                UIView.animate(withDuration: 0.35, animations: {
                    self.imageContainer.alpha = 1.0
                }) { finished in
                    self.imageContainer.alpha = 1.0
                }

    //            shimmerPostImageView.hidden = true
                return
            }

            imageContainer.yy_setImage(
                with: URL(string: postDict[DISPLAY_URL] as? String ?? ""),
                placeholder: nil,
                options: .setImageWithFadeAnimation,
                progress: { receivedSize, expectedSize in

                },
                transform: { image, url in
                    return image
                }) { image, url, from, stage, error in
                    if stage == .finished {
                        if image != nil{
                            let str = "\((Global.fullImageLocation())!)/\(media_id).png"
                            NSData(data: (image?.pngData())!).write(toFile: str, atomically: true)
                        }
                        

    //                self.shimmerPostImageView.hidden = true
                }
            }
        }
        else{
            let image = UIImage(contentsOfFile: "\((Global.fullImageLocation())!)/\((self.postDict[LINK_ID])!).png")
            if image != nil {

    //            image = Global.getMidiumSizeImage(image, width: 300)

                imageContainer.image = image

                imageContainer.alpha = 0.0

                UIView.animate(withDuration: 0.35, animations: {
                    self.imageContainer.alpha = 1.0
                }) { finished in
                    self.imageContainer.alpha = 1.0
                }

    //            shimmerPostImageView.hidden = true
                return
            }

            imageContainer.yy_setImage(
                with: URL(string: postDict[DISPLAY_URL] as? String ?? ""),
                placeholder: nil,
                options: .setImageWithFadeAnimation,
                progress: { receivedSize, expectedSize in

                },
                transform: { image, url in
                    return image
                }) { image, url, from, stage, error in
                    if stage == .finished {
                        if image != nil{
                            let str = "\((Global.fullImageLocation())!)/\((self.postDict[LINK_ID])!).png"
                            NSData(data: (image?.pngData())!).write(toFile: str, atomically: true)
                        }
                        

    //                self.shimmerPostImageView.hidden = true
                }
            }
        }
        

    }
}
