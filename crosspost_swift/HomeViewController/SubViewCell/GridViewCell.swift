//
//  GridViewCell.swift
//  crosspost_swift
//
//  Created by vamika on 10/09/20.
//  Copyright © 2020 vamika. All rights reserved.
//

import UIKit
import YYWebImage

class GridViewCell: UICollectionViewCell {

    @IBOutlet weak var noMedia: UIView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var imagePreview: UIImageView!
    @IBOutlet weak var videoImg: UIImageView!
    @IBOutlet weak var carousalImg: UIImageView!
    var postDict = NSDictionary()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.carousalImg.isHidden = true
        self.videoImg.isHidden = true
        self.contentView.backgroundColor = UIColor(named: "CELL_BG_COLOR")
        self.noMedia.backgroundColor = UIColor(named: "CELL_BG_COLOR")
        self.captionLabel.textColor = UIColor(named: "APP_BLUE_COLOR")
        self.userName.textColor = UIColor(named: "APP_BLUE_COLOR")
    }
    
    
    func setImageOnMediaImageView() {
        self.userName.text = String(format: "@%@",postDict[USER_NAME] as! CVarArg)
        self.captionLabel.text = postDict[CAPTION] as? String
        if self.postDict[IS_CAROUSEL] as! String == YES_STRING{
            self.carousalImg.isHidden = false
        }else{
            self.carousalImg.isHidden = true
        }
        if self.postDict[IS_VIDEO] as! String == YES_STRING{
            self.videoImg.isHidden = false
        }
        else{
            self.videoImg.isHidden = true
        }
//        let db = DBManager()
//        let mediaArray =  db.getDataForField(MEDIA_TABLE, where: String(format: "%@='%@'", LINK_ID,self.postDict[LINK_ID] as! String))
//        let dic = mediaArray?.object(at: 0) as! NSDictionary
        let media_id = self.postDict[LINK_ID] as! String //dic[MEDIA_ID] as! String
        let image = UIImage(contentsOfFile: "\((Global.fullImageLocation())!)/\(media_id).png")

        if image != nil {

//            image = Global.getMidiumSizeImage(image, width: 300)

            imagePreview.image = image

            imagePreview.alpha = 0.0

            UIView.animate(withDuration: 0.35, animations: {
                self.imagePreview.alpha = 1.0
            }) { finished in
                self.imagePreview.alpha = 1.0
            }

//            shimmerPostImageView.hidden = true
            return
        }
        
        imagePreview.yy_setImage(
            with: URL(string: postDict[DISPLAY_URL] as? String ?? ""),
            placeholder: nil,
            options: .setImageWithFadeAnimation,
            progress: { receivedSize, expectedSize in

            },
            transform: { image, url in
                return image
            }) { image, url, from, stage, error in
                if stage == .finished {
                    if image != nil {
                        let str = "\((Global.fullImageLocation())!)/\(media_id).png"
                        NSData(data: (image?.pngData())!).write(toFile: str, atomically: true)
                    }else{
                        print("\((Global.fullImageLocation())!)/\(media_id).png")
                    }
                

//                self.shimmerPostImageView.hidden = true
            }
        }

    }
}
