//
//  ListViewCell.swift
//  crosspost_swift
//
//  Created by vamika on 10/09/20.
//  Copyright © 2020 vamika. All rights reserved.
//

import UIKit
protocol ListViewCellDelegate: class {
    func hiddenContainerViewTapped(inCell cell: UICollectionViewCell)
}
class ListViewCell: UICollectionViewCell, UIGestureRecognizerDelegate {
    @IBOutlet weak var noMedia: UIView!
    @IBOutlet weak var videoImg: UIImageView!
    @IBOutlet weak var carousalImg: UIImageView!
    weak var delegate: ListViewCellDelegate?
    @IBOutlet weak var uaerName: UILabel!
    @IBOutlet weak var imagePreview: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var captionTextView: UILabel!
    @IBOutlet weak var backView: UIView!
    var postDict = NSDictionary()
    var cellLabel: UILabel!
    var pan: UIPanGestureRecognizer!
    var deleteLabel2: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.videoImg.isHidden = true
        self.carousalImg.isHidden = true
        Global.makeCornerRadius(view: backView, radius: 3.0)
        self.contentView.backgroundColor = UIColor(named: "APP_BG_COLOR")
        self.backView.backgroundColor = UIColor(named: "CELL_BG_COLOR")
        self.noMedia.backgroundColor = UIColor(named: "CELL_BG_COLOR")
        self.captionTextView.textColor = UIColor(named: "APP_BLUE_COLOR")
        self.uaerName.textColor = UIColor(named: "APP_BLUE_COLOR")
        // Initialization code
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        if self.postDict.count != 0{
        commonInit()
        }
      }

      required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
      }
    func setImageOnMediaImageView() {
        self.uaerName.text = String(format: "@%@",postDict[USER_NAME] as! CVarArg)
        if self.postDict[IS_CAROUSEL] as! String == YES_STRING{
            self.carousalImg.isHidden = false
        }else{
            self.carousalImg.isHidden = true
        }
        if self.postDict[IS_VIDEO] as! String == YES_STRING{
            self.videoImg.isHidden = false
        }
        else{
            self.videoImg.isHidden = true
        }
        self.captionTextView.text = postDict[CAPTION] as? String
        //            let db = DBManager()
        //            let mediaArray =  db.getDataForField(MEDIA_TABLE, where: String(format: "%@='%@'", LINK_ID,self.postDict[LINK_ID] as! String))
        //            let dic = mediaArray?.object(at: 0) as! NSDictionary
        let media_id = self.postDict[LINK_ID] as! String //dic[MEDIA_ID] as! String
        let image = UIImage(contentsOfFile: "\((Global.fullImageLocation())!)/\(media_id).png")
        if image != nil {
            
            //            image = Global.getMidiumSizeImage(image, width: 300)
            
            imagePreview.image = image
            
            imagePreview.alpha = 0.0
            
            UIView.animate(withDuration: 0.35, animations: {
                self.imagePreview.alpha = 1.0
            }) { finished in
                self.imagePreview.alpha = 1.0
            }
            
            //            shimmerPostImageView.hidden = true
            return
        }
        
        imagePreview.yy_setImage(
            with: URL(string: postDict[DISPLAY_URL] as? String ?? ""),
            placeholder: nil,
            options: .setImageWithFadeAnimation,
            progress: { receivedSize, expectedSize in
                
            },
            transform: { image, url in
                return image
            }) { image, url, from, stage, error in
            if stage == .finished {
                if image != nil {
                    let str = "\((Global.fullImageLocation())!)/\(media_id).png"
                    NSData(data: (image?.pngData())!).write(toFile: str, atomically: true)
                }
                
                
                //                self.shimmerPostImageView.hidden = true
            }
        }
        
    }
    private func commonInit() {
       
        self.contentView.backgroundColor = UIColor(named: "APP_BG_COLOR")
        self.backgroundColor = UIColor(named: "DELETE_BG_COLOR")
        
        cellLabel = UILabel()
        cellLabel.textColor = .red
        cellLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(cellLabel)
        cellLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
        cellLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor).isActive = true
        cellLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor).isActive = true
        cellLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor).isActive = true
        
        deleteLabel2 = UIButton()
//        deleteLabel2.backgroundColor = UIColor.red
        deleteLabel2.setTitleColor(.red, for: .normal)
        deleteLabel2.setImage(UIImage(named: "trash_icon.png"), for: .normal)
        self.insertSubview(deleteLabel2, belowSubview: self.contentView)
        
        pan = UIPanGestureRecognizer(target: self, action: #selector(onPan(_:)))
        pan.delegate = self
        self.addGestureRecognizer(pan)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        if pan != nil{
        if (pan.state == UIGestureRecognizer.State.changed) {
            let p: CGPoint = pan.translation(in: self)
            let width = self.contentView.frame.width
            let height = self.contentView.frame.height
            self.contentView.frame = CGRect(x: p.x,y: 0, width: width, height: height);
            self.deleteLabel2.frame = CGRect(x: p.x + width + deleteLabel2.frame.size.width, y: 0, width: 100, height: height)
        }
        }else{
            if self.postDict.count != 0{
            commonInit()
                layoutSubviews()
            }
        }
        
    }
    
    @objc func onPan(_ pan: UIPanGestureRecognizer) {
        if pan.state == UIGestureRecognizer.State.began {
            
        } else if pan.state == UIGestureRecognizer.State.changed {
            self.setNeedsLayout()
        } else {
            if abs(pan.velocity(in: self).x) > 500 {
                let collectionView: UICollectionView = self.superview as! UICollectionView
                let indexPath: IndexPath = collectionView.indexPathForItem(at: self.center)!
                collectionView.delegate?.collectionView!(collectionView, performAction: #selector(onPan(_:)), forItemAt: indexPath, withSender: nil)
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.setNeedsLayout()
                    self.layoutIfNeeded()
                })
            }
        }
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return abs((pan.velocity(in: pan.view)).x) > abs((pan.velocity(in: pan.view)).y)
    }
}

