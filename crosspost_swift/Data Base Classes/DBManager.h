//
//  DBManager.h
//  Report
//
//  Created by Mayank Barnwal on 22/01/15.
//  Copyright (c) 2015 Zudac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#define DATABASE_NAME   @"Repost.sqlite"

#define DATA_TABLE      @"migration_table"

#define MEDIA_TABLE     @"media_table"
#define SORT_CODE_DICT @"sort_code_dict"

#define LINK_ID         @"link_id"
#define LINK            @"link"
#define IS_VIDEO        @"is_video"
#define IS_CAROUSEL     @"is_carousel"
#define DISPLAY_URL     @"display_url"
#define USER_NAME       @"username"
#define FULL_NAME       @"name"
#define CAPTION         @"caption"
#define PROFILE_URL     @"profile_pic"
#define IS_POSTED       @"is_posted"

#define MEDIA_ID        @"media_id"
#define MEDIA_URL       @"media_url"
#define THUMBNAIL_URL   @"thumbnail_url"
@interface DBManager : NSObject

-(void) openDb;

-(void) createDatabase:(NSFileManager *)manager;

-(void) createTable:tableName;

-(void) insertValues:(NSString*) tableName Values:(NSDictionary*) data where:(NSString*) whereClause;
-(void) insertValues:(NSString*) tableName Values:(NSDictionary*) data;

-(void) update:(NSString*)table_name : (NSDictionary *) data :(NSString*) whereClause;

-(NSMutableArray *)getDataForField:(NSString*)tableName where:(NSString *)whereClause;

-(NSMutableArray *)getDataForTable:(NSString*)tableName;

-(BOOL) deleteRow:(NSString *)tableName Where:(NSString*)whereClause;

-(NSMutableArray *)getDataForQuery:(NSString *)query;

-(void) insertValues:(NSString*) tableName Array:(NSMutableArray*) dataArray;

-(void) alterTable:(NSString*) tableName Colomn:(NSString*) colomnName DefaultValue:(NSString*) defaultValue isPrimaryKey:(BOOL) primaryKey;

@end
