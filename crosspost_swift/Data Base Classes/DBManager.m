//
//  DBManager.m
//  Report
//
//  Created by Mayank Barnwal on 22/01/15.
//  Copyright (c) 2015 Zudac. All rights reserved.
//

#import "DBManager.h"
#import "FMDB.h"


@implementation DBManager
{
    sqlite3 *_db;
}

-(void)openDb{

    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    NSString *dbPath = [self dBFilePath];
    BOOL success = [fileManager fileExistsAtPath:dbPath];
    
    NSLog(@"My data base path%@",dbPath);
    if(!success) {
        
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DATABASE_NAME];
        success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
        
        if (!success)
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
    
//    [self createTable:DATA_TABLE];
}
-(void) insertValues:(NSString*) tableName Values:(NSDictionary*) data where:(NSString*) whereClause{
    
}
-(void) insertValues:(NSString*) tableName Values:(NSDictionary*) data 
{
    NSString *dbPath = [self dBFilePath];
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSMutableArray* newCols = [[NSMutableArray alloc] init];
    NSMutableArray* newVals = [[NSMutableArray alloc] init];
    
    for (NSString *key in [data allKeys]) {
        [newCols addObject:[NSString stringWithString:key]];
        
        NSString *dataString = [NSString stringWithFormat:@"%@",data[key]];
        
        [newVals addObject:[NSString stringWithFormat:@"'%@'",[dataString stringByReplacingOccurrencesOfString:@"'" withString:@"''"]]];
    }
    
    NSString *query = [NSString stringWithFormat:@"INSERT INTO %@(%@) VALUES (%@)", tableName, [newCols componentsJoinedByString:@", "], [newVals componentsJoinedByString:@", "]];
    
    BOOL isInserted = [database executeUpdate:query];
    
    [database close];
    
    if(!isInserted){
        if([tableName isEqualToString:DATA_TABLE]){
            [self update:DATA_TABLE :data :[NSString stringWithFormat:@"%@ = '%@' AND %@ = '%@'",LINK_ID,data[LINK_ID],@"workspace_id",data[@"workspace_id"]]];
        }
    }
}

-(void) insertValues:(NSString*) tableName Array:(NSMutableArray*) dataArray
{
    NSString *dbPath = [self dBFilePath];
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    for (NSMutableDictionary *data in dataArray) {
        
        NSMutableArray* newCols = [[NSMutableArray alloc] init];
        NSMutableArray* newVals = [[NSMutableArray alloc] init];
        
        for (NSString *key in [data allKeys]) {
            [newCols addObject:[NSString stringWithString:key]];
            [newVals addObject:[NSString stringWithFormat:@"'%@'",[data[key] stringByReplacingOccurrencesOfString:@"'" withString:@"''"]]];
        }
        
        NSString *query = [NSString stringWithFormat:@"INSERT INTO %@(%@) VALUES (%@)", tableName, [newCols componentsJoinedByString:@", "], [newVals componentsJoinedByString:@", "]];
        
        BOOL isInserted = [database executeUpdate:query];
        
        if(!isInserted){
            NSString *whereClause = @"";
            if([tableName isEqualToString:DATA_TABLE])
            {
                whereClause = [NSString stringWithFormat:@"%@ = '%@'",LINK_ID,data[LINK_ID]];
            }
            
            NSMutableArray* valueArray = [[NSMutableArray alloc] init];
            
            for (NSString *key in [data allKeys]) {
                [valueArray addObject:[NSString stringWithFormat:@"%@ = '%@'",key,[data[key] stringByReplacingOccurrencesOfString:@"'" withString:@"''"]]];
            }
            
            NSString *updatequery = [NSString stringWithFormat:@"UPDATE %@ SET %@ WHERE %@ ", tableName, [valueArray componentsJoinedByString:@", "], whereClause];
            
            [database executeUpdate:updatequery];
        }
    }
    [database close];
}


-(void) update:(NSString*)tableName : (NSDictionary *) data :(NSString*) whereClause{
    
    FMDatabase *database = [FMDatabase databaseWithPath:[self dBFilePath]];
    [database open];

    NSMutableArray* valueArray = [[NSMutableArray alloc] init];
    
    for (NSString *key in [data allKeys]) {
        
        NSString *tempString = [NSString stringWithFormat:@"%@",data[key]];
        
        [valueArray addObject:[NSString stringWithFormat:@"%@ = '%@'",key,[tempString stringByReplacingOccurrencesOfString:@"'" withString:@"''"]]];
    }
    
    NSString *query = [NSString stringWithFormat:@"UPDATE %@ SET %@ where %@ ", tableName, [valueArray componentsJoinedByString:@", "],whereClause];

    [database executeUpdate:query];
    
    [database close];
}

-(NSMutableArray *)getDataForField:(NSString*)tableName where:(NSString *)whereClause
{
    NSString *query = @"";
    
    if (whereClause == nil) {
        query = [NSString stringWithFormat:@"SELECT * FROM %@ ",tableName];
    }
    else {
        query = [NSString stringWithFormat:@"SELECT * FROM %@ where %@",tableName, whereClause];
    }
    
    FMDatabase *database = [FMDatabase databaseWithPath:[self dBFilePath]];
    
    [database open];
    
    NSMutableArray *arrayToReturn = [[NSMutableArray alloc] init];
    
    FMResultSet *results = [database executeQuery:query];
    
    while([results next]) {
        
        NSMutableDictionary *tempDisc=[[NSMutableDictionary alloc] init];
        
        for (int i=0; i < results.columnCount;i++ )
        {
            NSString *column = [results columnNameForIndex:i];
            NSString *value =  [results stringForColumn:column];
        
            [tempDisc setObject:value == nil ? @"" : value  forKey:column];
        }
        [arrayToReturn addObject:tempDisc];
    }
    [database close];
    
    return arrayToReturn ;
    
}
-(NSMutableArray *)getDataForTable:(NSString*)tableName
{
    NSString *query = @"";
    
    
    query = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE workspace_id is null ",tableName];
    
    FMDatabase *database = [FMDatabase databaseWithPath:[self dBFilePath]];
    
    [database open];
    
    NSMutableArray *arrayToReturn = [[NSMutableArray alloc] init];
    
    FMResultSet *results = [database executeQuery:query];
    
    while([results next]) {
        
        NSMutableDictionary *tempDisc=[[NSMutableDictionary alloc] init];
        
        for (int i=0; i < results.columnCount;i++ )
        {
            NSString *column = [results columnNameForIndex:i];
            NSString *value =  [results stringForColumn:column];
        
            [tempDisc setObject:value == nil ? @"" : value  forKey:column];
        }
        [arrayToReturn addObject:tempDisc];
    }
    [database close];
    
    return arrayToReturn ;
    
}

-(NSMutableArray *)getDataForQuery:(NSString *)query
{
    NSMutableArray *arrayToReturn = [[NSMutableArray alloc]init];
    FMDatabase *database = [FMDatabase databaseWithPath:[self dBFilePath]];
    [database open];
    FMResultSet *results = [database executeQuery:query];
    
    while([results next]) {
        
        NSMutableDictionary *tempDisc=[[NSMutableDictionary alloc] init];
        
        for (int i=0; i < results.columnCount;i++ )
        {
            NSString *column = [results columnNameForIndex:i];
            NSString *value =  [results stringForColumn:column];
            
            [tempDisc setObject:value == nil ? @"" : value  forKey:column];
        }
        [arrayToReturn addObject:tempDisc];
    }
    [database close];
    
    return arrayToReturn;
}


-(BOOL)deleteRow:(NSString *)tableName Where:(NSString*)whereClause
{
    FMDatabase *database = [FMDatabase databaseWithPath:[self dBFilePath]];
    [database open];
    
    NSString *query = @"";
    
    if(whereClause!=nil){
        query = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@", tableName, whereClause];
    }
    else{
        query = [NSString stringWithFormat:@"DELETE FROM %@", tableName];
    }

    BOOL _success = [database executeUpdate:query];
    
    [database close];
    
	return _success;
}

-(void) createDatabase:(NSFileManager *)manager {
    if (![manager fileExistsAtPath:[self dBFilePath]])
    {
        [manager createFileAtPath:[self dBFilePath] contents:nil attributes:nil];
    }
    NSLog(@"%@",[self dBFilePath]);
}

-(void) createTable:tableName{
    FMDatabase *database = [FMDatabase databaseWithPath:[self dBFilePath]];
    [database open];
    NSString *query;
    
    if ([tableName isEqualToString:@"workspace_table"]) {
        query=[NSString stringWithFormat:@"create table %@(name TEXT, id INTEGER UNIQUE ,created_time  TEXT, PRIMARY KEY(id AUTOINCREMENT))",tableName];
    }
    else {
        query=[NSString stringWithFormat:@"create table %@(link_id Varchar, link TEXT ,is_video  Varchar,is_carousel Varchar,display_url TEXT,username TEXT,name TEXT, caption TEXT, profile_pic TEXT, is_posted VARchar, workspace_name TEXT, workspace_id VARCHAR, PRIMARY KEY(link_id,workspace_id))",tableName];
    }
    [database executeUpdate:query];
    [database close];
}

-(void) alterTable:(NSString*) tableName Colomn:(NSString*) colomnName DefaultValue:(NSString*) defaultValue isPrimaryKey:(BOOL) primaryKey{
    FMDatabase *database = [FMDatabase databaseWithPath:[self dBFilePath]];
    NSString *updateSQL;
    [database open];
    if (!primaryKey) {
        updateSQL = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN %@ VARCHAR DEFAULT '%@'",tableName,colomnName,defaultValue]];
    }else{
       

        updateSQL = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"ALTER TABLE %@ ADD UNIQUE(%@)",tableName,colomnName]];
    }
    [database executeUpdate:updateSQL];
    [database close];
}

-(NSString *) dBFilePath
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	return [documentsDirectory stringByAppendingPathComponent:DATABASE_NAME];
}

@end
