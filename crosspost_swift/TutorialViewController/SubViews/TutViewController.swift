//
//  TutViewController.swift
//  crosspost_swift
//
//  Created by vamika on 17/11/20.
//  Copyright © 2020 vamika. All rights reserved.
//

import UIKit

class TutViewController: UIViewController {
    @IBOutlet weak var instFour: UILabel!
    @IBOutlet weak var instThree: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var instTwo: UILabel!
    @IBOutlet weak var instOne: UILabel!
    @IBOutlet weak var dotButton: UIButton!
    @IBOutlet weak var goToIg: UIButton!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var igShare: UIButton!
    @IBOutlet weak var closeBtn: UIButton!
    var path : UIBezierPath!
    var shape, shape1 : CAShapeLayer!
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "PostRepostCollectionCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "PostRepostCollectionCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        pageController.numberOfPages = 4
        // Do any additional setup after loading the view.
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        Global.makeCornerRadius(view: goToIg, radius: Float(goToIg.frame.height/2))
        Global.makeCornerRadius(view: igShare, radius: Float(igShare.frame.height/2))
    }
    @IBAction func openIG(_ sender: Any) {
        if UIApplication.shared.canOpenURL(URL(string: "instagram://")!) {
            UIApplication.shared.open(URL(string: "instagram://")!, options: [:], completionHandler: nil)
        }else if UIApplication.shared.canOpenURL(URL(string: "https://instagram.com")!) {
            UIApplication.shared.open(URL(string: "https://instagram.com")!, options: [:], completionHandler: nil)
        }
    }

    @IBAction func onClose(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}
extension TutViewController: UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    // MARK: CollectiontionDelegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "PostRepostCollectionCell", for: indexPath) as! PostRepostCollectionCell
        cell.previewImageView.image = self.imageWithImage(image: UIImage(named: String(format: "tut_%d.jpg", indexPath.row + 1))!, scaledToSize: self.collectionView.frame.size)
        cell.playButton.isHidden = true
        cell.zoomIcon.isHidden = true
        if indexPath.row == 2{
            cell.topSampleImage.isHidden = false
        }else{
            cell.topSampleImage.isHidden = true
        }
        if indexPath.row == 0 {
            
            self.pageController.currentPage = 0
            self.deSelectAllViews()
            self.instOne.isHidden = false
            self.goToIg.isHidden = false
            let point1 = CGPoint(x: self.instTwo.frame.width/2, y: self.instOne.frame.origin.y + self .instOne.frame.height + 20);
            let point2 = CGPoint(x: 50, y: (self.instOne.frame.origin.y + self.goToIg.frame.origin.y)/2);
            let point3 = CGPoint(x: self.goToIg.frame.origin.x - 10, y: self.goToIg.frame.origin.y + 20);
            self.createOpenArrowPath(from: point1, to: point3, conftrolPoint: point2)
//                 [self createOpenArrowPathFrom:point1 to:point3 conftrolPoint:point2];

        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cellSize = CGSize(width:self.collectionView.bounds.width , height: self.collectionView.bounds.height)
        return cellSize
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        let sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return sectionInset
    }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath){
    }
    func deSelectAllViews(){
        self.instOne.isHidden = true
        self.instTwo.isHidden = true
        self.instThree.isHidden = true
        self.instFour.isHidden = true
        
        self.dotButton.isHidden = true
        self.goToIg.isHidden = true
        self.igShare.isHidden = true
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageController?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        if pageController.currentPage == 0{
            self.deSelectAllViews()
            self.instOne.isHidden = false
            self.goToIg.isHidden = false
            let point1 = CGPoint(x: self.instOne.frame.width/2, y: self.instOne.frame.origin.y + self .instOne.frame.height + 20);
            let point2 = CGPoint(x: 50, y: (self.instOne.frame.origin.y + self.goToIg.frame.origin.y)/2);
            let point3 = CGPoint(x: self.goToIg.frame.origin.x - 10, y: self.goToIg.frame.origin.y + 20);
            self.createOpenArrowPath(from: point1, to: point3, conftrolPoint: point2)
//                 [self createOpenArrowPathFrom:point1 to:point3 conftrolPoint:point2];

        }else if pageController.currentPage == 1{
            self.deSelectAllViews()
            self.instTwo.isHidden = false
            self.dotButton.isHidden = false
            let point1 = CGPoint(x: self.instTwo.frame.width/2 + 10, y: self.instTwo.frame.origin.y);
            let point2 = CGPoint(x: 50, y: (self.instTwo.frame.origin.y + self.dotButton.frame.origin.y)/2);
            let point3 = CGPoint(x: self.dotButton.frame.origin.x - 10, y: self.dotButton.frame.origin.y + self.dotButton.frame.height );
            self.createOpenArrowPath(from: point1, to: point3, conftrolPoint: point2)
        }
        else if pageController.currentPage == 2{
            self.deSelectAllViews()
          //  self.tutThree.isHidden = false
            self.instThree.isHidden = false
            
            let point1 = CGPoint(x: self.instThree.frame.width/2 + 10, y: self.instThree.frame.origin.y + 100);
            let point2 = CGPoint(x: 50, y: self.instThree.frame.origin.y + self.instThree.frame.height + 20);
            
            let point3 : CGPoint!
                point3 = CGPoint(x: self.view.frame.width/2, y: self.view.frame.height*0.63);
            
            self.createOpenArrowPath(from: point1, to: point3, conftrolPoint: point2)
        }
        else if pageController.currentPage == 3{
            self.deSelectAllViews()
            self.instFour.isHidden = false
            self.igShare.isHidden = false
            let point1 = CGPoint(x: self.instFour.frame.width/2 + 10, y: self.instFour.frame.origin.y + 131);
            let point2 = CGPoint(x: 50, y: (self.instFour.frame.origin.y + self.igShare.frame.origin.y)/2);
            let point3 = CGPoint(x: self.igShare.frame.origin.x - 10, y: self.igShare.frame.origin.y);
            self.createOpenArrowPath(from: point1, to: point3, conftrolPoint: point2)
        }else{
            self.deSelectAllViews()
        }
    }
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        pageController?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        let imageRatio = image.size.width / image.size.height
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.width/imageRatio))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    func createOpenArrowPath(from point1: CGPoint, to point3: CGPoint, conftrolPoint point2: CGPoint) {
        // create new curve shape layer
        if path != nil{
        path.removeAllPoints()
        }
        if shape != nil{
        shape.removeFromSuperlayer()
        }
        if shape1 != nil{
        shape1.removeFromSuperlayer()
        }
        
        path = UIBezierPath()

        path.move(to: point1)
        path.addQuadCurve(to: point3, controlPoint: point2)



        shape = CAShapeLayer()
        shape.path = path.cgPath
        shape.lineWidth = 2
        shape.strokeColor = UIColor.white.cgColor
        shape.fillColor = UIColor.clear.cgColor
        shape.frame = view.bounds
        view.layer.addSublayer(shape)

        let angle = atan2f(Float(point3.y - point2.y), Float(point3.x - point2.x))


        let distance: CGFloat = 5.0
        path = UIBezierPath()
        path.move(to: point3)
        path.addLine(to: calculatePoint(from: point3, angle: CGFloat(angle) + .pi / 2, distance: distance)) // to the right
        path.addLine(to: calculatePoint(from: point3, angle: CGFloat(angle), distance: distance)) // straight ahead
        path.addLine(to: calculatePoint(from: point3, angle: CGFloat(angle) - .pi / 2, distance: distance)) // to the left
        path.close()


        let pathAnimation = CABasicAnimation(keyPath: "strokeEnd")
        pathAnimation.duration = 0.2
        pathAnimation.fromValue = NSNumber(value: 0.0)
        pathAnimation.toValue = NSNumber(value: 1.0)
        shape.add(pathAnimation, forKey: "strokeEnd")

        perform(#selector(addArrow), with: self, afterDelay: 0.3)



    }
    func calculatePoint(from point: CGPoint, angle: CGFloat, distance: CGFloat) -> CGPoint {
        return CGPoint(x: point.x + CGFloat(cosf(Float(angle))) * distance, y: point.y + CGFloat(sinf(Float(angle))) * distance)
    }
    @objc func addArrow() {
        if shape1 != nil{
        shape1.removeFromSuperlayer()
        }
        shape1 = CAShapeLayer()
        shape1.path = path.cgPath
        shape1.lineWidth = 0.7
        shape1.strokeColor = UIColor.white.cgColor
        shape1.fillColor = UIColor.white.cgColor
        shape1.frame = view.bounds
        view.layer.addSublayer(shape1)
    }
   
}
