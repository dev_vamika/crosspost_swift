//
//  TutorialViewController.swift
//  crosspost_swift
//
//  Created by vamika on 15/09/20.
//  Copyright © 2020 vamika. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController {
@IBOutlet weak var tutView: UIView!
    var tutorialView = TutViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadTutView()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    func loadTutView(){
        tutorialView = TutViewController()
        tutorialView = TutViewController.init(nibName: "TutViewController", bundle: nil)
        tutorialView.view.frame = tutView.bounds
        tutorialView.closeBtn.addTarget(self, action: #selector(onCloseBtn(_:)), for: .touchUpInside)
//        purchaseViewController.delegate = self
        
        addChild(tutorialView)
        self.tutView.addSubview(tutorialView.view)
    }
    @objc func onCloseBtn(_ sender: Any?) {
        self.navigationController?.popViewController(animated: false)
//        if tutorialView.view != nil {
//            UIView.animate(withDuration: 0.35, animations: { [self] in
//                tutorialView.view.alpha = 0.0
//            }) { [self] finished in
//                tutorialView.view.alpha = 0.0
//                tutorialView.view.removeFromSuperview()
//                tutorialView = TutViewController()
//            }
//        }
    }
}
