//
//  PurchaseSuccessViewController.h
//  Cross Post
//
//  Created by Mayank Barnwal on 02/05/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PurchaseSuccessViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *letsGoBtn;

@end

NS_ASSUME_NONNULL_END
