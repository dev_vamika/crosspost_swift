//
//  PurchaseViewController.m
//  Cross Post
//
//  Created by Mayank Barnwal on 29/04/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import "PurchaseViewController.h"

@interface PurchaseViewController (){
    NSMutableArray *viewArray;
    SKProduct *globalProductToPurchase;
    UIButton *selectedBtn;
}

@end

@implementation PurchaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    viewArray = [[NSMutableArray alloc] init];
    
    [self.activityIndicator setHidden:TRUE];
    
    [self makeCornerRadius:self.plusLabel Corner:3.0];
    
    [self makeCornerRadius:self.mainContainerView Corner:10.0];
    
    [self makeCornerRadius:self.priceContainerView Corner:10.0];
    
    [self makeCornerRadius:self.startFreeWeekBtn Corner:10.0];
    
    [self addShadowToView:self.priceContainerView];
    
    [self makeCornerRadius:self.monthBtn Corner:5.0];
    
    [self makeCornerRadius:self.sixMonthBtn Corner:5.0];
    
    [self makeCornerRadius:self.oneYearBtn Corner:5.0];

    [self.monthBtn setTitleColor:DEFAULT_TINT_DARK_COLOR forState:UIControlStateNormal];
    
    [self.sixMonthBtn setTitleColor:DEFAULT_TINT_DARK_COLOR forState:UIControlStateNormal];
    
    [self.oneYearBtn setTitleColor:DEFAULT_TINT_DARK_COLOR forState:UIControlStateNormal];
    
    [self onSixMonthBtn:nil];
    
    NSString *termsString = @"Cancel anytime. Subscription auto-renews.\nBy joining you agree to our Privacy Policy and Terms of Use";
    NSRange privacyRange = [termsString rangeOfString:@"Privacy Policy"];
    NSRange termsRange = [termsString rangeOfString:@"Terms of Use"];
    
    NSMutableAttributedString *termsAttributeString = [[NSMutableAttributedString alloc] initWithString:termsString];
    
    [termsAttributeString addAttribute:NSFontAttributeName
                                 value:[UIFont fontWithName:@"Karla-Bold" size:12.0f]
                                 range:privacyRange];
    
    [termsAttributeString addAttribute:NSFontAttributeName
                                 value:[UIFont fontWithName:@"Karla-Bold" size:12.0f]
                                 range:termsRange];;
    
    [self.termsBtn setAttributedTitle:termsAttributeString forState:UIControlStateNormal];

}
-(void) makeCornerRadius:(UIView*) view Corner:(float) radius{
    view.layer.cornerRadius = radius;
    view.layer.masksToBounds = true;
}
-(void) addShadowToView:(UIView*) view {
    
    view.layer.masksToBounds = NO;
    
    view.layer.shadowColor = [[UIColor blackColor] CGColor];
    
    view.layer.shadowOffset = CGSizeMake(0,0);
    view.layer.shadowOpacity = 0.3;
}

- (IBAction)onMonthBtn:(id)sender {
    self.monthBtn.layer.borderWidth = 2.0;
    self.monthBtn.layer.borderColor = [DEFAULT_TINT_DARK_COLOR CGColor];
    
    self.sixMonthBtn.layer.borderWidth = 0.5;
    self.sixMonthBtn.layer.borderColor = [DEFAULT_TINT_LIGHT_COLOR CGColor];
    
    self.oneYearBtn.layer.borderWidth = 0.5;
    self.oneYearBtn.layer.borderColor = [DEFAULT_TINT_LIGHT_COLOR CGColor];
    
    selectedBtn = self.monthBtn;
    
    globalProductToPurchase = [self getProduct:0];
}

- (IBAction)onSixMonthBtn:(id)sender {
    
    self.monthBtn.layer.borderWidth = 0.5;
    self.monthBtn.layer.borderColor = [DEFAULT_TINT_LIGHT_COLOR CGColor];
    
    self.sixMonthBtn.layer.borderWidth = 2.0;
    self.sixMonthBtn.layer.borderColor = [DEFAULT_TINT_DARK_COLOR CGColor];
    
    self.oneYearBtn.layer.borderWidth = 0.5;
    self.oneYearBtn.layer.borderColor = [DEFAULT_TINT_LIGHT_COLOR CGColor];
    
    selectedBtn = self.sixMonthBtn;
    
    globalProductToPurchase = [self getProduct:1];
}


- (IBAction)onOneYearBtn:(id)sender {
    
    self.monthBtn.layer.borderWidth = 0.5;
    self.monthBtn.layer.borderColor = [DEFAULT_TINT_LIGHT_COLOR CGColor];
    
    self.sixMonthBtn.layer.borderWidth = 0.5;
    self.sixMonthBtn.layer.borderColor = [DEFAULT_TINT_LIGHT_COLOR CGColor];
    
    self.oneYearBtn.layer.borderWidth = 2.0;
    self.oneYearBtn.layer.borderColor = [DEFAULT_TINT_DARK_COLOR CGColor];
    
    selectedBtn = self.oneYearBtn;
    
    globalProductToPurchase = [self getProduct:2];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productLoaded)
                                                 name:@"ProductLoaded"
                                               object:nil];
    
}

-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ProductLoaded" object:nil];
}

-(void) productLoaded{
    [self addViews];
}

-(void) updateViewConstraints{
    [super updateViewConstraints];
    
    [self addViews];

}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if(isiPhone5s){
        self.termsBtn.titleLabel.font = [self.termsBtn.titleLabel.font fontWithSize:10.0];
        self.topDistance.constant = -35;
    }
}

-(void) updateFrame{
    
}

-(void) addViews{
    
    self.monthBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.sixMonthBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.oneYearBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.monthBtn setTitle:[self getPriceText:[self getProduct:0] Tag:0] forState:UIControlStateNormal];
    
    [self.sixMonthBtn setTitle:[self getPriceText:[self getProduct:1] Tag:1] forState:UIControlStateNormal];
    
    [self.oneYearBtn setTitle:[self getPriceText:[self getProduct:2] Tag:2] forState:UIControlStateNormal];
}

//-(void) onButtonClicked:(int) tag{
//    for (ButtonViewController *buttonView in viewArray) {
//        [buttonView.view removeFromSuperview];
//        [UIView animateWithDuration:0.5 animations:^{
//            CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 1.0f);
//            buttonView.view.transform = transform;
//        } completion:^(BOOL finished) {
//        }];
//    }
//    for (ButtonViewController *buttonView in viewArray) {
//        if(buttonView.view.tag != tag){
//            [self.view addSubview:buttonView.view];
//        }
//    }
//    ButtonViewController *buttonView = viewArray[tag];
//
//    if([PurchaseHandler sharedInstance].productsArray.count > tag)
//        globalProductToPurchase = [self getProduct:tag];
//
//
//    [self.view addSubview:buttonView.view];
//
//    [UIView animateWithDuration:0.5 animations:^{
//        CGAffineTransform transform = CGAffineTransformMakeScale(1.12f, 1.12f);
//        buttonView.view.transform = transform;
//    } completion:^(BOOL finished) {
//    }];
//}

-(SKProduct*) getProduct:(int) i {
    for (SKProduct *product in [PurchaseHandler sharedInstance].productsArray) {
        if(i == 0){
            if([product.productIdentifier isEqualToString:ONE_MONTH_PRIDUCT_ID]){
                return product;
            }
        }
        if(i == 1){
            if([product.productIdentifier isEqualToString:SIX_MONTH_PRIDUCT_ID]){
                return product;
            }
        }
        if(i == 2){
            if([product.productIdentifier isEqualToString:ONE_YEAR_PRIDUCT_ID]){
                return product;
            }
        }
    }
    return nil;
}

-(NSString*) getProductDuration:(int) index {
    
    switch (index) {
        case 0:
            return @"1 Month";
            break;
        case 1:
            return @"6 Months";
        break;
        case 2:
            return @"1 Year";
        break;
        default:
            break;
    }
    return @"";
}

-(NSString*) getPriceText:(SKProduct*) product Tag:(int)tag{
    
    if(tag == 0){
        return [NSString stringWithFormat:@"%@\nper month",[self getCurrencyText:[NSString stringWithFormat:@"%@",product.price]]];
    }
                
    if(tag == 1){
        return [NSString stringWithFormat:@"%@\nper half-year",[self getCurrencyText:[NSString stringWithFormat:@"%@",product.price]]];
    }
    
    if(tag == 2){
        return [NSString stringWithFormat:@"%@\nper year",[self getCurrencyText:[NSString stringWithFormat:@"%@",product.price]]];
    }
    
    return @"";
    
}
-(NSString*) getCurrencyText:(NSString*)value{
    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setLocale:[NSLocale currentLocale]];
    [currencyFormatter setMaximumFractionDigits:2];
    [currencyFormatter setMinimumFractionDigits:2];
    [currencyFormatter setAlwaysShowsDecimalSeparator:YES];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *someAmount = [NSNumber numberWithFloat:[value floatValue]];
    NSString *string = [currencyFormatter stringForObjectValue:someAmount];
    
    return string;
}
- (IBAction)onStartFreeTrialBtn:(id)sender {
    if(globalProductToPurchase != nil){
        if([self.delegate respondsToSelector:@selector(buyProduct:)]){
            [self.delegate buyProduct:globalProductToPurchase];
        }
    }
    else{
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Woops!!" message:@"Please wait!! or check your internet connection" preferredStyle:UIAlertControllerStyleAlert];

        [alertView addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:^{
                
            }];
        }]];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertView animated:YES completion:nil];
    }
}
- (IBAction)onRestoreBtn:(id)sender {
    if([self.delegate respondsToSelector:@selector(restoreTransaction)]){
        [self.delegate restoreTransaction];
    }
}
- (IBAction)onPrivacyPolicyBtn:(id)sender {
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"https://synchsofthq.com/privacy-policy-for-apps/"]]){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://synchsofthq.com/privacy-policy-for-apps/"]];
    }
}
@end
