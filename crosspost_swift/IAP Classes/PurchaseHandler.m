//
//  PurchaseHandler.m
//  Plann
//
//  Created by Mayank Barnwal on 05/12/17.
//  Copyright © 2017 SynchSoft Technologies. All rights reserved.
//

#import "PurchaseHandler.h"


NSString *const kSandboxServer = @"https://sandbox.itunes.apple.com/verifyReceipt";
NSString *const kLiveServer = @"https://buy.itunes.apple.com/verifyReceipt";

@interface PurchaseHandler()<SKProductsRequestDelegate,SKPaymentTransactionObserver>{
    SKProductsRequest * productsRequest;
}

@end

@implementation PurchaseHandler
    
+ (PurchaseHandler *)sharedInstance {
    
    static PurchaseHandler *sharedObject;
    if(sharedObject == nil){
        sharedObject = [[PurchaseHandler alloc] init];
        [sharedObject requestProducts];
        [sharedObject startValidatingAppStoreReceipt:nil];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:sharedObject];
    }
    return sharedObject;
}

-(void) buyProduct:(NSString*) productIdentifier{
    NSLog(@"Purchasing product... %@",productIdentifier);
    
    if (self.productsArray == nil || self.productsArray.count == 0) {
        NSLog(@"No products are available. Did you initialize MKStoreKit by calling [[MKStoreKit sharedKit] startProductRequest]?");
    }
    if (![SKPaymentQueue canMakePayments]) {
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"In App Purchasing Disabled"
                                                                            message:@"Check your parental control settings and try again later" preferredStyle:UIAlertControllerStyleAlert];
        
        [controller addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if([self.delegate respondsToSelector:@selector(transactionFailed)]){
                [self.delegate transactionFailed];
            }
        }]];
        
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:controller animated:YES completion:nil];
    }
    BOOL isIdAvailable = false;
    
    for (SKProduct *thisProduct in self.productsArray) {
        if ([thisProduct.productIdentifier isEqualToString:productIdentifier]) {
            SKPayment *payment = [SKPayment paymentWithProduct:thisProduct];
            [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
            [[SKPaymentQueue defaultQueue] addPayment:payment];
            isIdAvailable = true;
        }
    }
    if(!isIdAvailable){
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"In App Purchasing Disabled"
                                                                            message:@"Check your parental control settings and try again later" preferredStyle:UIAlertControllerStyleAlert];
        
        [controller addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if([self.delegate respondsToSelector:@selector(transactionFailed)]){
                [self.delegate transactionFailed];
            }
        }]];
        
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:controller animated:YES completion:nil];
    }
}

-(SKProduct*)getProduct:(NSString*) productIdentifier{
    for (SKProduct *thisProduct in self.productsArray) {
        if ([thisProduct.productIdentifier isEqualToString:productIdentifier]) {
            return thisProduct;
        }
    }
    return nil;
}

-(void) restorePurchases{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

-(void) requestProducts{
    NSSet *productIdentifiers = [NSSet setWithObjects:
                                 ONE_MONTH_PRIDUCT_ID,
                                 SIX_MONTH_PRIDUCT_ID,
                                 ONE_YEAR_PRIDUCT_ID,
                                 nil];
    
    productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}

- (void)startValidatingAppStoreReceipt:(RequestCompletionHandler) completionHandler {
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSError *receiptError;
    BOOL isPresent = [receiptURL checkResourceIsReachableAndReturnError:&receiptError];
    if (!isPresent) {
        return;
    }
    NSData *receiptData = [NSData dataWithContentsOfURL:receiptURL];
    if (!receiptData) {
        NSLog(@"Receipt exists but there is no data available. Try refreshing the reciept payload and then checking again.");
        return;
    }
    NSError *error;
    NSMutableDictionary *requestContents = [NSMutableDictionary dictionaryWithObject:
                                            [receiptData base64EncodedStringWithOptions:0] forKey:@"receipt-data"];
    NSString *sharedSecret = IN_APP_SHARED_KEY;
    if (sharedSecret) requestContents[@"password"] = sharedSecret;
    
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:requestContents options:0 error:&error];
#ifdef DEBUG
    NSMutableURLRequest *storeRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:kSandboxServer]];
#else
    NSMutableURLRequest *storeRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:kLiveServer]];
#endif
    
    [storeRequest setHTTPMethod:@"POST"];
    [storeRequest setHTTPBody:requestData];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:storeRequest completionHandler:^(NSData* data, NSURLResponse* response, NSError *error) {
        if (!error) {
            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            NSInteger status = [jsonResponse[@"status"] integerValue];
            
            if (status != 0) {
                
            } else {
                NSMutableDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                
                if (jsonResponse[@"receipt"] != [NSNull null]) {
                    
                    NSMutableArray *performersArray = [jsonObject objectForKey:@"latest_receipt_info"];
                    
                    NSArray *sortedArrayCopy = [performersArray sortedArrayUsingComparator: ^(id obj1, id obj2) {
                        
                        NSDate *startDate1 = [obj1 valueForKey:@"expires_date"];
                        NSDate *startDate2 = [obj2 valueForKey:@"expires_date"];
                        
                        return [startDate1 compare:startDate2];
                        
                    }];
                    
                    performersArray = [sortedArrayCopy mutableCopy];
                    
                    NSMutableDictionary *purchasedDict = [[NSMutableDictionary alloc] init];
                    
                    NSMutableDictionary *dictToSave = [[NSMutableDictionary alloc] init];
                    
                    for (NSDictionary *reciept in performersArray) {
                        purchasedDict = [reciept mutableCopy];
                    }
                    
                    NSDate *expiryDateFromServer = [self getLocalDateTimeFromGMT:purchasedDict[@"expires_date"]];
                    NSDate *expiryDateFromLocal = [self getExpiryDateOfProduct:[self getPurchasedProductDict][@"PurchasedIdentifier"]];
                    
                    if([expiryDateFromLocal compare:expiryDateFromServer] == NSOrderedAscending || [expiryDateFromLocal compare:expiryDateFromServer] == NSOrderedSame || expiryDateFromLocal == nil){
                        dictToSave[@"PurchasedIdentifier"] = purchasedDict[@"product_id"];
                        dictToSave[@"ExpiryDate"] = [NSString stringWithFormat:@"%@",purchasedDict[@"expires_date"]];
                        dictToSave[@"PurchaseDate"] = [NSString stringWithFormat:@"%@",purchasedDict[@"purchase_date"]];
                        
                        [dictToSave writeToFile:[NSString stringWithFormat:@"%@/%@",[self purchasedRecordLocation],PURCHASED_RECORD_LIST] atomically:YES];
                    }
                    
                    NSLog(@"dictToSave = %@",dictToSave);
                
                }
            }
        }
        else{
            NSLog(@"Error in loading reciept = %@",error);
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if(completionHandler != nil){
                completionHandler(nil);
            }
        });
        
    }] resume];
}
-(NSString*) purchasedRecordLocation{
    
    NSString *dataPath = [[self documentFilePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"/purchased_record"]];
    
    NSError *error;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    
    return dataPath;
}
-(NSString *) documentFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return  documentsDirectory;
}
-(NSDate *)getLocalDateTimeFromGMT:(NSString *)strDate{
    NSDateFormatter *dtFormat = [[NSDateFormatter alloc] init];
    [dtFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss VV"];
    [dtFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSDate *aDate = [dtFormat dateFromString:strDate];
    [dtFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dtFormat setTimeZone:[NSTimeZone systemTimeZone]];
    return aDate;
}

-(NSDictionary*) getPurchasedProductDict{
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self purchasedRecordLocation],PURCHASED_RECORD_LIST]];
    if(dict[@"PurchasedIdentifier"] != nil && dict[@"PurchasedIdentifier"] != nil) {
        return dict;
    }
    return nil;
}

-(NSDate*) getExpiryDateOfProduct:(NSString*) productid{
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[self purchasedRecordLocation],PURCHASED_RECORD_LIST]];
    
    if([dict[@"PurchasedIdentifier"] isEqualToString:productid]){
        if(![dict[@"ExpiryDate"] isEqualToString:@""]){
            return [self getLocalDateTimeFromGMT:dict[@"ExpiryDate"]];
        }
    }
    return nil;
}

-(void) completeTransaction:(SKPaymentTransaction*) transaction{
    if(transaction.transactionState == SKPaymentTransactionStatePurchased){
        if(transaction.payment != nil && ![transaction.payment.productIdentifier isEqualToString:@""]){
            NSLog(@"Transaction date = %@ Transaction Identifier = %@",transaction.payment.productIdentifier,transaction.transactionDate);
            
            NSMutableDictionary *dictToSave = [[NSMutableDictionary alloc] init];
            
            
            dictToSave[@"PurchasedIdentifier"] = transaction.payment.productIdentifier;
            dictToSave[@"ExpiryDate"] = @"";
            dictToSave[@"PurchaseDate"] = @"";
            
            [dictToSave writeToFile:[NSString stringWithFormat:@"%@/%@",[self purchasedRecordLocation],PURCHASED_RECORD_LIST] atomically:YES];
            
            [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"purchasing_uploaded"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self startValidatingAppStoreReceipt:^(NSDictionary *responseDict) {
                if([self.delegate respondsToSelector:@selector(transactionSuccessful:)]){
                    [self.delegate transactionSuccessful:transaction];
                }
            }];
        }
    }
}
-(void) failedTransaction:(SKPaymentTransaction*) transaction{
    
    NSString *messageToShow = @"";
    
    switch (transaction.error.code) {
       case SKErrorUnknown:
            messageToShow = @"Cannot connect to iTunes Store";
           break;
       case SKErrorClientInvalid:
            messageToShow = @"You are not allowed to request";
           break;
       case SKErrorPaymentCancelled:
           messageToShow = @"You have cancelled the request";
           break;
       case SKErrorPaymentInvalid:
            messageToShow = @"Invalid request";
           break;
       case SKErrorPaymentNotAllowed:
           messageToShow = @"You are not allowed to make a payment";
           break;
       default:
            messageToShow = @"Cannot connect to iTunes Store";
           break;
    }
    
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"eep!!"
                                                                        message:messageToShow preferredStyle:UIAlertControllerStyleAlert];
    
    [controller addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }]];
    
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:controller animated:YES completion:nil];
    
    if([self.delegate respondsToSelector:@selector(transactionFailed)]){
        [self.delegate transactionFailed];
    }
}

-(void) restoreTransaction:(SKPaymentTransaction*) transaction{
    [self startValidatingAppStoreReceipt:^(NSDictionary *responseDict) {
            if([self.delegate respondsToSelector:@selector(transactionRestored)]){
                [self.delegate transactionRestored];
            }
    }];
}

#pragma mark SKProductsRequestDelegate
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    if(response.products != nil && response.products.count != 0){
        self.productsArray = response.products;
        NSLog(@"Found Products : ");
        for (SKProduct *product in self.productsArray) {
            NSLog(@"Identifier = %@ Price = %@",product.productIdentifier,product.price);
        }
    }
    else{
        NSLog(@"No product Found");
    }
    if(response.invalidProductIdentifiers){
        NSLog(@"Invalid products ids = %@",response.invalidProductIdentifiers);
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ProductLoaded" object:nil userInfo:nil];
    });
}

#pragma mark SKPaymentTransactionOBserver

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    
    NSLog(@"queue.transactions.count = %lu,  transactions.count = %lu",(unsigned long)queue.transactions.count,(unsigned long)transactions.count);
    
    for (SKPaymentTransaction * transaction in transactions) {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                if([transactions lastObject] == transaction)
                    [self restoreTransaction:transaction];
                
            default:
                break;
        }
    };
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error{
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"eep!!"
                                                                        message:error.userInfo[NSLocalizedDescriptionKey] preferredStyle:UIAlertControllerStyleAlert];
    
    [controller addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }]];
    
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:controller animated:YES completion:nil];
    
    if([self.delegate respondsToSelector:@selector(transactionFailed)]){
        [self.delegate transactionFailed];
    }
    for (SKPaymentTransaction * transaction in queue.transactions) {
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    };
}

- (BOOL)paymentQueue:(SKPaymentQueue *)queue shouldAddStorePayment:(SKPayment *)payment forProduct:(SKProduct *)product{
    return YES;
}

@end
