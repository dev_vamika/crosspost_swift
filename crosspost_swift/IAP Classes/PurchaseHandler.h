//
//  PurchaseHandler.h
//  Plann
//
//  Created by Mayank Barnwal on 05/12/17.
//  Copyright © 2017 SynchSoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#define ONE_MONTH_PRIDUCT_ID    @"crosspost.onemonth"
#define SIX_MONTH_PRIDUCT_ID    @"crosspost.sixmonth"
#define ONE_YEAR_PRIDUCT_ID     @"crosspost.oneyear"
#define IN_APP_SHARED_KEY @"cddcca69a3e74f61a9855993e87eee9a"
#define PURCHASED_RECORD_LIST           @"purchased_record.plist"
typedef void(^RequestCompletionHandler)(NSDictionary *responseDict);

@protocol PurchaseHandlerDelegate <NSObject>

-(void) transactionFailed;
-(void) transactionSuccessful:(SKPaymentTransaction*) transaction;
-(void) transactionRestored;

@end


@interface PurchaseHandler : NSObject

+ (PurchaseHandler *)sharedInstance;

@property (nonatomic,strong) NSArray *productsArray;

@property (weak, nonatomic) id<PurchaseHandlerDelegate> delegate;

-(void) buyProduct:(NSString*) productIdentifier;

-(void) restorePurchases;

-(SKProduct*)getProduct:(NSString*) productIdentifier;

-(NSDate*) getExpiryDateOfProduct:(NSString*) productid;

-(NSDictionary*) getPurchasedProductDict;

@end
