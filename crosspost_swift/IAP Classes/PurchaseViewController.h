//
//  PurchaseViewController.h
//  Cross Post
//
//  Created by Mayank Barnwal on 29/04/20.
//  Copyright © 2020 Mayank Barnwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
#import "PurchaseHandler.h"
#define isiPhone5s                  [UIScreen mainScreen].bounds.size.height == 568
#define COLOR_RGB(r,g,b,a)          [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define DEFAULT_TINT_DARK_COLOR     COLOR_RGB(0,35,102,1.0)

#define DEFAULT_TINT_LIGHT_COLOR    COLOR_RGB(66,150,180,1.0)
@protocol PurchaseViewControllerDelegate <NSObject>

-(void) buyProduct:(SKProduct*) product;

-(void) restoreTransaction;

@end

@interface PurchaseViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *crossPostLbl;
@property (weak, nonatomic) IBOutlet UIButton *crossBtn;
@property (weak, nonatomic) IBOutlet UIButton *startFreeWeekBtn;

@property (weak, nonatomic) IBOutlet UILabel *plusLabel;
@property (nonatomic,weak) id<PurchaseViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIView *mainContainerView;

@property (weak, nonatomic) IBOutlet UIView *priceContainerView;
@property (weak, nonatomic) IBOutlet UIButton *monthBtn;
@property (weak, nonatomic) IBOutlet UIButton *sixMonthBtn;
@property (weak, nonatomic) IBOutlet UIButton *oneYearBtn;
@property (weak, nonatomic) IBOutlet UIButton *termsBtn;
@property (weak, nonatomic) IBOutlet UIButton *restoreBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topDistance;

@end
