//
//  Global.swift
//  crosspost_swift
//
//  Created by vamika on 07/08/20.
//  Copyright © 2020 vamika. All rights reserved.
//

import UIKit
import StoreKit
import CommonCrypto
import ImageIO
import MobileCoreServices
import AVFoundation
var GET_USER_MEDIA_URL_1 : String!
var GET_USER_MEDIA_URL_2 : String!
var GET_USER_MEDIA_URL_3 : String!
var queryHash : String!, queryId : String!
let DATABASE_NAME   = "Repost.sqlite"
let DATA_TABLE      = "migration_table"

let MEDIA_TABLE     = "media_table"
let WORSPACE_TABLE      = "workspace_table"
let MIGRATION_TABLE      = "data_table"
let SORT_CODE_DICT = "sort_code_dict"

let LINK_ID         = "link_id"
let LINK            = "link"
let IS_VIDEO        = "is_video"
let IS_CAROUSEL     = "is_carousel"
let DISPLAY_URL     = "display_url"
let USER_NAME       = "username"
let FULL_NAME       = "name"
let CAPTION         = "caption"
let PROFILE_URL     = "profile_pic"
let IS_POSTED       = "is_posted"
let WORK_SPACE_NAME         = "workspace_name"
let WORK_SPACE_NAME_ID         = "workspace_id"
let MEDIA_ID        = "media_id"
let MEDIA_URL       = "media_url"
let THUMBNAIL_URL   = "thumbnail_url"

let DISPLAY_PRIVIEW     = "display_preview"
let DISPLAY_MEDIA       = "diaplay_media"


let YES_STRING      = "yes"
let NO_STRING       = "no"

let ONE_MONTH_PRIDUCT_ID    = "crosspost.onemonth"
let SIX_MONTH_PRIDUCT_ID    = "crosspost.sixmonth"
let ONE_YEAR_PRIDUCT_ID     = "crosspost.oneyear"

let PURCHASED_RECORD_LIST           = "purchased_record.plist"

let IN_APP_SHARED_KEY = "cddcca69a3e74f61a9855993e87eee9a"

let DEFAULT_QUERY_HASH  = "42323d64886122307be10013ad2dcc44"

let DEFAULT_QUERY_ID  =  "17888483320059182"

let ALLOWED_POST         =       60
let POST_COUNT          = "post_count"
let MONTH_VALUE         = "month_value"
let SHOW_GUIDE_POUP          = "SHOW_GUIDE_POUP"

let APP_SESSION_COUNT   = "app_session_count"


let USERNAME_STRING         = "username_string"
let HASHTAG_STRING          = "hashtag_string"
let SEARCH_DATE             = "search_date"
let SEARCHED_STRINGS        = "searched_strings"
let GET_USER_MEDIA_URL      = "https://www.instagram.com/explore/tags/%@/?__a=1"
let VARIABLES               = "&variables={\"id\":\"%@\",\"first\":%@,\"after\":\"%@\"}"
let GET_DATA_COUNT          = "10"
let MANAGE_SUBSCRIPTION_URL = "itms-apps://apps.apple.com/account/subscriptions"
let FOLLOW_US_WITH_APP_URL = "instagram://user?username=crosspost.app"
let FOLLOW_US_WITH_WEB_URL = "https://www.instagram.com/crosspost.app/"
let CURRENT_WORK_SPACE         = "current_workspace"
let CURRENT_WORK_SPACE_ID       = "current_workspace_id"
let SHARE_APP_TEXT              = "My Crosspost app makes it easy to share photos & videos from Instagram. Follow on https://instagram.com/crosspost.app"
let GOOGLE_TRACK_ID = "AIzaSyCInE7ewLH6jvkoXx7rnw0eEV11WKTkUmA"
let HOME_SCREEN_VIEW_TYPE = "type"
let isiPad  = UIDevice().userInterfaceIdiom == UIUserInterfaceIdiom.pad
let isiPhone4S = UIScreen.main.bounds.size.height < 500
let isiPhone6 = UIScreen.main.bounds.size.height == 667
let isiPhone6Plus = UIScreen.main.bounds.size.height == 736
let isiPhone5s = UIScreen.main.bounds.size.height == 568
let isiPhoneX = UIScreen.main.bounds.size.height == 812
let isiPhone11 = UIScreen.main.bounds.size.height == 896

func COLOR_RGB(r:Float,g:Float,b:Float,a:Float) -> UIColor{return UIColor(red: CGFloat(r)/255.0, green: CGFloat(g)/255.0, blue: CGFloat(b)/255.0, alpha: CGFloat(a))}

let DEFAULT_TINT_DARK_COLOR    = COLOR_RGB(r: 0,g: 35,b: 102,a: 1.0)

let DEFAULT_TINT_LIGHT_COLOR   = COLOR_RGB(r: 66,g: 150,b: 180,a: 1.0)
let APP_REVIEW_LINK          = "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=1510907417&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8"






var waitingView  = WaitingViewController()
var cookiesArray = NSMutableArray()
var avPlayer : AVPlayer?
var videoLayer = AVPlayerLayer()
class Global: NSObject {
    
    static func showPermissionDeniedErrorMsg() {
        let alertView = UIAlertController(title: "eep!", message: "We can't access your photos without your permission! Please jump into your phone settings so you can add media to CrossPost.", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.openURL(url)
            }
        })
        alertView.addAction(ok)
        UIApplication.shared.keyWindow?.rootViewController?.present(alertView, animated: true)
    }
    static func setLeftPaddingPoints(field:UITextField){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 0))
        field.leftView = paddingView
        field.leftViewMode = .always
        }
    
    static func openRateUsPopUp(){
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        } else {
            let alertController = UIAlertController(title: "Enjoying Cross Post?", message: "Please leave a review for us to make it better.", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Yes!", style: .default, handler: { action in
                alertController.dismiss(animated: true)
                if let url = URL(string: APP_REVIEW_LINK) {
                    UIApplication.shared.openURL(url)
                }
                UserDefaults.standard.synchronize()
            }))
            alertController.addAction(UIAlertAction(title: "Not Now", style: .default, handler: { action in
                alertController.dismiss(animated: true)
            }))
             UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true)
        }
    }
    static func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        let imageRatio = image.size.width / image.size.height
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.width/imageRatio))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    static func addWaitingView(waitingViewLabel: String)
    {
        UIApplication.shared.keyWindow?.rootViewController?.view.isUserInteractionEnabled = false
        UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(waitingView.view)
        waitingView.view.frame = UIScreen.main.bounds
        waitingView.messageLabel.textColor = self.getTintColor()
        waitingView.messageLabel.text = waitingViewLabel
        waitingView.loader.color = self.getTintColor()
        waitingView.view.isHidden = false
        waitingView.view.alpha = 0.0
        waitingView.loader.startAnimating()
        UIView.animate(withDuration: 0.35, animations: {
             waitingView.view.alpha = 1.0
        }) { (finished) in
            if finished
            {
             waitingView.view.alpha = 1.0
            }
        }
    }
    static func removeWaitingView()
    {
        if waitingView.view == nil {
            return
        }
        
        waitingView.loader.stopAnimating()
        UIView.animate(withDuration: 0.35, animations: {
            waitingView.view.alpha = 0.0
        }) { (finished) in
            if finished
            {
                waitingView.view.alpha = 0.0
                waitingView.view.isHidden = true
                
            }
        }
        UIApplication.shared.keyWindow?.rootViewController?.view.isUserInteractionEnabled = true
    }
    static func fullImageLocation() -> String? {

        let dataPath = URL(fileURLWithPath: Global.documentFilePath()!).appendingPathComponent("full_image").path

        if !FileManager.default.fileExists(atPath: dataPath) {
            do {
                try FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: false, attributes: nil)
            } catch {
            }
        }

        return dataPath
    }
    static func videoLocation() -> String? {

        let dataPath = URL(fileURLWithPath: Global.documentFilePath()!).appendingPathComponent("videos").path

        if !FileManager.default.fileExists(atPath: dataPath) {
            do {
                try FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: false, attributes: nil)
            } catch {
            }
        }

        return dataPath
    }

    static func userImageLocation() -> String? {

        let dataPath = URL(fileURLWithPath: Global.documentFilePath()!).appendingPathComponent("user_image").path

        if !FileManager.default.fileExists(atPath: dataPath) {
            do {
                try FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: false, attributes: nil)
            } catch {
            }
        }

        return dataPath

    }
    static func purchasedRecordLocation() -> String? {
        let dataPath = URL(fileURLWithPath: Global.documentFilePath()!).appendingPathComponent("purchased_record").path
        if !FileManager.default.fileExists(atPath: dataPath) {
            do {
                try FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: false, attributes: nil)
            } catch {
            }
        }

        return dataPath
    }
    static func resizedImage(at inputImage: UIImage, for size: CGSize) -> UIImage? {
        let renderer = UIGraphicsImageRenderer(size: size)
        return renderer.image { (context) in
            inputImage.draw(in: CGRect(origin: .zero, size: size))
        }
    }
    static func getTintColor() -> UIColor{
        if UIApplication.shared.windows.first?.rootViewController!.traitCollection.userInterfaceStyle == .dark {
            return DEFAULT_TINT_LIGHT_COLOR;
        }else{
            return DEFAULT_TINT_DARK_COLOR;
        }
        return DEFAULT_TINT_DARK_COLOR;
    }
    static func makeCornerRadius(view: UIView, radius: Float){
        view.layer.cornerRadius = CGFloat(radius);
        view.layer.masksToBounds = true;
    }
    static func addBorderWithhRadius(view: UIView, radius: Float, borderWidth: Float, borderColor: UIColor){
        view.layer.cornerRadius = CGFloat(radius);
        view.layer.borderWidth = CGFloat(borderWidth)
        view.layer.borderColor = borderColor.cgColor
        view.layer.masksToBounds = true;
    }
    static func addShadowAtBottom(view: UIView){
        view.layer.shadowColor = UIColor.systemGroupedBackground.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view.layer.shadowOpacity = 0.5
        view.layer.shadowRadius = 0.0
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 4.0
    }
    static func addShadowToView(view: UIView){
        let shadowSize : CGFloat = 5.0
            let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                       y: -shadowSize / 2,
                                                       width: view.frame.size.width + shadowSize,
                                                       height: view.frame.size.height + shadowSize))
        
            view.layer.masksToBounds = false
            view.layer.shadowColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5).cgColor
    
            view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            view.layer.shadowOpacity = 0.5
            view.layer.shadowPath = shadowPath.cgPath
    }
    static func zoonInAnimationOnView(view: UIView){
        view.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001);
        UIView.animate(withDuration: 0.3) {
            view.transform = CGAffineTransform(scaleX: 1.0,y: 1.0);
        } completion: { (finished) in
            //
        }
    }
    static func zoonOutAnimationOnView(view: UIView){
        UIView.animate(withDuration: 0.3) {
            view.transform = CGAffineTransform(scaleX: 0.1,y: 0.1);
        } completion: { (finished) in
            view.transform = CGAffineTransform(scaleX: 0.0,y: 0.0);
            
        }
    
    }
    static func roundedAllCornersToView(view: UIView){
        let maskPath1 = UIBezierPath(roundedRect: view.bounds,
                                     byRoundingCorners: [.topLeft , .topRight, .bottomRight, .bottomLeft],
            cornerRadii: CGSize(width: 8, height: 8))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = view.bounds
        maskLayer1.path = maskPath1.cgPath
        view.layer.mask = maskLayer1
    }
    static func newVideoLocation() -> String {
        let dataPath = URL(fileURLWithPath: self.documentFilePath()!).appendingPathComponent("new_videos").path
        if !FileManager.default.fileExists(atPath: dataPath) {
            do {
                try FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: false, attributes: nil)
            } catch {
            }
        }
        return dataPath
    }
    static func documentFilePath() -> String? {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).map(\.path)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    static func getValidString(string:String)->String{
                let valueString = "\(string)"
        
                if valueString.lowercased().contains("null") {
                    return ""
                }
        
                if valueString.lowercased().contains("nil") {
                    return ""
                }
        
               return "\(valueString)"
    }
    
    static func getActiveSubscriptionName() -> String{
        
        let purchasedProductId = self.getActivePurchasedIdentifier()
        
        if purchasedProductId == ONE_YEAR_PRIDUCT_ID {
            return "Crosspost Annual";
        }
        if purchasedProductId == SIX_MONTH_PRIDUCT_ID{
            return "Crosspost Half Yearly";
        }
        if purchasedProductId == ONE_MONTH_PRIDUCT_ID{
            return "Crosspost Monthly";
        }
        
        return "Free";
    }
    static func getActivePurchasedIdentifier() -> String? {
        let productDict = PurchaseHandler.sharedInstance().getPurchasedProductDict()
        if let productDict : NSDictionary = productDict as NSDictionary? {
            let productExpiryDate = PurchaseHandler.sharedInstance().getExpiryDate(ofProduct: productDict.value(forKey: "PurchasedIdentifier") as? String)
            if let productExpiryDate = productExpiryDate {
                if Date().compare(productExpiryDate) == .orderedAscending {//productExpiryDate == nil
                    return productDict["PurchasedIdentifier"] as? String
                }
            }
        }
        return ""
    }

    static func isPaidUser() -> Bool {
        if !(Global.getActivePurchasedIdentifier() == "") {
            return true
        }
        return false
    }
    static func cookietoUse(userDict: NSMutableDictionary) {

        let url = "https://www.instagram.com/\((userDict["user_name"])!)"

        var cookiesArrayCopy: [HTTPCookie]? = nil
        if let url1 = URL(string: url) {
            cookiesArrayCopy = HTTPCookieStorage.shared.cookies(for: url1)
        }

        let properties = NSMutableDictionary()
        properties.setValue("www.instagram.com", forKey: HTTPCookiePropertyKey.domain.rawValue)
        properties.setValue("/", forKey: HTTPCookiePropertyKey.path.rawValue)
        properties.setValue("ig_pr", forKey: HTTPCookiePropertyKey.name.rawValue)
        properties.setValue("2", forKey: HTTPCookiePropertyKey.value.rawValue)
                
        var cookie: HTTPCookie? = nil
        if let properties = properties as? [HTTPCookiePropertyKey : Any] {
            cookie = HTTPCookie(properties: properties)
        }

        if let cookie = cookie {
            cookiesArrayCopy?.append(cookie)
        }

        for cookie in cookiesArrayCopy ?? [] {
            if cookie.name == "csrftoken" {
                userDict["csrf"] = cookie.value
            }
        }
        cookiesArray = NSMutableArray(array: cookiesArrayCopy!)
    }
    class func getGis(userDic:[AnyHashable : Any], variables: String?, userAgent: String?) -> String? {
        var variables = variables

        variables = variables?.replacingOccurrences(of: "&variables=", with: "")

        let utfEncode = "\("\((userDic["rhx_gis"])!):\(variables ?? "")".utf8CString)"

        //    NSString *utfEncode = [NSString stringWithFormat:@"%s",[[NSString stringWithFormat:@"5627a311562fa2cc915d34c3c3b162bc:%@",@"{\"after\":\"AQB19fD4Ey6wezBAFP7ohljQJeCKj76VyBAaaXWNO375cw3BNdBDJ9-BdRlP9VpNMUwKYNgxPab1diGiclln2aSdiut1sKsDHurk5QM41jzcVw\",\"first\":\"28\",\"id\":\"2008045917\"}"] UTF8String]];

        //    NSString *utfEncode = [NSString stringWithFormat:@"%s",[[NSString stringWithFormat:@"9da6fe7549224fd6c855bc2992ed64bb:9EnKiEsm8Yvdnm6gATNTxUBab1Sbr2IT:{\"id\":\"1497919134\",\"first\":12,\"after\":\"AQAoGWHLmdlDUIbqVLbFZ-qLs9OsRg5Yo7QZLhhrklMqcfOUIz1sHUxUVYprF8ngqO8QFSnU6rq7fZEExUQgWS_VRs_pKu6PmPrdg-RyO_fDew\"}"] UTF8String]];

        return self.md5HexDigest(input:utfEncode)
    }
    class func md5HexDigest(input: String?) -> String? {
        
        let str = input!.cString(using: String.Encoding.utf8)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: Int(CC_MD5_DIGEST_LENGTH))
        let strLen = CUnsignedInt(input!.lengthOfBytes(using: String.Encoding.utf8))
        CC_MD5(str, strLen, result)
        var ret = String(repeating: "\0", count: Int(CC_MD5_DIGEST_LENGTH) * 2)
        for i in 0..<CC_MD5_DIGEST_LENGTH {
            ret += String(format: "%02x", result[Int(i)])
        }
        return ret
    }

}
