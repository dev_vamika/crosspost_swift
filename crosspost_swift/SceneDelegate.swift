//
//  SceneDelegate.swift
//  crosspost_swift
//
//  Created by vamika on 05/08/20.
//  Copyright © 2020 vamika. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate,URLSessionDelegate {
    var isObserverCalled = false
    var window: UIWindow?
    var db = DBManager()
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        db = DBManager()
        db.openDb()
        db.createTable(WORSPACE_TABLE)
        db.createTable(DATA_TABLE)
        db = DBManager()
        let data = db.getDataForField(MIGRATION_TABLE, where: nil)
        let migrationData = db.getDataForField(DATA_TABLE, where: nil)
        if data?.count != 0 && migrationData?.count == 0{
            for i in 0...data!.count-1{
                let postDictCopy = data![i] as! NSMutableDictionary
                db.insertValues(DATA_TABLE, values: postDictCopy as? [AnyHashable : Any])
            }
        
        }
        
//        self.fetchDataInBackground()
        Global.newVideoLocation()
        GET_USER_MEDIA_URL_1 =  "https://www.instagram.com/graphql/query/?query_id="
        GET_USER_MEDIA_URL_2 =  "https://www.instagram.com/graphql/query/?query_hash="
        GET_USER_MEDIA_URL_3 =  "https://www.instagram.com/graphql/query/?query_id=%@&id=%@&first=%@&after=%@"
        queryHash = DEFAULT_QUERY_HASH;
        queryId = DEFAULT_QUERY_ID;
        guard let _ = (scene as? UIWindowScene) else { return }
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        print("Called as the scene is being released by the system.")
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        print("Called when the scene has moved from an inactive state to an active state.")
        self.fetchDataInBackground()
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        print(" Called when the scene will move from an active state to an inactive state.")
//        self.fetchDataInBackground()
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
//        self.fetchDataInBackground()
        print("Called as the scene transitions from the background to the foreground.")
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        print("Called as the scene transitions from the foreground to the background.")
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        if (URLContexts.first?.url) != nil {
            if let incomingURL = UserDefaults(suiteName: "group.ShareExtensionCrossPostSynch")?.value(forKey: "incomingURL") as? String {
                if incomingURL.contains("https://www.instagram.com/") {
                    fetchData(urlString: incomingURL)
                }else{
                    DispatchQueue.main.async(execute: {
                        let privateMediaAlert = UIAlertController(title: "whoops!", message: "It doesn't look like instagram link.", preferredStyle: .alert)
                        
                        privateMediaAlert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in
                            
                        }))
                        
                        UIApplication.shared.keyWindow?.rootViewController?.present(privateMediaAlert, animated: true)
                    })
                }
                
                
            }
            
        }
    }
//    func handleIncomingURL(_ url: URL) {
//        if let scheme = url.scheme,
//            scheme.caseInsensitiveCompare("ShareExtensionCrossPostSynch") == .orderedSame,
//            let page = url.host {
//
//            var parameters: [String: String] = [:]
//            URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
//                parameters[$0.name] = $0.value
//            }
//
//            print("redirect(to: \(page), with: \(parameters))")
//
//            for parameter in parameters where parameter.key.caseInsensitiveCompare("url") == .orderedSame {
//                UserDefaults().set(parameter.value, forKey: "incomingURL")
//            }
//        }
//    }
}

extension SceneDelegate{
    func fetchDataInBackground(){
        let pb = UIPasteboard.general
        let linkString = pb.string
//        print("vamika.....................\n")
//        print(linkString)
        guard (linkString != nil) else {return}
        if (linkString?.contains("http"))! || (linkString?.contains("https"))!{
            let url = NSURL(string: linkString!)
            if url == nil{
                return
            }
            if UIApplication.shared.canOpenURL(url! as URL) {
                if (linkString?.contains("https://www.instagram.com/"))! {
                    let linkId = self.getInstagramLinkId(urlString: linkString)
                    if linkId == "" {
                        return
                    }
                    var arr = NSMutableArray()
                    if let w_id = UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE_ID) as? String {
                        arr = db.getDataForField(DATA_TABLE, where: "\(LINK) = 'https://www.instagram.com/p/\(linkId!)' AND \(WORK_SPACE_NAME_ID) = '\(w_id)'")
                    }else{
                    arr = db.getDataForField(DATA_TABLE, where: "\(LINK) = 'https://www.instagram.com/p/\(linkId!)'")
                    }
                    if arr.count == 0 {
                        if linkId != ""{
                            self.getDataFromLinkId(linkId: linkId) { (responseDict) in
                                var dictionaryToSave: [AnyHashable : Any] = [:]
                                dictionaryToSave[LINK] = "https://www.instagram.com/p/\(linkId!)"
                                dictionaryToSave[LINK_ID] = linkId
                                dictionaryToSave[IS_POSTED] = NO_STRING
                                
                                if responseDict.count > 0 {
                                    let graphQldict = responseDict["graphql"]
                                    if graphQldict != nil {
                                        let shortCodeDict = (graphQldict as AnyObject).value(forKey:"shortcode_media") as? [AnyHashable : Any]
                                        if shortCodeDict != nil {
                                            dictionaryToSave[IS_CAROUSEL] = shortCodeDict?["edge_sidecar_to_children"] != nil ? YES_STRING : NO_STRING
                                            dictionaryToSave[IS_VIDEO] = (shortCodeDict?["is_video"] as? NSNumber)?.boolValue ?? false ? YES_STRING : NO_STRING
                                            if let aShortCodeDict = shortCodeDict?["display_url"] {
                                                dictionaryToSave[DISPLAY_URL] = "\(aShortCodeDict)"
                                            }
                                            if let dict = shortCodeDict!["owner"]{
                                                let values =  dict as AnyObject
                                                dictionaryToSave[USER_NAME] = values.value(forKey: "username")
                                                dictionaryToSave[PROFILE_URL] = values.value(forKey: "profile_pic_url")
                                                dictionaryToSave[FULL_NAME] = values.value(forKey: "full_name")
                                            }
                                            if let dict = shortCodeDict!["edge_media_to_caption"]{
                                                //                                                    var captionArr: [AnyHashable]? = nil
                                                let values =  dict as AnyObject
                                                if values["edges"] != nil{
                                                    let   captionArr = values["edges"] as? NSArray
                                                    if captionArr?.count != 0 {
                                                        let txt = ((captionArr?.value(forKey: "node") as AnyObject).value(forKey: "text") as! NSArray)[0] as! String
                                                        //                                                        if dicArr.count > 0 {
                                                        //                                                            let dic = dicArr.value(forKey: "text") as! NSArray
                                                        if txt == "" {dictionaryToSave[CAPTION] = ""}else
                                                        {
                                                            dictionaryToSave[CAPTION] = "\(txt)"
                                                            
                                                        }
                                                        
                                                    }
                                                    
                                                }
                                            }else{
                                                dictionaryToSave[CAPTION] = ""
                                            }
                                            if UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE) as? String != nil{
                                                dictionaryToSave[WORK_SPACE_NAME] = UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE) as? String
                                            }else{
                                                dictionaryToSave[WORK_SPACE_NAME] = ""
                                            }
                                            if UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE_ID) as? String != nil{
                                                dictionaryToSave[WORK_SPACE_NAME_ID] = UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE_ID) as? String
                                            }else{
                                                dictionaryToSave[WORK_SPACE_NAME_ID] = ""
                                            }
                                            self.db.insertValues(DATA_TABLE, values: dictionaryToSave)
                                            self.loadMediaData(shortCodeDict, linkId: linkId)
//                                            if !self.isObserverCalled {
//                                                self.isObserverCalled = false
                                            DispatchQueue.main.async(execute: {
                                                NotificationCenter.default.post(name: NSNotification.Name("ReloadData"), object: dictionaryToSave, userInfo: nil)
                                            })
                                                
//                                            }
                                        }
                                    }
                                }
                               
                            }
                        }
                    }
                }
            }
        }
    }
    func getInstagramLinkId(urlString : String?)-> String?{
        var idString = ""
        
        let tempString = urlString?.replacingOccurrences(of: "https://www.instagram.com/", with: "")
        
        let arr = tempString?.components(separatedBy: "/")
        
        if arr!.count > 1{
            idString = arr![1];
        }
        return idString;
    }
    func getDataFromLinkId(linkId: String? , successHandler: ((_ response: NSDictionary)->Void)?){
        
        var request = URLRequest(url: URL(string: "https://www.instagram.com/p/\(linkId!)?__a=1")!)
        request.cachePolicy = .useProtocolCachePolicy
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.timeoutInterval = 30
        request.httpMethod = "GET"
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil )
        session.dataTask(with: request, completionHandler: { dataRecieved, response, error in
            
            if error == nil {
                var postJsonRecieved: Any? = nil
                do {
                    postJsonRecieved = try JSONSerialization.jsonObject(with: dataRecieved!, options: .allowFragments)
                } catch {
                    let jsonString = String(data: dataRecieved!, encoding: .utf8)
                    
                    print("updateDeviceId string Recieved = \(jsonString ?? "")")
                    
                    if jsonString?.contains("Page Not Found") ?? false {
                        
                        DispatchQueue.main.async(execute: {
                            let privateMediaAlert = UIAlertController(title: "whoops!", message: "Crosspost can only get public media links, The Copied url is from a private account.", preferredStyle: .alert)
                            
                            privateMediaAlert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in
                                
                            }))
                            
                            UIApplication.shared.keyWindow?.rootViewController?.present(privateMediaAlert, animated: true)
                        })
                    }
                }
                
                DispatchQueue.main.async(execute: {
                    if postJsonRecieved != nil{
                    successHandler!(postJsonRecieved as! NSDictionary)
                    }
                })
            } else {
                DispatchQueue.main.async(execute: {
                })
            }
        }).resume()
    }
    func loadMediaData(_ shortCodeDict: [AnyHashable : Any]?, linkId: String?) {
        
        if shortCodeDict?["edge_sidecar_to_children"] == nil {
            var mediaDictToSave: [AnyHashable : Any] = [:]
            if (shortCodeDict?["is_video"] as? NSNumber)?.boolValue ?? false {
                mediaDictToSave[LINK_ID] = linkId ?? ""
                mediaDictToSave[MEDIA_ID] = Global.getValidString(string: (shortCodeDict!["id"] as? String)!)
                mediaDictToSave[MEDIA_URL] = Global.getValidString(string: (shortCodeDict!["video_url"] as? String)!)
                mediaDictToSave[IS_VIDEO] = YES_STRING
                mediaDictToSave[THUMBNAIL_URL] = Global.getValidString(string: (shortCodeDict!["display_url"] as? String)!)
            } else {
                mediaDictToSave[LINK_ID] = linkId ?? ""
                mediaDictToSave[MEDIA_ID] = Global.getValidString(string: (shortCodeDict!["id"] as? String)!)
                mediaDictToSave[MEDIA_URL] = Global.getValidString(string: (shortCodeDict!["display_url"] as? String)!)
                mediaDictToSave[IS_VIDEO] = NO_STRING
                mediaDictToSave[THUMBNAIL_URL] = Global.getValidString(string: (shortCodeDict!["display_url"] as? String)!)
            }
            self.db.insertValues(MEDIA_TABLE, values: mediaDictToSave)
        } else {
            var mediaArray = NSArray()
            
            
            if shortCodeDict!["edge_sidecar_to_children"] != nil{
                let values =  (shortCodeDict!["edge_sidecar_to_children"] as! NSDictionary).value(forKey: "edges")
                if values != nil{
                    mediaArray = values as! NSArray
                }
            }
            for mediaDict in mediaArray{
                var mediaDictToSave: [AnyHashable : Any] = [:]
                let values = mediaDict as? NSDictionary
                let mediaNode = values!["node"] as! [AnyHashable : Any]
                mediaDictToSave[LINK_ID] = linkId ?? ""
                if let node = mediaNode["id"] as? String{
                mediaDictToSave[MEDIA_ID] = node
                }else{mediaDictToSave[MEDIA_ID] = ""}
                if let value = mediaNode["is_video"] as? NSNumber{
                mediaDictToSave[IS_VIDEO] = value.boolValue ? YES_STRING : NO_STRING
                }else{
                mediaDictToSave[IS_VIDEO] = NO_STRING
                }
                
                let isVideo = mediaNode["is_video"] as? Bool
                if isVideo! {
                    mediaDictToSave[MEDIA_URL] = (mediaNode["video_url"] as? String)!
                } else {
                    mediaDictToSave[MEDIA_URL] = (mediaNode["display_url"] as? String)!
                }
                mediaDictToSave[THUMBNAIL_URL] = (mediaNode["display_url"] as? String)!
                
                self.db.insertValues(MEDIA_TABLE, values: mediaDictToSave)
                
            }
            
        }
        
    }
    
    
    func fetchData(urlString: String?){
        
        if (urlString?.contains("http"))!{
            let url = URL(string: urlString!)
            if url == nil{
                return
            }
            if UIApplication.shared.canOpenURL(url!) {
                if (urlString?.contains("https://www.instagram.com/"))! {
                    let linkId = self.getInstagramLinkId(urlString: urlString)
                    if linkId == "" {
                        return
                    }
                    let arr = db.getDataForField(DATA_TABLE, where: "\(LINK) = https://www.instagram.com/p/\(linkId!)")
                    if arr?.count == 0 {
                        if linkId != ""{
                            self.getDataFromLinkId(linkId: linkId) { (responseDict) in
                                var dictionaryToSave: [AnyHashable : Any] = [:]
                                dictionaryToSave[LINK] = "https://www.instagram.com/p/\(linkId!)"
                                dictionaryToSave[LINK_ID] = linkId
                                dictionaryToSave[IS_POSTED] = NO_STRING
                                
                                if responseDict.count > 0 {
                                    let graphQldict = responseDict["graphql"]
                                    if graphQldict != nil {
                                        let shortCodeDict = (graphQldict as AnyObject).value(forKey:"shortcode_media") as? [AnyHashable : Any]
                                        if shortCodeDict != nil {
                                            dictionaryToSave[IS_CAROUSEL] = shortCodeDict?["edge_sidecar_to_children"] != nil ? YES_STRING : NO_STRING
                                            dictionaryToSave[IS_VIDEO] = (shortCodeDict?["is_video"] as? NSNumber)?.boolValue ?? false ? YES_STRING : NO_STRING
                                            if let aShortCodeDict = shortCodeDict?["display_url"] {
                                                dictionaryToSave[DISPLAY_URL] = "\(aShortCodeDict)"
                                            }
                                            if let dict = shortCodeDict!["owner"]{
                                                let values =  dict as AnyObject
                                                dictionaryToSave[USER_NAME] = values.value(forKey: "username")
                                                dictionaryToSave[PROFILE_URL] = values.value(forKey: "profile_pic_url")
                                                dictionaryToSave[FULL_NAME] = values.value(forKey: "full_name")
                                            }
                                            if let dict = shortCodeDict!["edge_media_to_caption"]{
                                                let values =  dict as AnyObject
                                                if values["edges"] != nil{
                                                    let   captionArr = values["edges"] as? NSArray
                                                    if captionArr?.count != 0 {
                                                        let txt = ((captionArr?.value(forKey: "node") as AnyObject).value(forKey: "text") as! NSArray)[0] as! String
                                                        if txt == "" {dictionaryToSave[CAPTION] = ""}else
                                                        {
                                                            dictionaryToSave[CAPTION] = "\(txt)"
                                                        }
                                                    }
                                                }
                                            }else{
                                                dictionaryToSave[CAPTION] = ""
                                            }
                                            if UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE) as? String != nil{
                                                dictionaryToSave[WORK_SPACE_NAME] = UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE) as? String
                                            }else{
                                                dictionaryToSave[WORK_SPACE_NAME] = ""
                                            }
                                            if UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE_ID) as? String != nil{
                                                dictionaryToSave[WORK_SPACE_NAME_ID] = UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE_ID) as? String
                                            }else{
                                                dictionaryToSave[WORK_SPACE_NAME_ID] = ""
                                            }
                                            self.db.insertValues(DATA_TABLE, values: dictionaryToSave)
                                            self.loadMediaData(shortCodeDict, linkId: linkId)
                                            NotificationCenter.default.post(name: NSNotification.Name("ReloadData"), object: dictionaryToSave, userInfo: nil)
                                        }
                                    }
                                }
                            }
                        }else{
                            DispatchQueue.main.async(execute: {
                                NotificationCenter.default.post(name: NSNotification.Name("ReloadData"), object: nil, userInfo: nil)
                            })
                            print("Its not a valid instagram url")
                        }
                    }else{
                        DispatchQueue.main.async(execute: {
                            NotificationCenter.default.post(name: NSNotification.Name("ReloadData"), object: nil, userInfo: nil)
                        })
                    }
                }else{
                    DispatchQueue.main.async(execute: {
                        NotificationCenter.default.post(name: NSNotification.Name("ReloadData"), object: nil, userInfo: nil)
                    })
                    print("Its a non instagram url")
                }
            }else{
                DispatchQueue.main.async(execute: {
                    NotificationCenter.default.post(name: NSNotification.Name("ReloadData"), object: nil, userInfo: nil)
                })
                print("Its not a url")
            }
        }else{
            DispatchQueue.main.async(execute: {
                NotificationCenter.default.post(name: NSNotification.Name("ReloadData"), object: nil, userInfo: nil)
            })
            print("Its not a url")
        }
    }
    func forceFetchData(urlString: String?){
        
        if (urlString?.contains("http"))!{
            let url = URL(string: urlString!)
            if url == nil{
                return
            }
            if UIApplication.shared.canOpenURL(url!) {
                if (urlString?.contains("https://www.instagram.com/"))! {
                    let linkId = self.getInstagramLinkId(urlString: urlString)
                    if linkId == "" {
                        return
                    }
                    
                    db.deleteRow(DATA_TABLE, where: String(format:"%@ = '%@'",LINK_ID,(linkId!)))
                    db.deleteRow(MEDIA_TABLE, where: String(format:"%@ = '%@'",LINK_ID,(linkId!)))
                        if linkId != ""{
                            self.getDataFromLinkId(linkId: linkId) { (responseDict) in
                                var dictionaryToSave: [AnyHashable : Any] = [:]
                                dictionaryToSave[LINK] = "https://www.instagram.com/p/\(linkId!)"
                                dictionaryToSave[LINK_ID] = linkId
                                dictionaryToSave[IS_POSTED] = NO_STRING
                                
                                if responseDict.count > 0 {
                                    let graphQldict = responseDict["graphql"]
                                    if graphQldict != nil {
                                        let shortCodeDict = (graphQldict as AnyObject).value(forKey:"shortcode_media") as? [AnyHashable : Any]
                                        if shortCodeDict != nil {
                                            dictionaryToSave[IS_CAROUSEL] = shortCodeDict?["edge_sidecar_to_children"] != nil ? YES_STRING : NO_STRING
                                            dictionaryToSave[IS_VIDEO] = (shortCodeDict?["is_video"] as? NSNumber)?.boolValue ?? false ? YES_STRING : NO_STRING
                                            if let aShortCodeDict = shortCodeDict?["display_url"] {
                                                dictionaryToSave[DISPLAY_URL] = "\(aShortCodeDict)"
                                            }
                                            if let dict = shortCodeDict!["owner"]{
                                                let values =  dict as AnyObject
                                                dictionaryToSave[USER_NAME] = values.value(forKey: "username")
                                                dictionaryToSave[PROFILE_URL] = values.value(forKey: "profile_pic_url")
                                                dictionaryToSave[FULL_NAME] = values.value(forKey: "full_name")
                                            }
                                            if let dict = shortCodeDict!["edge_media_to_caption"]{
                                                let values =  dict as AnyObject
                                                if values["edges"] != nil{
                                                    let   captionArr = values["edges"] as? NSArray
                                                    if captionArr?.count != 0 {
                                                        let txt = ((captionArr?.value(forKey: "node") as AnyObject).value(forKey: "text") as! NSArray)[0] as! String
                                                        if txt == "" {dictionaryToSave[CAPTION] = ""}else
                                                        {
                                                            dictionaryToSave[CAPTION] = "\(txt)"
                                                        }
                                                    }
                                                }
                                            }else{
                                                dictionaryToSave[CAPTION] = ""
                                            }
                                            if UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE) as? String != nil{
                                                dictionaryToSave[WORK_SPACE_NAME] = UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE) as? String
                                            }else{
                                                dictionaryToSave[WORK_SPACE_NAME] = ""
                                            }
                                            if UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE_ID) as? String != nil{
                                                dictionaryToSave[WORK_SPACE_NAME_ID] = UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE_ID) as? String
                                            }else{
                                                dictionaryToSave[WORK_SPACE_NAME_ID] = ""
                                            }
                                            self.db.insertValues(DATA_TABLE, values: dictionaryToSave)
                                            self.loadMediaData(shortCodeDict, linkId: linkId)
                                            NotificationCenter.default.post(name: NSNotification.Name("RefreshPost"), object: dictionaryToSave, userInfo: nil)
                                        }
                                    }
                                }
                            }
                        }else{
                            DispatchQueue.main.async(execute: {
                                NotificationCenter.default.post(name: NSNotification.Name("ReloadData"), object: nil, userInfo: nil)
                            })
                            print("Its not a valid instagram url")
                        }
                    
                }else{
                    DispatchQueue.main.async(execute: {
                        NotificationCenter.default.post(name: NSNotification.Name("ReloadData"), object: nil, userInfo: nil)
                    })
                    print("Its a non instagram url")
                }
            }else{
                DispatchQueue.main.async(execute: {
                    NotificationCenter.default.post(name: NSNotification.Name("ReloadData"), object: nil, userInfo: nil)
                })
                print("Its not a url")
            }
        }else{
            DispatchQueue.main.async(execute: {
                NotificationCenter.default.post(name: NSNotification.Name("ReloadData"), object: nil, userInfo: nil)
            })
            print("Its not a url")
        }
    }


}
