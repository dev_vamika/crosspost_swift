//
//  PostRepostViewController.swift
//  crosspost_swift
//
//  Created by vamika on 12/11/20.
//  Copyright © 2020 vamika. All rights reserved.
//

import UIKit
import AVFoundation
protocol PostRepostCollectionCellDelegate: class {
    func onZoomClickWithIndex(indexPath: IndexPath)
}
class PostRepostViewController: UIViewController {
    weak var delegate: PostRepostCollectionCellDelegate?
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var zoomIcon: UIButton!
    @IBOutlet weak var playerView: UIView!
    var index : IndexPath!
    var mediaDict = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        Global.addShadowToView(view: zoomIcon)
        self.playerView.isHidden = true
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.backgroundColor = UIColor(named: "CELL_BG_COLOR")
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
       // if self.mediaDict[IS_VIDEO] as! String == YES_STRING{
            removewObserver()
       // }
    }
    
    func removewObserver() {
        if avPlayer?.currentItem != nil{
            self.finishPlaying(player: (avPlayer?.currentItem)!)
            NotificationCenter.default.removeObserver(self,name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,object: avPlayer?.currentItem)
        }
       
        
    }
    func addObserver() {
        NotificationCenter.default.addObserver(self,selector: #selector(itemDidFinishPlaying(notification:)),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,object: avPlayer?.currentItem)
    }
    func finishPlaying(player: AVPlayerItem){
        if previewImageView != nil {
            UIView.transition(with: previewImageView, duration: 0.4,
                              options: .transitionCrossDissolve,
                              animations: {
                                self.previewImageView.isHidden = false
                          })
        }
        
        
        player.seek(to: CMTime.zero, completionHandler: nil)
        avPlayer?.pause()
        videoLayer.removeFromSuperlayer()
    }
    
    @objc func itemDidFinishPlaying(notification: NSNotification) {
        UIView.transition(with: previewImageView, duration: 0.4,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.previewImageView.isHidden = false
                      })
        
        if let player = notification.object as? AVPlayerItem{
        player.seek(to: CMTime.zero, completionHandler: nil)
        }
        avPlayer?.pause()
        videoLayer.removeFromSuperlayer()
    }
    
    @IBAction func playVideo(_ sender: UIButton) {
        if videoLayer.player != nil {
            avPlayer?.seek(to: CMTime.zero)
            avPlayer?.pause()
            videoLayer.removeFromSuperlayer()
            videoLayer.player = nil
            UIView.transition(with: previewImageView, duration: 0.4,
                              options: .transitionCrossDissolve,
                              animations: {
                                self.previewImageView.isHidden = false
                          })
            return
        }
        videoLayer = AVPlayerLayer()
        videoLayer.player = nil
        let fileStr = (self.mediaDict[DISPLAY_MEDIA] as? String)!
        let fileURL =  NSURL(string: fileStr)
        avPlayer = AVPlayer(url: fileURL! as URL)
        avPlayer?.actionAtItemEnd = .none
        
        videoLayer = AVPlayerLayer(player: avPlayer)
        videoLayer.frame = self.playerView.frame
        videoLayer.videoGravity = .resizeAspect;
        self.view.layer.addSublayer(videoLayer)
        avPlayer?.play()
        UIView.transition(with: previewImageView, duration: 0.4,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.previewImageView.isHidden = true
                      })
        
    }
    func setImageOnMediaImageView(){
        self.previewImageView.image = self.mediaDict[DISPLAY_PRIVIEW] as? UIImage
    }
    func setValues(){
        if self.mediaDict[IS_VIDEO] as! String == YES_STRING{
            self.playButton.isHidden = false
        }else{
            self.playButton.isHidden = true
        }

        setImageOnMediaImageView()
    }
    @IBAction func onClickZoom(_ sender: Any) {
        removewObserver()
        videoLayer.player = nil
        setImageOnMediaImageView()
        self.delegate?.onZoomClickWithIndex(indexPath: self.index)
    }

   

}
