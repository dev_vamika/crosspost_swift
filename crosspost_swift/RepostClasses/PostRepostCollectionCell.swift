//
//  PostRepostCollectionCell.swift
//  crosspost_swift
//
//  Created by vamika on 04/09/20.
//  Copyright © 2020 vamika. All rights reserved.
//

import UIKit
import AVFoundation
//protocol PostRepostCollectionCellDelegate: class {
//    func onZoomClickWithIndex(indexPath: IndexPath)
//}
class PostRepostCollectionCell: UICollectionViewCell {
//    weak var delegate: PostRepostCollectionCellDelegate?
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var topSampleImage: UIImageView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var zoomIcon: UIButton!
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var zoomX: NSLayoutConstraint!
    @IBOutlet weak var zoomY: NSLayoutConstraint!
    var avPlayer: AVPlayer?
    var index : IndexPath!
//    var avPlayer = AVPlayer()
    var mediaDict = NSDictionary()
    var videoLayer = AVPlayerLayer()
    override func awakeFromNib() {
        super.awakeFromNib()
        Global.addShadowToView(view: zoomIcon)
        self.contentView.backgroundColor = UIColor(named: "CELL_BG_COLOR")
        self.playerView.isHidden = true
        self.topSampleImage.isHidden = true
        self.topSampleImage.image = Global.imageWithImage(image: UIImage(named: "copy_link.png")!, scaledToSize: CGSize(width: self.contentView.frame.width, height: self.contentView.frame.height*0.34))
        addObserver()
    }
    override func prepareForReuse() {
         super.prepareForReuse()
        
//         self.label.text = nil
    }
    func removewObserver() {
        if self.avPlayer?.currentItem != nil{
            self.finishPlaying(player: (self.avPlayer?.currentItem)!)
            NotificationCenter.default.removeObserver(self,name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,object: self.avPlayer?.currentItem)
        }
       
        
    }
    func addObserver() {
        NotificationCenter.default.addObserver(self,selector: #selector(itemDidFinishPlaying(notification:)),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,object: self.avPlayer?.currentItem)
    }
    func finishPlaying(player: AVPlayerItem){
        UIView.transition(with: previewImageView, duration: 0.4,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.previewImageView.isHidden = false
                      })
        
        player.seek(to: CMTime.zero, completionHandler: nil)
        self.avPlayer?.pause()
        videoLayer.removeFromSuperlayer()
        UserDefaults.standard.set(false, forKey: "IS_VIDEO_PLAYING")
    }
    
    @objc func itemDidFinishPlaying(notification: NSNotification) {
        UIView.transition(with: previewImageView, duration: 0.4,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.previewImageView.isHidden = false
                      })
        
        if let player = notification.object as? AVPlayerItem{
        player.seek(to: CMTime.zero, completionHandler: nil)
        }
        self.avPlayer?.pause()
        videoLayer.removeFromSuperlayer()
        UserDefaults.standard.set(false, forKey: "IS_VIDEO_PLAYING")
    }
    
    @IBAction func playVideo(_ sender: UIButton) {
        
        UIView.transition(with: previewImageView, duration: 0.4,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.previewImageView.isHidden = true
                      })
        if videoLayer.player != nil{
            self.avPlayer?.seek(to: CMTime.zero)
            self.avPlayer?.pause()
            videoLayer.removeFromSuperlayer()
            videoLayer.player = nil
            UIView.transition(with: previewImageView, duration: 0.4,
                              options: .transitionCrossDissolve,
                              animations: {
                                self.previewImageView.isHidden = false
                          })
            UserDefaults.standard.set(false, forKey: "IS_VIDEO_PLAYING")
        }else{
        let fileStr = (self.mediaDict[DISPLAY_MEDIA] as? String)!
        let fileURL =  NSURL(string: fileStr)
        self.avPlayer = AVPlayer(url: fileURL! as URL)
            self.avPlayer?.actionAtItemEnd = .none
        
        videoLayer = AVPlayerLayer(player: self.avPlayer)
        videoLayer.frame = self.playerView.frame
        videoLayer.videoGravity = .resizeAspect;
        self.contentView.layer.addSublayer(videoLayer)
            self.avPlayer?.play()
            UserDefaults.standard.set(true, forKey: "IS_VIDEO_PLAYING")
        }
    }
    func setImageOnMediaImageView(){
        self.previewImageView.image = self.mediaDict[DISPLAY_PRIVIEW] as? UIImage
        
//        if self.mediaDict[DISPLAY_PRIVIEW] as? UIImage != nil {
//        let imageRect = AVMakeRect(aspectRatio: previewImageView.image!.size, insideRect: previewImageView.bounds);
//        self.zoomY.constant =  5
//            self.zoomX.constant = (self.contentView.frame.width - (imageRect.origin.x + imageRect.width)) == 0.0 ? 10 :  (self.contentView.frame.width - (imageRect.origin.x + imageRect.width))
//        }
        
    }
    func setValues(){
        if self.mediaDict[IS_VIDEO] as! String == YES_STRING{
            self.playButton.isHidden = false
//            self.zoomIcon.isHidden = true
            addObserver()
        }else{
            removewObserver()
            self.playButton.isHidden = true
//            self.zoomIcon.isHidden = false
        }
        setImageOnMediaImageView()
    }
    @IBAction func onClickZoom(_ sender: Any) {
        removewObserver()
        videoLayer.player = nil
        setImageOnMediaImageView()
//        self.delegate?.onZoomClickWithIndex(indexPath: self.index)
    }
}
