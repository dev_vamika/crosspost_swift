//
//  RepostViewController.swift
//  crosspost_swift
//
//  Created by vamika on 06/08/20.
//  Copyright © 2020 vamika. All rights reserved.
//

import UIKit
import Photos


class RepostViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, PurchaseViewControllerDelegate, PostRepostCollectionCellDelegate, PurchaseHandlerDelegate, UIDocumentInteractionControllerDelegate {
    
    @IBOutlet weak var captionTextView: UITextView!
    @IBOutlet weak var captionSwitch: UISwitch!
    @IBOutlet weak var darkSwitch: UISwitch!
    @IBOutlet weak var mediaCollection: UICollectionView!
    @IBOutlet weak var frameBackView: UIView!
    @IBOutlet weak var darkModeBackView: UIView!
    @IBOutlet weak var CaptionBackView: UIView!
    @IBOutlet weak var captionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var captionView: UIView!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var bottomRightButton: UIButton!
    @IBOutlet weak var bottomLeftButton: UIButton!
    @IBOutlet weak var topLeftButton: UIButton!
    @IBOutlet weak var topRightButton: UIButton!
    @IBOutlet weak var noneButton: UIButton!
    @IBOutlet weak var userNameLbl: UIButton!
    @IBOutlet weak var shareIGButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var chooseFrameLbl: UILabel!
    @IBOutlet weak var darkFrameLbl: UILabel!
    @IBOutlet weak var captionLbl: UILabel!
    @IBOutlet weak var logoLbl: UILabel!
    
    var interactionController = UIDocumentInteractionController.init()
    
    
    var mediaArray = NSMutableArray()
    var tempMediaArray = NSMutableArray()
    var previewViewDictionary = NSMutableDictionary()
    var postDictionary = NSDictionary()
    var db = DBManager()
    var isExporting = false
    var purchaseViewController = PurchaseViewController()
    var purchaseSuccessViewController = PurchaseSuccessViewController()
    var avPlayer = AVPlayer()
    var videoLayer = AVPlayerLayer()
    var isNormalSharing = false
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,selector: #selector(refreshView),name: NSNotification.Name("RefreshPost"),object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(appEnterInForeground),name: UIApplication.willEnterForegroundNotification,object: nil)
        UIView.appearance().isExclusiveTouch = true
        if UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE) as? String == nil {
            self.userNameLbl.setTitle("Add Workspace", for: .normal)
        }else{
            self.userNameLbl.setTitle(UserDefaults.standard.value(forKey: CURRENT_WORK_SPACE) as? String, for: .normal)
        }
        
        captionSwitch.isSelected = true
        //        let nib = UINib(nibName: "PostRepostCollectionCell", bundle: nil)
        //        mediaCollection.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "PostRepostCollectionCell")
        //        mediaCollection.register(nib, forCellWithReuseIdentifier: "PostRepostCollectionCell")
        mediaCollection.delegate = self
        mediaCollection.dataSource = self
        
        previewViewDictionary = NSMutableDictionary()
        db = DBManager()
        
        mediaArray = db.getDataForField(MEDIA_TABLE, where:String(format:"%@ = '%@'",LINK_ID,self.postDictionary[LINK_ID] as! String))
        //        if mediaArray.count > 1  {
        //
        //            let sortedArray = (mediaArray as NSArray).sortedArray(using: [
        //                NSSortDescriptor(key: MEDIA_ID, ascending: true)
        //            ])
        //            mediaArray = NSMutableArray()
        //            mediaArray = NSMutableArray(array: sortedArray)
        ////            let ascendingArray = NSMutableArray(array: (mediaArray as NSArray).sortedArray(using: [
        ////                NSSortDescriptor(key: MEDIA_ID, ascending: true)]))
        ////            mediaArray = NSMutableArray()
        ////             mediaArray = ascendingArray
        //        }
        tempMediaArray = NSMutableArray()
        
        pageController.numberOfPages = mediaArray.count
        pageController.isHidden = !(mediaArray.count > 1)
        
        self.navigationItem.title = String(format:"@%@",self.postDictionary[USER_NAME] as! String);
        if self.postDictionary[CAPTION] != nil {
            self.captionTextView.text = String(format: "credits : @%@, \n\n%@ \n\nReposted with @crosspost.app", self.postDictionary["username"] as! CVarArg,self.postDictionary[CAPTION] as! CVarArg)
        }else{
            self.captionTextView.text = String(format: "credits : @%@,\n\nReposted with @crosspost.app", self.postDictionary["username"] as! CVarArg)
        }
        
        self.captionTextView.translatesAutoresizingMaskIntoConstraints = true
        self.captionTextView.sizeToFit()
        if self.captionTextView.text == "" {
            self.CaptionBackView.isHidden = true
        }
        else{
            self.CaptionBackView.isHidden = false
        }
        self.captionViewHeight.constant = self.captionTextView.contentSize.height + 70
        self.navigationController?.navigationBar.tintColor = Global.getTintColor()
        self.captionTextView.isEditable = false
        
        let q = DispatchQueue.global(qos: .default)
        q.async(execute: {
            
            for mediaDictInfo in self.mediaArray {
                let mediaDict = mediaDictInfo as! NSDictionary
                var tempDict: [AnyHashable : Any] = [:]
                self.db = DBManager()
                
                
                tempDict[MEDIA_ID] = mediaDict[MEDIA_ID]
                
                tempDict[DISPLAY_PRIVIEW] = self.getDisplayImage((mediaDict as! [AnyHashable : Any]))
                
                //                if mediaDict[IS_VIDEO] as! String == YES_STRING {
                //                    tempDict[DISPLAY_MEDIA] = self.getLocalVideoUrl((mediaDict as! [AnyHashable : Any]))
                //                } else {
                //                    tempDict[DISPLAY_MEDIA] = self.getDisplayImage((mediaDict as! [AnyHashable : Any]))
                //                }
                if mediaDict[IS_VIDEO] as! String == YES_STRING {
                    let data = self.getLocalVideoUrl((mediaDict as! [AnyHashable : Any]))
                    if data == "" {
                        DispatchQueue.main.async(execute: {
                            let stringToCopy = String(format:"https://www.instagram.com/p/%@",(mediaDict[LINK_ID])! as! CVarArg)
                            let app = SceneDelegate()
                            app.forceFetchData(urlString: stringToCopy)
                            Global.addWaitingView(waitingViewLabel: "Getting data...")
                        })
                        
                    }else{
                        tempDict[DISPLAY_MEDIA] = self.getLocalVideoUrl((mediaDict as! [AnyHashable : Any]))
                        tempDict[IS_VIDEO] = mediaDict[IS_VIDEO]
                        
                        self.tempMediaArray.add(tempDict)
                        DispatchQueue.main.async(execute: {
                            Global.removeWaitingView()
                            self.mediaCollection.reloadData()
                            self.onChooseFrameType(self.bottomRightButton)
                        })
                    }
                } else {
                    let data = self.getDisplayImage((mediaDict as! [AnyHashable : Any]))
                    if data == nil{
                        DispatchQueue.main.async(execute: {
                            let stringToCopy = String(format:"https://www.instagram.com/p/%@",(mediaDict[LINK_ID])! as! CVarArg)
                            let app = SceneDelegate()
                            app.forceFetchData(urlString: stringToCopy)
                            Global.addWaitingView(waitingViewLabel: "Getting data...")
                        })
                    }
                    else{
                        tempDict[DISPLAY_MEDIA] = self.getDisplayImage((mediaDict as! [AnyHashable : Any]))
                        tempDict[IS_VIDEO] = mediaDict[IS_VIDEO]
                        
                        self.tempMediaArray.add(tempDict)
                        DispatchQueue.main.async(execute: {
                            Global.removeWaitingView()
                            self.mediaCollection.reloadData()
                            self.onChooseFrameType(self.bottomRightButton)
                        })
                    }
                    
                }
            }
        })
        let date = UserDefaults.standard.value(forKey: MONTH_VALUE) as? Date
        if date == nil && !Global.isPaidUser() {
            UserDefaults.standard.setValue(Date(), forKey: MONTH_VALUE)
            UserDefaults.standard.set(0, forKey: POST_COUNT)
        } else if date != nil && numberOfMonthBetween(date: date, toDate: Date()) >= 1 {
            UserDefaults.standard.setValue(Date(), forKey: MONTH_VALUE)
            UserDefaults.standard.set(0, forKey: POST_COUNT)
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        Global.makeCornerRadius(view: darkModeBackView, radius: 5.0)
        Global.makeCornerRadius(view: mediaCollection, radius: 5.0)
        Global.makeCornerRadius(view: frameBackView, radius: 5.0)
        Global.makeCornerRadius(view: CaptionBackView, radius: 5.0)
        Global.makeCornerRadius(view: shareIGButton, radius: Float(shareIGButton.frame.height/2))
        Global.addShadowAtBottom(view: topView)
        Global.addBorderWithhRadius(view: captionView, radius: 1.5, borderWidth: 0.5, borderColor: UIColor(named: "APP_BLUE_COLOR")!)
        self.view.backgroundColor = UIColor(named: "APP_BG_COLOR")
        let color = UIColor(named: "APP_BLUE_COLOR")
        self.userNameLbl.setTitleColor(color, for: .normal)
        self.captionTextView.textColor = color
        self.captionLbl.textColor = color
        self.logoLbl.textColor = color
        self.darkFrameLbl.textColor = color
        self.chooseFrameLbl.textColor = color
        self.shareButton.setImage(UIImage(named: "share"), for: .normal)
        self.frameBackView.backgroundColor = UIColor(named: "CELL_BG_COLOR")
        self.darkModeBackView.backgroundColor = UIColor(named: "CELL_BG_COLOR")
        self.CaptionBackView.backgroundColor = UIColor(named: "CELL_BG_COLOR")
        self.backButton.setTitleColor(UIColor(named: "APP_BLUE_COLOR"), for: .normal)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.isUserInteractionEnabled = true
       
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if tempMediaArray.count == 0 {
            Global.addWaitingView(waitingViewLabel: "Please wait...")
        }
       
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.post(name: NSNotification.Name("refreshView"), object: [:], userInfo: nil)
    }
    @objc func appEnterInForeground() {
        reloadData()
     }
    func numberOfMonthBetween(date: Date?, toDate: Date?) -> Int {
        
        let month = Calendar.current.dateComponents([.month], from: date!, to: toDate!)
        if month.date != nil{
            return month.day!
        }
        return 0
    }
    func reloadData(){
        mediaArray = NSMutableArray()
        mediaArray = db.getDataForField(MEDIA_TABLE, where:String(format:"%@ = '%@'",LINK_ID,self.postDictionary[LINK_ID] as! String))
        tempMediaArray = NSMutableArray()
        
        pageController.numberOfPages = mediaArray.count
        pageController.isHidden = !(mediaArray.count > 1)
        Global.addWaitingView(waitingViewLabel: "Downloading...")
        let q = DispatchQueue.global(qos: .default)
        q.async(execute: {
            
            for mediaDictInfo in self.mediaArray {
                let mediaDict = mediaDictInfo as! NSDictionary
                var tempDict: [AnyHashable : Any] = [:]
                self.db = DBManager()
                
                
                tempDict[MEDIA_ID] = mediaDict[MEDIA_ID]
                
                tempDict[DISPLAY_PRIVIEW] = self.getDisplayImage((mediaDict as! [AnyHashable : Any]))
                
                if mediaDict[IS_VIDEO] as! String == YES_STRING {
                    tempDict[DISPLAY_MEDIA] = self.getLocalVideoUrl((mediaDict as! [AnyHashable : Any]))
                } else {
                    tempDict[DISPLAY_MEDIA] = self.getDisplayImage((mediaDict as! [AnyHashable : Any]))
                }
                
                tempDict[IS_VIDEO] = mediaDict[IS_VIDEO]
                
                self.tempMediaArray.add(tempDict)
                
            }
            
            DispatchQueue.main.async(execute: {
                self.mediaCollection.reloadData()
                Global.removeWaitingView()
                self.onChooseFrameType(self.bottomRightButton)
            })
        })
        
    }
    @objc func refreshView(){
        let db = DBManager()
        self.mediaArray = NSMutableArray()
        self.mediaArray = db.getDataForField(MEDIA_TABLE, where:String(format:"%@ = '%@'",LINK_ID,self.postDictionary[LINK_ID] as! String))
        tempMediaArray = NSMutableArray()
        pageController.numberOfPages = mediaArray.count
        pageController.isHidden = !(mediaArray.count > 1)
        Global.addWaitingView(waitingViewLabel: "Downloading...")
        let q = DispatchQueue.global(qos: .default)
        q.async(execute: {
            
            for mediaDictInfo in self.mediaArray {
                let mediaDict = mediaDictInfo as! NSDictionary
                var tempDict: [AnyHashable : Any] = [:]
                self.db = DBManager()
                
                
                tempDict[MEDIA_ID] = mediaDict[MEDIA_ID]
                
                tempDict[DISPLAY_PRIVIEW] = self.getDisplayImage((mediaDict as! [AnyHashable : Any]))
                
                if mediaDict[IS_VIDEO] as! String == YES_STRING {
                    tempDict[DISPLAY_MEDIA] = self.getLocalVideoUrl((mediaDict as! [AnyHashable : Any]))
                } else {
                    tempDict[DISPLAY_MEDIA] = self.getDisplayImage((mediaDict as! [AnyHashable : Any]))
                }
                
                tempDict[IS_VIDEO] = mediaDict[IS_VIDEO]
                
                self.tempMediaArray.add(tempDict)
                
            }
            
            DispatchQueue.main.async(execute: {
                
                self.mediaCollection.reloadData()
                Global.removeWaitingView()
                NotificationCenter.default.post(name: NSNotification.Name("refreshView"), object: [:], userInfo: nil)
//                self.onChooseFrameType(self.bottomRightButton)
            })
            
        })
        
    }
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickCaption(_ sender: UISwitch) {
        if captionSwitch.isSelected {
            captionSwitch.isSelected = false
            self.captionTextView.isHidden = true
            UIView.animate(withDuration: 0.5, animations: {
                self.captionViewHeight.constant = 51
                
                self.view.layoutIfNeeded()
            })
            
        }else{
            captionSwitch.isSelected = true
            self.captionTextView.isHidden = false
            self.captionTextView.translatesAutoresizingMaskIntoConstraints = true
            self.captionTextView.sizeToFit()
            UIView.animate(withDuration: 0.5, animations: {
                self.captionViewHeight.constant = self.captionTextView.contentSize.height + 70
                self.view.layoutIfNeeded()
            })
            
        }
    }
    func onZoomClickWithIndex(indexPath: IndexPath) {
//        let cell: PostRepostCollectionCell = self.mediaCollection.cellForItem(at: indexPath) as! PostRepostCollectionCell
        self.performSegue(withIdentifier: "zooming", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "zooming" {
            if let nextViewController = segue.destination as? ZoomPreviewViewController {
                nextViewController.mediaArr = self.tempMediaArray
            }
            
        }
    }
    @IBAction func oCLickUserNameButton(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: ViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                NotificationCenter.default.post(name: NSNotification.Name("Add_Worspace"), object: [:], userInfo: nil)
                break
            }
        }
        
    }
    @IBAction func onClickDarkFrame(_ sender: UISwitch) {
        NotificationCenter.default.post(name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: [:], userInfo: nil)
        if darkSwitch.isSelected {
            darkSwitch.isSelected = false
        }else{
            darkSwitch.isSelected = true
        }
        self.setAttributeOnMedia()
    }
    
    @IBAction func onClickSettings(_ sender: Any?) {
        isNormalSharing = true
        let postCount = UserDefaults.standard.integer(forKey: POST_COUNT)
        
        if postCount >= ALLOWED_POST {
            initialisePurchaseViewController()
        } else {
            if isExporting {
                let alertView = UIAlertController(title: "Please wait...", message: nil, preferredStyle: .alert)
                
                let okBtn = UIAlertAction(title: "Ok", style: .default, handler: { action in
                    
                })
                
                alertView.addAction(okBtn)
                
                present(alertView, animated: true)
                
                return
            }
            
            if self.captionSwitch.isOn {
                let pb = UIPasteboard.general
                
                let stringToCopy = getCaptionToCopy()
                
                pb.string = stringToCopy
            }
            
            if tempMediaArray.count > 1 {
                let actionSheet = UIAlertController(title: "Select Media", message: "This post contains multiple media - do you want to share them all or only the currently selected media?", preferredStyle: isiPad ? .alert : .actionSheet)
                
                actionSheet.addAction(UIAlertAction(title: "All Media", style: .default, handler: { [self] action in
                    dismiss(animated: true) { [self] in
                        
                        postImage(mediaArray: tempMediaArray, isFromShare: sender == nil)
                    }
                }))
                
                actionSheet.addAction(UIAlertAction(title: "Current Media", style: .default, handler: { [self] action in
                    dismiss(animated: true) { [self] in
                        let currentPage = self.pageController.currentPage
                        if currentPage >= 0 {
                            postImage(mediaArray: [tempMediaArray[currentPage]] , isFromShare: sender == nil)
                        }
                    }
                }))
                
                actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { [self] action in
                    dismiss(animated: true) {
                        Global.removeWaitingView()
                    }
                }))
                present(actionSheet, animated: true)
            } else {
                
                Global.addWaitingView(waitingViewLabel: "Exporting...")
                
                postImage(mediaArray: tempMediaArray , isFromShare: sender == nil)
            }
        }
        
    }
    func updateDatabase(){
        if !isNormalSharing {
        let postDictCopy = self.postDictionary.mutableCopy() as! NSMutableDictionary
        postDictCopy[IS_POSTED] = YES_STRING;
        db = DBManager()
        db.insertValues(DATA_TABLE, values: postDictCopy as? [AnyHashable : Any])
        if !(Global.isPaidUser()) {
            let postCount  = UserDefaults.standard.integer(forKey:POST_COUNT)
            UserDefaults.standard.set(postCount+1, forKey: POST_COUNT)
            UserDefaults.standard.synchronize()
        }
        }
        NotificationCenter.default.post(name: NSNotification.Name("refreshView"), object: [:], userInfo: nil)
            
    }
    func getCaptionToCopy() -> String{
        if self.postDictionary[CAPTION] != nil {
           
            
            let captionToCopy = String(format: "credits : @%@, \n\n%@ \n\nReposted with @crosspost.app",self.postDictionary["username"] as! CVarArg, (self.postDictionary[CAPTION] as! String).replacingOccurrences(of: "\n", with: "\u{2800}\n"))
            return captionToCopy
        }else{
            let captionToCopy = String(format: "credits : @%@,\n\nReposted with @crosspost.app", self.postDictionary["username"] as! CVarArg)
            return captionToCopy
        }
       
    }
    
    @IBAction func onClickShare(_ sender: UIButton) {
        
        let mediaDict = self.tempMediaArray.object(at: 0) as! NSDictionary
        
        let string = (mediaDict as AnyObject).value(forKey: IS_VIDEO) as! String as String
        
        if(self.tempMediaArray.count == 1 && string != YES_STRING){
            
            if self.captionSwitch.isOn {
                let pb = UIPasteboard.general
                
                let stringToCopy = getCaptionToCopy()
                
                pb.string = stringToCopy
            }
            
            var image: UIImage? = nil
            if let aMedia = mediaDict[DISPLAY_MEDIA] {
                image = aMedia as? UIImage
            }
            
            // get the documents directory url
            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            // choose a name for your image
            let fileName = "image.igo"
            // create the destination file url to save your image
            let fileURL = documentsDirectory.appendingPathComponent(fileName)
            // get your UIImage jpeg data representation and check if the destination file url already exists
            if let data = image?.jpegData(compressionQuality:  1.0),
              !FileManager.default.fileExists(atPath: fileURL.path) {
                do {
                    // writes the image data to disk
                    try data.write(to: fileURL)
                    print("file saved")
                } catch {
                    print("error saving file:", error)
                }
            }
            
            interactionController = UIDocumentInteractionController.init(url: fileURL)
            
            interactionController.uti = "com.instagram.photo"
        
            interactionController.delegate = self
            
            interactionController.presentPreview(animated: true)
            
//            self.updateDatabase()
            
        }
        else{
            onClickSettings(nil)
        }
        
//        isNormalSharing = false
//        let postCount =  UserDefaults.standard.integer(forKey: POST_COUNT)
//        if postCount >=  ALLOWED_POST {
//           initialisePurchaseViewController()
//        }
//        else{
//            if(isExporting){
//                let alertView = UIAlertController(title: "Please wait...", message: nil, preferredStyle: .alert)
//
//                alertView.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in
//
//                }))
//
//                self.present(alertView, animated: true)
//                return
//            }
//
//            if(self.captionSwitch.isOn){
//                let pb = UIPasteboard.general
//                let stringToCopy = getCaptionToCopy()
//                pb.string = stringToCopy
//            }
//
//            PHPhotoLibrary.requestAuthorization({ status in
//                if status == .notDetermined {
//                    DispatchQueue.main.async(execute: {
//                        PHPhotoLibrary.requestAuthorization({ status in
//                            if status == .authorized {
//                                DispatchQueue.main.async(execute: {
//                                    self.initialisePostingtoInstagram(localMediaArray: self.tempMediaArray)
//                                })
//                            } else {
//                                DispatchQueue.main.async(execute: {
//                                    Global.showPermissionDeniedErrorMsg()
//                                })
//                            }
//                        })
//                    })
//                } else if status == .restricted {
//                    DispatchQueue.main.async(execute: {
//                        Global.showPermissionDeniedErrorMsg()
//                    })
//                } else if status == .denied {
//                    DispatchQueue.main.async(execute: {
//                        Global.showPermissionDeniedErrorMsg()
//                    })
//                } else if status == .authorized {
//                    DispatchQueue.main.async(execute: {
//                        self.initialisePostingtoInstagram(localMediaArray: self.tempMediaArray )
//                    })
//                }
//            });
//        }
    }
    
    public func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }

    public func documentInteractionControllerDidEndPreview(_ controller: UIDocumentInteractionController) {
//        controller = nil
    }
    
    func initialisePostingtoInstagram(localMediaArray: NSMutableArray) {
        if localMediaArray.count > 1 {
            
            let actionSheet = UIAlertController(title: "Select Media", message: "This post contains multiple media - do you want to share them all or only the currently selected media?", preferredStyle: isiPad ? .alert : .actionSheet)
            
            actionSheet.addAction(UIAlertAction(title: "All Media", style: .default, handler: { action in
                self.dismiss(animated: true) {
                    
                    Global.addWaitingView(waitingViewLabel:"Exporting...")
                    
                    self.initialisePosting(mediaToPostArray: localMediaArray)
                }
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Current Media", style: .default, handler: { action in
                self.dismiss(animated: true) {
                    
                    Global.addWaitingView(waitingViewLabel: "Exporting...")
                    
                    let currentPage = self.pageController.currentPage
                    if currentPage >= 0{
                        self.initialisePosting(mediaToPostArray: [localMediaArray[currentPage]])
                    }
                }
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                self.dismiss(animated: true) {
                    
                }
            }))
            present(actionSheet, animated: true)
        } else {
            
            Global.addWaitingView(waitingViewLabel:"Exporting...")
            
            initialisePosting(mediaToPostArray: localMediaArray)
        }
    }
    func initialisePosting(mediaToPostArray: NSMutableArray) {
        let mediaDict = mediaToPostArray.lastObject as? NSDictionary
        let string = mediaDict?.value(forKey: IS_VIDEO) as! String as String
        if string == YES_STRING {
            var pathOfFile: String? = nil
            if let aMediaDict = mediaDict?[DISPLAY_MEDIA] {
                pathOfFile = "\(aMediaDict)"
            }
            
            var localId: String?
            
            PHPhotoLibrary.shared().performChanges({
                var assetChangeRequest: PHAssetChangeRequest? = nil
                if let url = URL(string: pathOfFile ?? "") {
                    assetChangeRequest = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
                }
                
                let assetCollection = PHAssetCollection()
                
                if assetCollection.estimatedAssetCount > 0 {
                    let assetCollectionChangeRequest = PHAssetCollectionChangeRequest(for: assetCollection)
                    if assetChangeRequest?.placeholderForCreatedAsset != nil {
                        assetCollectionChangeRequest?.addAssets([assetChangeRequest?.placeholderForCreatedAsset] as NSFastEnumeration)
                    }
                }
                localId = assetChangeRequest?.placeholderForCreatedAsset?.localIdentifier
            }, completionHandler: { success, error in
                if !success {
                    if let error = error {
                        print("Error creating asset: \(error)")
                    }
                } else {
                    
                    mediaToPostArray.removeLastObject()
                    
                    if mediaToPostArray.count > 0 {
                        self.initialisePosting(mediaToPostArray: mediaToPostArray)
                    } else {
                        let assetResult = PHAsset.fetchAssets(withLocalIdentifiers: [localId].compactMap { $0 }, options: nil)
                        let asset = assetResult.firstObject
                        DispatchQueue.main.async(execute: {
                            self.openInstagram(locationId: asset?.localIdentifier)
                        })
                    }
                }
            })
        }else{
            let photo = mediaDict![DISPLAY_PRIVIEW] as? UIImage
            
            var localId: String?
            
            PHPhotoLibrary.shared().performChanges({
                var assetChangeRequest: PHAssetChangeRequest? = nil
                if let photo = photo {
                    assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: photo)
                }
                
                let assetCollection = PHAssetCollection()
                
                if assetCollection.estimatedAssetCount > 0 {
                    let assetCollectionChangeRequest = PHAssetCollectionChangeRequest(for: assetCollection)
                    if assetChangeRequest?.placeholderForCreatedAsset != nil {
                        assetCollectionChangeRequest?.addAssets([assetChangeRequest?.placeholderForCreatedAsset] as NSFastEnumeration)
                    }
                }
                localId = assetChangeRequest?.placeholderForCreatedAsset?.localIdentifier
            }, completionHandler: { success, error in
                mediaToPostArray.removeLastObject()
                
                if mediaToPostArray.count > 0 {
                    self.initialisePosting(mediaToPostArray: mediaToPostArray)
                } else {
                    let assetResult = PHAsset.fetchAssets(withLocalIdentifiers: [localId].compactMap { $0 }, options: nil)
                    let asset = assetResult.firstObject
                    DispatchQueue.main.async(execute: {
                        self.openInstagram(locationId: asset?.localIdentifier)
                    })
                }
            })
        }
        
    }
    func postImage(mediaArray:NSMutableArray, isFromShare:Bool) {
        if mediaArray.count == 1 {
            let mediaDict = mediaArray[0] as? NSDictionary
            let string = mediaDict?.value(forKey: IS_VIDEO) as! String as String
            if string == YES_STRING {
                
                var videoUrl: URL? = nil
                if let aMediaDict = mediaDict?[DISPLAY_MEDIA] {
                    videoUrl = URL(string: "\(aMediaDict)")
                }
                
                let activityItems = [videoUrl]
                
                let activityViewController = UIActivityViewController(activityItems: activityItems.compactMap { $0 }, applicationActivities: nil)
                
                if(isFromShare){
                    activityViewController.excludedActivityTypes =  [
                        UIActivity.ActivityType.assignToContact,
                        UIActivity.ActivityType.print,
                        UIActivity.ActivityType.addToReadingList,
                        UIActivity.ActivityType.copyToPasteboard,
                        UIActivity.ActivityType.markupAsPDF,
                        UIActivity.ActivityType.openInIBooks,
                        UIActivity.ActivityType(rawValue: "com.apple.reminders.RemindersEditorExtension"),
                        UIActivity.ActivityType(rawValue: "com.apple.mobilenotes.SharingExtension")]
                }
                
                if activityViewController.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    activityViewController.popoverPresentationController?.sourceRect = self.shareButton.frame
                }
                
                present(activityViewController, animated: true) {
                    
                    self.updateDatabase()
                    
                    Global.removeWaitingView()
                }
            } else {
                let photo = mediaDict?[DISPLAY_PRIVIEW] as? UIImage
                
                let activityItems = [photo]
                
                let activityViewController = UIActivityViewController(activityItems: activityItems.compactMap { $0 }, applicationActivities: nil)
                
                if(isFromShare){
                    activityViewController.excludedActivityTypes =  [
                        UIActivity.ActivityType.assignToContact,
                        UIActivity.ActivityType.print,
                        UIActivity.ActivityType.addToReadingList,
                        UIActivity.ActivityType.copyToPasteboard,
                        UIActivity.ActivityType.markupAsPDF,
                        UIActivity.ActivityType.openInIBooks,
                        UIActivity.ActivityType(rawValue: "com.apple.reminders.RemindersEditorExtension"),
                        UIActivity.ActivityType(rawValue: "com.apple.mobilenotes.SharingExtension")]
                }
                
                if activityViewController.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    activityViewController.popoverPresentationController?.sourceRect = self.shareButton.frame
                }
                
                present(activityViewController, animated: true) {
                    
                    self.updateDatabase()
                    
                    Global.removeWaitingView()
                }
            }
        }
        else
        {
            Global.addWaitingView(waitingViewLabel:"Exporting...")
            
            var activityItems: [AnyHashable] = []
            
            for mediaDict in mediaArray {
                guard let mediaDict = mediaDict as? NSDictionary else {
                    continue
                }
                let string = mediaDict.value(forKey: IS_VIDEO) as! String as String
                if string == YES_STRING {
                    
                    let videoUrl = URL(fileURLWithPath: "\((Global.newVideoLocation()))/\((mediaDict[MEDIA_ID])!).mp4")
                    
                    activityItems.append(videoUrl)
                } else {
                    let photo = mediaDict[DISPLAY_PRIVIEW] as? UIImage
                    
                    if photo != nil {
                        if let photo = photo {
                            activityItems.append(photo)
                        }
                    } else {
                        var photo: UIImage? = nil
                        if let url = URL(string: mediaDict[MEDIA_URL] as? String ?? ""), let data = NSData(contentsOf: url) {
                            photo = UIImage(data: data as Data)
                        }
                        if let photo = photo {
                            activityItems.append(photo)
                        }
                    }
                }
            }
            
            let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
            
            if activityViewController.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
                activityViewController.popoverPresentationController?.sourceView = self.view
                activityViewController.popoverPresentationController?.sourceRect = self.shareButton.frame
            }
            
            if(isFromShare){
                activityViewController.excludedActivityTypes =  [
                    UIActivity.ActivityType.assignToContact,
                    UIActivity.ActivityType.print,
                    UIActivity.ActivityType.addToReadingList,
                    UIActivity.ActivityType.copyToPasteboard,
                    UIActivity.ActivityType.markupAsPDF,
                    UIActivity.ActivityType.openInIBooks,
                    UIActivity.ActivityType(rawValue: "com.apple.reminders.RemindersEditorExtension"),
                    UIActivity.ActivityType(rawValue: "com.apple.mobilenotes.SharingExtension")]
            }
            
            present(activityViewController, animated: true) {
                
                self.updateDatabase()
                
                Global.removeWaitingView()
            }
        }
    }
    func openInstagram(locationId: String?) {
        

        Global.removeWaitingView()
        
        if let url = URL(string: "instagram://library?LocalIdentifier=\(locationId ?? "")") {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                updateDatabase()
            } else {
                let alertView = UIAlertController(title: "eep!", message: "We couldn’t locate Instagram on your device, Please download it here", preferredStyle: .alert)
                
                let okBtn = UIAlertAction(title: "Download", style: .default, handler: { action in
                    if let url = URL(string: "https://itunes.apple.com/in/app/instagram/id389801252") {
                        if UIApplication.shared.canOpenURL(url) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        }
                    }
                })
                
                alertView.addAction(okBtn)
                
                present(alertView, animated: true)
            }
        }
    }
    @IBAction func onChooseFrameType(_ sender: UIButton) {
        
        switch sender.tag {
        case 0:
            self.deselecteAllBtn()
            self.bottomRightButton.isSelected = true
            self.setAttributeOnMedia()
            break
        case 1:
            self.deselecteAllBtn()
            self.bottomLeftButton.isSelected = true
            self.setAttributeOnMedia()
            break
        case 2:
            self.deselecteAllBtn()
            self.topLeftButton.isSelected = true
            self.setAttributeOnMedia()
            break
        case 3:
            self.deselecteAllBtn()
            self.topRightButton.isSelected = true
            self.setAttributeOnMedia()
            break
        case 4:
            
            if Global.isPaidUser(){
                self.deselecteAllBtn()
                self.noneButton.isSelected = true
                self.setAttributeOnMedia()
            }
            else{
                self.initialisePurchaseViewController()
            }
            break
        default:
            break
        }
    }
    func initialisePurchaseViewController(){
        
        self.view.endEditing(true)
        if (purchaseViewController.isKind(of: PurchaseViewController.self)){
            purchaseViewController.view.removeFromSuperview()
            purchaseViewController = PurchaseViewController()
            
        }
        purchaseViewController = PurchaseViewController.init(nibName: "PurchaseViewController", bundle: nil)
        purchaseViewController.delegate = self
        purchaseViewController.view.frame = UIScreen.main.bounds
        let currentWindow = UIApplication.shared.keyWindow
        purchaseViewController.crossBtn.addTarget(self, action: #selector(onNoThanksBtn(_:)), for: .touchUpInside)
        currentWindow?.addSubview(purchaseViewController.view)
        purchaseViewController.view.alpha = 0.0;
        UIView.animate(withDuration: 0.35, animations: {
            self.purchaseViewController.view.alpha = 1.0
        }) { (finished) in
            self.purchaseViewController.view.alpha = 1.0
        }
    }
    func buy(_ product: SKProduct?) {
        PurchaseHandler.sharedInstance().delegate = self
        PurchaseHandler.sharedInstance()?.buyProduct(product?.productIdentifier)
        setUserInterationDisabled()
    }
    
    func restoreTransaction() {
        PurchaseHandler.sharedInstance().delegate = self
        PurchaseHandler.sharedInstance().restorePurchases()
        setUserInterationDisabled()
    }
    
    func setUserInterationDisabled() {
        purchaseViewController.activityIndicator.isHidden = false
        purchaseViewController.activityIndicator.startAnimating()
        purchaseViewController.startFreeWeekBtn.isUserInteractionEnabled = false
        purchaseViewController.restoreBtn.isUserInteractionEnabled = false
        purchaseViewController.crossBtn.isUserInteractionEnabled = false
    }
    func transactionFailed() {
        purchaseViewController.activityIndicator.isHidden = true
        purchaseViewController.activityIndicator.stopAnimating()
        purchaseViewController.startFreeWeekBtn.isUserInteractionEnabled = true
        purchaseViewController.restoreBtn.isUserInteractionEnabled = true
        purchaseViewController.crossBtn.isUserInteractionEnabled = true
        Global.removeWaitingView()
    }
    
    func transactionSuccessful(_ transaction: SKPaymentTransaction?) {
        purchaseViewController.activityIndicator.isHidden = true
        purchaseViewController.activityIndicator.stopAnimating()
        purchaseViewController.startFreeWeekBtn.isUserInteractionEnabled = true
        purchaseViewController.restoreBtn.isUserInteractionEnabled = true
        purchaseViewController.crossBtn.isUserInteractionEnabled = true
        onNoThanksBtn(nil)
        initialisePurchaseSuccessViewController()
    }
    @objc func onNoThanksBtn(_ sender: Any?) {
        if purchaseViewController.view != nil {
            UIView.animate(withDuration: 0.35, animations: { [self] in
                purchaseViewController.view.alpha = 0.0
            }) { [self] finished in
                purchaseViewController.view.alpha = 0.0
                purchaseViewController.view.removeFromSuperview()
                purchaseViewController = PurchaseViewController()
//                UserDefaults.standard.set(1, forKey: APP_SESSION_COUNT)
//                UserDefaults.standard.synchronize()
            }
        }
    }
    func transactionRestored() {
        
        Global.removeWaitingView()
        
        if Global.isPaidUser() {
            
            let controller = UIAlertController(
                title: "Success",
                message: "Restored Successfully!!",
                preferredStyle: .alert)
            
            controller.addAction(UIAlertAction(title: "OK", style: .default, handler: { [self] action in
                if purchaseViewController.view != nil {
                    purchaseViewController.activityIndicator.isHidden = true
                    purchaseViewController.activityIndicator.stopAnimating()
                    purchaseViewController.startFreeWeekBtn.isUserInteractionEnabled = true
                    purchaseViewController.restoreBtn.isUserInteractionEnabled = true
                    purchaseViewController.crossBtn.isUserInteractionEnabled = true
                    onNoThanksBtn(nil)
                    initialisePurchaseSuccessViewController()
                }
            }))
            
            UIApplication.shared.keyWindow?.rootViewController?.present(controller, animated: true)
        } else {
            let controller = UIAlertController(
                title: "Woops!!",
                message: "No Active subscription found.",
                preferredStyle: .alert)
            
            controller.addAction(UIAlertAction(title: "OK", style: .default, handler: { [self] action in
                if  purchaseViewController.view != nil {
                    purchaseViewController.activityIndicator.isHidden = true
                    purchaseViewController.activityIndicator.stopAnimating()
                    purchaseViewController.startFreeWeekBtn.isUserInteractionEnabled = true
                    purchaseViewController.restoreBtn.isUserInteractionEnabled = true
                    purchaseViewController.crossBtn.isUserInteractionEnabled = true
                }
            }))
            
            UIApplication.shared.keyWindow?.rootViewController?.present(controller, animated: true)
        }
    }
    func initialisePurchaseSuccessViewController(){
        if (purchaseSuccessViewController.isKind(of: PurchaseSuccessViewController.self)){
            purchaseSuccessViewController.view.removeFromSuperview()
            purchaseSuccessViewController = PurchaseSuccessViewController()
            
        }
        purchaseSuccessViewController = PurchaseSuccessViewController.init(nibName: "PurchaseSuccessViewController", bundle: nil)
        purchaseSuccessViewController.view.frame = UIScreen.main.bounds
        let currentWindow = UIApplication.shared.keyWindow
        purchaseSuccessViewController.letsGoBtn!.addTarget(self, action: #selector(onLetsGoBtn(_:)), for: .touchUpInside)
        currentWindow?.addSubview(purchaseSuccessViewController.view)
        purchaseSuccessViewController.view.alpha = 0.0;
        UIView.animate(withDuration: 0.35, animations: {
            self.purchaseSuccessViewController.view.alpha = 1.0
        }) { (finished) in
            self.purchaseSuccessViewController.view.alpha = 1.0
        }
    }
    @objc func onLetsGoBtn(_ sender: Any) {
        if purchaseSuccessViewController.view != nil {
            UIView.animate(withDuration: 0.35, animations: { [self] in
                purchaseSuccessViewController.view.alpha = 0.0
            }) { [self] finished in
                purchaseSuccessViewController.view.alpha = 0.0
                purchaseSuccessViewController.view.removeFromSuperview()
                purchaseSuccessViewController = PurchaseSuccessViewController()
            }
        }
    }
    func deselecteAllBtn(){
        self.bottomRightButton.isSelected = false
        self.bottomLeftButton.isSelected = false
        self.topRightButton.isSelected = false
        self.topLeftButton.isSelected = false
        self.noneButton.isSelected = false
    }
    func setAttributeOnMedia(){
        NotificationCenter.default.post(name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: [:], userInfo: nil)
        
        
        let tempArrayCopy = NSMutableArray(array: tempMediaArray)
        Global.addWaitingView(waitingViewLabel:"Applying Attributes..." )
        self.addAttribute(tempArrayCopy: tempArrayCopy, index : tempArrayCopy.count-1)
    }
    func makeOverLayView(_ imageSize: CGSize) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: imageSize.height * 0.08))
        
        view.backgroundColor = !(darkSwitch.isSelected) ? UIColor.white : UIColor.black
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        label.text = "@\((postDictionary[USER_NAME])!)"
        
        label.font = UIFont(name: "Karla", size: view.frame.size.height * 0.4)
        
        label.sizeToFit()
        
        label.textColor = !(darkSwitch.isSelected) ? UIColor.black : UIColor.white
        
        let appImageView = UIImageView(image: UIImage(named: "app_icon@2x.png"))
        
        appImageView.frame = CGRect(x: 10, y: 0, width: view.frame.size.height * 0.8, height: view.frame.size.height * 0.8)
        
        view.addSubview(appImageView)
        
        var userImage = UIImage(contentsOfFile: "\((Global.userImageLocation())!)/\((postDictionary[USER_NAME])!).png")
        
        if userImage == nil {
            if let url = URL(string: postDictionary[PROFILE_URL] as? String ?? ""), let data = NSData(contentsOf: url) {
                userImage = UIImage(data: data as Data)
            }
            let str = "\((Global.userImageLocation())!)/\((postDictionary[USER_NAME])!).png"
            NSData(data: (userImage?.pngData())!).write(toFile: str, atomically: true)
            
        }
        
        let userImageView = UIImageView(image: userImage)
        
        userImageView.frame = CGRect(x: appImageView.frame.origin.x + appImageView.frame.size.width + 20, y: 0, width: view.frame.size.height * 0.8, height: view.frame.size.height * 0.8)
        Global.makeCornerRadius(view: userImageView ,radius: Float(userImageView.frame.size.height/2))
        
        view.addSubview(userImageView)
        
        view.frame = CGRect(x: 0, y: 0, width: userImageView.frame.size.width + label.frame.size.width + appImageView.frame.size.width + 65, height: view.frame.size.height)
        
        label.frame = CGRect(x: userImageView.frame.origin.x + userImageView.frame.size.width + 20, y: 5, width: label.frame.size.width, height: label.frame.size.height)
        userImageView.center = CGPoint(x: userImageView.center.x, y: view.center.y)
        
        appImageView.center = CGPoint(x: appImageView.center.x, y: view.center.y)
        
        label.center = CGPoint(x: label.center.x, y: view.center.y)
        
        view.addSubview(label)
        
        return view
    }
    func addAttribute(tempArrayCopy: NSMutableArray, index: Int) {
        
        let localIndex = index
        
        isExporting = true
        
        if tempArrayCopy.count == 0 {
            
            mediaCollection.reloadData()
            
            isExporting = false
            
            Global.removeWaitingView()
            
            return
        }
        
        let mediaDict = tempArrayCopy.lastObject as? NSDictionary
        let tempDict = NSMutableDictionary(dictionary: mediaDict!)
        
        let mainImage = getDisplayImage(mediaArray[index] as? [AnyHashable : Any])
        
        if noneButton.isSelected {
            
            tempDict[DISPLAY_PRIVIEW] = mainImage
            
            if mediaDict![IS_VIDEO] as! String  == YES_STRING {
                var videoData: Data? = nil
                if let aMediaDict = mediaDict![MEDIA_ID] {
                    videoData = NSData(contentsOf: URL(fileURLWithPath: "\((Global.videoLocation())!)/\(aMediaDict).mp4")) as Data?
                }
                
                if let aMediaDict = mediaDict?[MEDIA_ID], let videoData = videoData {
                    NSData(data: videoData).write(toFile: "\(Global.newVideoLocation())/\(aMediaDict).mp4", atomically: true)
                }
            } else {
                tempDict[DISPLAY_MEDIA] = mainImage
            }
            tempMediaArray[index] = tempDict
            tempArrayCopy.removeLastObject()
            
            let index = index - 1
            addAttribute(tempArrayCopy: tempArrayCopy, index: index)
        }
        else {
            if tempDict[IS_VIDEO] as! String  == YES_STRING {
                
                let userNameImage = self.image(withview: makeOverLayView(mainImage!.size))
                
                let pointOfUserName = self.getPoint(imageSize: mainImage!.size, userNameImage: userNameImage)
                
                tempDict[DISPLAY_PRIVIEW] = self.draw(fgImage: userNameImage,bgImage: mainImage,point: pointOfUserName)
                
                var videoUrl: URL? = nil
                if let aTempDict = tempDict[MEDIA_ID] {
                    
                    videoUrl = NSURL.fileURL(withPath: "\((Global.videoLocation())!)/\(aTempDict).mp4") as URL
                }
                
                self.someFuntion(url: videoUrl, videoPath: tempDict[DISPLAY_MEDIA] as? String, completionHandler: { [self] responseDict in
                    
                    self.tempMediaArray[localIndex] = tempDict
                    
                    tempArrayCopy.removeLastObject()
                    
                    let localIndex = localIndex - 1
                    self.addAttribute(tempArrayCopy: tempArrayCopy, index: localIndex)
                })
            }
            else {
                let userNameImage = image(withview: makeOverLayView(mainImage!.size))
                
                let pointOfUserName = getPoint(imageSize: mainImage!.size, userNameImage: userNameImage)
                
                let q = DispatchQueue.global(qos: .default)
                q.async(execute: { [self] in
                    
                    tempDict[DISPLAY_PRIVIEW] = self.draw(fgImage: userNameImage, bgImage: mainImage, point: pointOfUserName)
                    
                    DispatchQueue.main.async(execute: { [self] in
                        self.tempMediaArray[localIndex] = tempDict
                        
                        tempArrayCopy.removeLastObject()
                        
                        let localIndex = localIndex - 1
                        self.addAttribute(tempArrayCopy: tempArrayCopy, index: localIndex)
                    })
                })
            }
        }
    }
    func someFuntion(url: URL?, videoPath: String?, completionHandler: @escaping (RequestCompletionHandler?) -> Void) {
        var videoPath = videoPath
        
        var videoAsset: AVAsset? = nil
        if let url = url {
            videoAsset = AVAsset(url: url)
        }
        
        //create an avassetrack with our asset
        let clipVideoTrack = videoAsset?.tracks(withMediaType: .video)[0]
        
        let size = clipVideoTrack?.naturalSize
        
        let userNameImage = image(withview: makeOverLayView(size!))
        
        let pointOfUserName = getPoint(imageSize: size!, userNameImage: userNameImage)
        
        //create a video composition and preset some settings
        let videoComposition = AVMutableVideoComposition()
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 20)
        
        //here we are setting its render size to its height x height (Square)
        videoComposition.renderSize = size ?? CGSize.zero
        
        //create a video instruction
        let instruction = AVMutableVideoCompositionInstruction()
        if let duration = videoAsset?.duration {
            instruction.timeRange = CMTimeRangeMake(start: .zero, duration: duration)
        }
        
        var transformer: AVMutableVideoCompositionLayerInstruction? = nil
        if let clipVideoTrack = clipVideoTrack {
            transformer = AVMutableVideoCompositionLayerInstruction(assetTrack: clipVideoTrack)
        }
        
        //Make sure the square is portrait
        
        if let preferredTransform = videoAsset?.preferredTransform {
            transformer?.setTransform(preferredTransform, at: .zero)
        }
        
        //add the transformer layer istructions, then add to video composition
        instruction.layerInstructions = [transformer].compactMap { $0 }
        videoComposition.instructions = [instruction]
        
        applyVideoEffects(tocomposition: videoComposition, size: size!, at: pointOfUserName, overlay: userNameImage)
        
        //Export
        var exporter: AVAssetExportSession? = nil
        if let videoAsset = videoAsset {
            exporter = AVAssetExportSession(asset: videoAsset, presetName: AVAssetExportPresetHighestQuality)
        }
        exporter?.videoComposition = videoComposition
        videoPath = videoPath?.replacingOccurrences(of: "file://", with: "")
        do {
            try FileManager.default.removeItem(atPath: videoPath ?? "")
        } catch {
        }
        exporter?.outputURL = URL(fileURLWithPath: videoPath ?? "")
        exporter?.outputFileType = .mov
        
        exporter?.exportAsynchronously(completionHandler:{
            if exporter?.status == .failed {
                print("failed")
            } else if exporter?.status == .completed {
                if let outputURL = exporter?.outputURL {
                    print(" exporter.outputURL = \(outputURL)")
                }
            }
            DispatchQueue.main.async(execute:{
                completionHandler(nil)
            })
        })
    }
    func applyVideoEffects(tocomposition: AVMutableVideoComposition?, size: CGSize, at point: CGPoint, overlay someImage: UIImage?) {
        let overlayLayer = CALayer()
        
        overlayLayer.contents = someImage?.cgImage
        
        overlayLayer.frame = CGRect(x: point.x, y: size.height - point.y - (someImage?.size.height ?? 0.0), width: someImage?.size.width ?? 0.0, height: someImage?.size.height ?? 0.0)
        
        overlayLayer.masksToBounds = true
        
        // 2 - set up the parent layer
        let parentLayer = CALayer()
        let videoLayer = CALayer()
        parentLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        videoLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        parentLayer.addSublayer(videoLayer)
        parentLayer.addSublayer(overlayLayer)
        
        // 3 - apply magic
        tocomposition?.animationTool = AVVideoCompositionCoreAnimationTool(
            postProcessingAsVideoLayer: videoLayer,
            in: parentLayer)
    }
    func getPoint(imageSize: CGSize, userNameImage: UIImage?) -> CGPoint {
        
        var pointToReturn = CGPoint(x: 0, y: 0)
        
        if topLeftButton.isSelected {
            pointToReturn = CGPoint(x: 0, y: 0)
        }
        if topRightButton.isSelected {
            pointToReturn = CGPoint(x: imageSize.width - (userNameImage?.size.width ?? 0.0), y: 0)
        }
        if bottomLeftButton.isSelected {
            pointToReturn = CGPoint(x: 0, y: imageSize.height - (userNameImage?.size.height ?? 0.0))
        }
        if bottomRightButton.isSelected {
            pointToReturn = CGPoint(x: imageSize.width - (userNameImage?.size.width ?? 0.0), y: imageSize.height - (userNameImage?.size.height ?? 0.0))
        }
        
        return pointToReturn
    }
    
    func draw(fgImage: UIImage?,bgImage: UIImage?,point: CGPoint) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(bgImage?.size ?? CGSize.zero, false, 0.0)
        bgImage?.draw(in: CGRect(x: 0, y: 0, width: bgImage?.size.width ?? 0.0, height: bgImage?.size.height ?? 0.0))
        fgImage?.draw(in: CGRect(x: point.x, y: point.y, width: fgImage?.size.width ?? 0.0, height: fgImage?.size.height ?? 0.0))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func image(withview: UIView?) -> UIImage? {
        
        //        UIGraphicsBeginImageContextWithOptions(withview.bounds.size, withview.opaque, 1.0);
        //
        //        [view.layer renderInContext:UIGraphicsGetCurrentContext()];
        //
        //        UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsBeginImageContextWithOptions((withview?.bounds.size)!, withview!.isOpaque, 1.0)
        if let context = UIGraphicsGetCurrentContext() {
            withview?.layer.render(in: context)
        }
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        let imageLayer = CALayer()
        imageLayer.frame = CGRect(x: 0, y: 0, width: image?.size.width ?? 0.0, height: image?.size.height ?? 0.0)
        imageLayer.contents = image?.cgImage
        
        imageLayer.masksToBounds = true
        imageLayer.cornerRadius = 10.0
        
        UIGraphicsBeginImageContext(CGSize(width: (image?.size.width ?? 0.0) - 1, height: (image?.size.height ?? 0.0) - 1))
        if let context = UIGraphicsGetCurrentContext() {
            imageLayer.render(in: context)
        }
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return roundedImage
    }
        func getDisplayImage(_ mediaDict: [AnyHashable : Any]?) -> UIImage? {
            var photo: UIImage? = nil
            let photoUrl = URL(fileURLWithPath: "\((Global.fullImageLocation())!)/\((mediaDict![MEDIA_ID])!).png")
            let photoData = NSData(contentsOf: photoUrl as URL)
            if photoData != nil {
                photo = UIImage(data: photoData! as Data)
                return photo
            }
            
            do {
                let string =  mediaDict!["thumbnail_url"] as! String
                if string != "" {
                    let data =  try Data(contentsOf: URL(string: mediaDict![THUMBNAIL_URL] as! String )!)
                    if !data.isEmpty{
                        photo = UIImage(data: data)
                    }
                }
                
                
            } catch {
                print(error)
            }
            
            
            if let aMediaDict = mediaDict?[MEDIA_ID], let photo1 = photo?.pngData() {
                NSData(data: photo1).write(toFile: "\((Global.fullImageLocation())!)/\((aMediaDict)).png", atomically: true)
            }
            
            return photo
        }
        
        func getLocalVideoUrl(_ mediaDict: [AnyHashable : Any]?) -> String? {
            let videoUrl = URL(fileURLWithPath: "\((Global.newVideoLocation()))/\((mediaDict![MEDIA_ID])!).mp4")
            var videoData = NSData(contentsOf: videoUrl as URL)
            if videoData != nil {
                return videoUrl.absoluteString
            }else{
                videoData = NSData(contentsOf: NSURL(string: mediaDict![MEDIA_URL] as! String)! as URL)
                if videoData == nil {
                    return ""
                }
                videoData!.write(toFile: "\((Global.newVideoLocation()))/\((mediaDict![MEDIA_ID])!).mp4", atomically: true)
                videoData!.write(toFile: "\((Global.videoLocation())!)/\((mediaDict![MEDIA_ID])!).mp4", atomically: true)
                return videoUrl.absoluteString
            }
        }
}
extension RepostViewController{
    // MARK: CollectiontionDelegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tempMediaArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let titleCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        let cell = PostRepostViewController(nibName: "PostRepostViewController", bundle: nil)
        cell.view.frame = CGRect(x: 0, y: 0, width: self.mediaCollection.bounds.width, height: self.mediaCollection.bounds.height)
        cell.delegate = self
        cell.index = indexPath
        if tempMediaArray.count != 0 {
            cell.mediaDict = tempMediaArray[indexPath.row] as! NSDictionary;
            cell.setValues()
            addChild(cell)
            let values = tempMediaArray[indexPath.row] as AnyObject
            previewViewDictionary[values[MEDIA_ID] as Any] = cell;
        }
        titleCell.addSubview(cell.view)

        return titleCell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cellSize = CGSize(width:self.mediaCollection.bounds.width , height: self.mediaCollection.bounds.height)
        return cellSize
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        let sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return sectionInset
    }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath){
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if pageController?.currentPage != Int(scrollView.contentOffset.x) / Int(scrollView.frame.width){
            let dic = tempMediaArray[pageController!.currentPage] as! NSDictionary
            if dic[IS_VIDEO] as! String == YES_STRING{
            PostRepostViewController.init().removewObserver()
            self.mediaCollection.reloadItems(at: [IndexPath.init(row: pageController!.currentPage, section: 0)])
            }
        }
        pageController?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        pageController?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }

   
}
